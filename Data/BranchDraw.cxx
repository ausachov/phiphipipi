#include <TMath.h>




void BranchDraw()
{
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  
  Double_t range0=0   *1000000;
  Double_t range1=5000*1000000;
  ULong64_t nch  = 1*100000000;
  
  TChain * DecayTreeCh=new TChain("DecayTree");
  DecayTreeCh->Add("AllPhiPhiPiPi_Snd.root");
    
    
   
    
    
  TTree* DecayTree = DecayTreeCh->CopyTree("runNumber==182739");
        
  ULong64_t NEn=DecayTree->GetEntries();
  UInt_t Branch_Value;
  ULong64_t brValue;
  DecayTree->SetBranchAddress("runNumber",&Branch_Value);
  DecayTree->SetBranchAddress("eventNumber",&brValue);

    
  TH1D * hist = new TH1D("hist","hist" , nch, range0, range1);

  for (ULong64_t i=0; i<NEn; i++) 
  {

      DecayTree->GetEntry(i);
      hist->Fill(brValue);
  }
  

    
    TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
    hist->SetXTitle("B_MM");
    hist->DrawCopy("");
}
