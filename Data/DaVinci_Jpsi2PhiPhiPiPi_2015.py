def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    # TISTOS for Jpsi
    from Configurables import TupleToolTISTOS, TupleToolDecay
    from Configurables import LoKi__Hybrid__TupleTool
    tuple.addTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)



    tuple.addTool(TupleToolDecay, name = 'Etac')
    tuple.Etac.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForEtac" ]
    tuple.Etac.addTool(TupleToolTISTOS, name="TupleToolTISTOSForEtac" )
    tuple.Etac.TupleToolTISTOSForEtac.Verbose=True
    tuple.Etac.TupleToolTISTOSForEtac.TriggerList = myTriggerList
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Etac=LoKi__Hybrid__TupleTool("LoKi_Etac")
    LoKi_Etac.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }    
    tuple.Etac.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Etac"]
    tuple.Etac.addTool(LoKi_Etac)


    tuple.addTool(TupleToolDecay, name = 'f0')
    tuple.f0.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForf0" ]
    tuple.f0.addTool(TupleToolTISTOS, name="TupleToolTISTOSForf0" )
    tuple.f0.TupleToolTISTOSForf0.Verbose=True
    tuple.f0.TupleToolTISTOSForf0.TriggerList = myTriggerList
    LoKi_f0=LoKi__Hybrid__TupleTool("LoKi_f0")
    LoKi_f0.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.f0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_f0"]
    tuple.f0.addTool(LoKi_f0)





    tuple.addTool(TupleToolDecay, name = 'Phi1')
    tuple.Phi1.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPhi1" ]
    tuple.Phi1.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPhi1" )
    tuple.Phi1.TupleToolTISTOSForPhi1.Verbose=True
    tuple.Phi1.TupleToolTISTOSForPhi1.TriggerList = myTriggerList
    LoKi_Phi1=LoKi__Hybrid__TupleTool("LoKi_Phi1")
    LoKi_Phi1.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Phi1.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi1"]
    tuple.Phi1.addTool(LoKi_Phi1)





    tuple.addTool(TupleToolDecay, name = 'Phi2')
    tuple.Phi2.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPhi2" ]
    tuple.Phi2.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPhi2" )
    tuple.Phi2.TupleToolTISTOSForPhi2.Verbose=True
    tuple.Phi2.TupleToolTISTOSForPhi2.TriggerList = myTriggerList
    LoKi_Phi2=LoKi__Hybrid__TupleTool("LoKi_Phi2")
    LoKi_Phi2.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Phi2.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi2"]
    tuple.Phi2.addTool(LoKi_Phi2)





    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        #,"m_scaled" : "DTF_FUN ( M , False )"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    

# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 "Hlt1AllL0Decision",
                 
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
    ]

year = "2015"
Jpsi2PhiPhiPiPiLocation = "Phys/Ccbar2PhiPhiPiPiLine/Particles"

from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2PhiPhiPiPiLocation) 
inputData = MomentumScaling(inputData, Turbo = False, Year = year)



from PhysSelPython.Wrappers import (
                                    Selection,
                                    SelectionSequence,
                                    DataOnDemand,
                                    AutomaticData,
                                    SimpleSelection
                                    )
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from Configurables import CombineParticles, DaVinci, FilterInTrees, PrintDecayTree

KaonsFromStr = FilterInTrees("KaonsFromStr",Code="('K+' == ABSID)")
Kaons = Selection("Kaons",Algorithm=KaonsFromStr,
                  RequiredSelections=[inputData])

phi = FilterInTrees("Combine_phi",Code="('phi(1020)' == ABSID)")
phi_sel = Selection("Sel_phi",Algorithm=phi,
                  RequiredSelections=[inputData])

PionsFromStr = FilterInTrees("PionsFromStr",Code="('pi+' == ABSID)")
Pions = Selection("Pions",Algorithm=PionsFromStr,
                  RequiredSelections=[inputData])


f0 = CombineParticles('Combine_f0',
                      DecayDescriptor='f_0(980) -> pi+ pi-',
                      DaughtersCuts={},
                      CombinationCut="AALL",
                      MotherCut="ALL"
                      )
f0_sel = Selection('Sel_f0',
                   Algorithm=f0,
                   RequiredSelections=[Pions])


etac_sel = SimpleSelection('Sel_etac',
                           ConfigurableGenerators.CombineParticles,
                           [phi_sel],
                           DecayDescriptor='eta_c(1S) -> phi(1020) phi(1020)',
                           DaughtersCuts={},
                           CombinationCut="(AM > 2.7*GeV) & (AM<6.3*GeV)",
                           MotherCut="ALL"
                           )
etac_seq = SelectionSequence('etac_Seq', TopSelection=etac_sel)


jpsi_sel = SimpleSelection('Sel_jpsi',
                           ConfigurableGenerators.CombineParticles,
                           [etac_sel, f0_sel],
                           DecayDescriptor='J/psi(1S) -> eta_c(1S) f_0(980)',
                           DaughtersCuts={},
                           CombinationCut="(AM > 2.7*GeV) & (AM<100.*GeV)",
                           MotherCut="ALL"
                           )
jpsi_seq = SelectionSequence('jpsi_Seq', TopSelection=jpsi_sel)



Jpsi2PhiPhiPiPiTuple = DecayTreeTuple("Jpsi2PhiPhiPiPiTuple")
Jpsi2PhiPhiPiPiTuple.Decay = "J/psi(1S) -> ^(eta_c(1S) -> ^(phi(1020) -> ^K+ ^K-) ^(phi(1020) -> ^K+ ^K-)) ^(f_0(980) ->^pi+ ^pi-)"
Jpsi2PhiPhiPiPiTuple.Inputs = [jpsi_seq.outputLocation()]





Jpsi2PhiPhiPiPiBranches = {
     "Kaon1" :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) ->^K+ K-) ( phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-)"
    ,"Kaon2" :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+^K-) ( phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-)"
    ,"Kaon3" :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ( phi(1020) ->^K+ K-)) (f_0(980)->pi+ pi-)"
    ,"Kaon4" :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ( phi(1020) -> K+^K-)) (f_0(980)->pi+ pi-)"
    ,"Phi1"  :  "J/psi(1S) -> (eta_c(1S)->^(phi(1020) -> K+ K-) ( phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-)"
    ,"Phi2"  :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ^(phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-)"
    ,"Etac"  :  "J/psi(1S) -> ^(eta_c(1S)->( phi(1020) -> K+ K-) (phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-)"
    ,"PiP"   :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ( phi(1020) -> K+ K-)) (f_0(980)->^pi+ pi-)"
    ,"PiM"   :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ( phi(1020) -> K+ K-)) (f_0(980)->pi+ ^pi-)"
    ,"f0"    :  "J/psi(1S) -> (eta_c(1S)->( phi(1020) -> K+ K-) ( phi(1020) -> K+ K-)) ^(f_0(980)->pi+ pi-)"
    ,"Jpsi"  :  "(J/psi(1S) -> (eta_c(1S)->(phi(1020) -> K+ K-) (phi(1020) -> K+ K-)) (f_0(980)->pi+ pi-))"
    }
fillTuple( Jpsi2PhiPhiPiPiTuple, Jpsi2PhiPhiPiPiBranches, myTriggerList )


# Jpsi2PhiPhiPiPiTuple = TupleSelection("Jpsi2PhiPhiPiPiTuple", jpsi_seq.outputLocation(), Decay = "J/psi(1S) -> ^(phi(1020) -> ^K+ ^K-) ^(phi(1020) -> ^K+ ^K-) ^pi+ ^pi-", Branches = Jpsi2PhiPhiPiPiBranches)
# fillTuple( Jpsi2PhiPhiPiPiTuple, myTriggerList )





# Jpsi2PhiPhiPiPiTuple.Inputs = [ inputData ]
# fillTuple( Jpsi2PhiPhiPiPiTuple, Jpsi2PhiPhiPiPiBranches, myTriggerList )



#from PhysConf.MicroDST import uDstConf
#uDstConf ("/Event/Charm")


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2PhiPhiPiPiFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2PhiPhiPiPiLineDecision')"""
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Ccbar2PhiPhiPiPiFilters.filters('Ccbar2PhiPhiPiPiFilters')
DaVinci().EvtMax = -1                        # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            jpsi_seq.sequence(),
                            Jpsi2PhiPhiPiPiTuple ]        # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Charm"

# Get Luminosity
DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20150724"
# DaVinci().CondDBtag = "cond-20161004"


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

# from GaudiConf import IOHelper
# # Use the local input data
# IOHelper().inputFiles([
#                        '00069595_00000016_1.charm.mdst'
#                        ], clear=True)


