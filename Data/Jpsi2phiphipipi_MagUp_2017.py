myJobName = 'Jpsi2PhiPhiPiPi_R17S29r2_MagUp_2017'

myApplication = GaudiExec()
myApplication.directory = "$HOME/cmtuser/DaVinciDev_v42r7p2"
myApplication.options = ['DaVinci_Jpsi2PhiPhiPiPi_2017.py']

data  = BKQuery('/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST', dqflag=['OK']).getDataset()

validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ DiracFile('Tuple.root'),
                         DiracFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

