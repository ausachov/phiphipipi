from ROOT import *
from array import *


def MakeSingle(type):

    chain = TChain("DecayTree");

    chain.Add("AllPhiPhiPiPi_Snd_Soft_1.root")
    newfile = TFile("AllPhiPhiPiPi_Snd_Soft_Mult_1.root","recreate")



    newtree = chain.CopyTree("")
    nEn = newtree.GetEntries()

    eventN    = array('L',[0])
    runN      = array('I',[0])
    vtxPrior  = array('i',[0])
    mult      = array('i',[0])
    vtX       = array('d',[0])
    pvZ       = array('d',[0])
    pvX       = array('d',[0])

    newtree.SetBranchAddress("eventNumber",eventN)
    newtree.SetBranchAddress("runNumber",runN)
    newtree.SetBranchAddress("Jpsi_ENDVERTEX_CHI2",vtX)
    newtree.SetBranchAddress("PVX",pvX)
    newtree.SetBranchAddress("PVZ",pvZ)


    print "33% TTree created"

    eventsMap = {}

    for i in range(nEn):
        newtree.GetEntry(i)
        runn = runN[0]
        eventn = eventN[0]
        vtx = vtX[0]
        pvx = int(1000*pvX[0])
        pvz = int(1000*pvZ[0])
        if((runn,eventn) in eventsMap.keys()):
            eventsMap[(runn,eventn,pvx,pvz)].append([i,vtx])
            eventsMap[(runn,eventn,pvx,pvz)].sort(key=lambda tup: tup[1])
        else:
            eventsMap[(runn,eventn,pvx,pvz)]=[[i,vtx]]
        if(i%1000==0):
            print i," events  read"
    #    print runn, eventn, vtx

    print "\n\n 66% Dictionary is ready  \n\n"

    VtxPriority_Branch = newtree.Branch("VtxPriority", vtxPrior, "VtxPriority/I")
    Mult_Branch = newtree.Branch("Multiple", mult, "Multiple/I")

    nProcessed = 0
#    keyList = eventsMap.keys()[:]
    for key in eventsMap.keys():
        length = len(eventsMap[key])
        mult[0] = int(length)
        for k in range(length):
            events = eventsMap[key][:]
            ev  = events[k]
            nev = ev[0]
            newtree.GetEntry(nev)
            vtxPrior[0] = int(k)
            VtxPriority_Branch.Fill()
            Mult_Branch.Fill()
            nProcessed = nProcessed+1
            if(nProcessed%1000==0):
                print "Processed ",nProcessed,"  events"
        del eventsMap[key]

    newtree.Print()
    newfile.Write()
    newfile.Close()


def merge():
    DecayTree = TChain("DecayTree")
    DecayTree.Add("AllPhiPhiPiPi_Snd_Down_Mult.root")
    DecayTree.Add("AllPhiPhiPiPi_Snd_Up_Mult.root")
    
    newfile = TFile("AllPhiPhiPiPi_Snd.root","recreate")
    newtree = DecayTree.CopyTree("")
    
    newtree.Print()
    newfile.Write()


if __name__ == "__main__":
    MakeSingle(0)
#    MakeSingle(1)
#    merge()
