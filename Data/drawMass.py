from ROOT import *

cutTrigger = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)"
cutTrack = "Kaon1_TRACK_CHI2NDOF<3 && Kaon2_TRACK_CHI2NDOF<3 && Kaon3_TRACK_CHI2NDOF<3 && Kaon4_TRACK_CHI2NDOF<3 && PiP_TRACK_CHI2NDOF<3 && PiM_TRACK_CHI2NDOF<3"
cutVtx = "Phi1_ENDVERTEX_CHI2<9 && Phi2_ENDVERTEX_CHI2<9 && Jpsi_ENDVERTEX_CHI2<81"
cutPID = "Kaon1_ProbNNk>0.2 && Kaon2_ProbNNk>0.2 && Kaon3_ProbNNk>0.2 && Kaon4_ProbNNk>0.2 && PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2"
cutIP = "Kaon1_IPCHI2_OWNPV>16 && Kaon2_IPCHI2_OWNPV>16 && Kaon3_IPCHI2_OWNPV>16 && Kaon4_IPCHI2_OWNPV>16 && PiM_IPCHI2_OWNPV>16 && PiP_IPCHI2_OWNPV>16"

#cutPhiVeto = "abs(Phi_M-1020)>10 && abs(Lst1_p2K_M-1020)>10 && abs(Lst2_p2K_M-1020)>10 && abs(Etac_PpPm2KpKm_M-1020)>10"
cutPhiM = "abs(Phi1_M-1020)<10 && abs(Phi2_M-1020)<10"

cutDef_v0 = cutTrigger+" && "+cutTrack+" && "+cutVtx+" && "+cutPID+" && "+cutIP+" && "+cutPhiM

ch = TChain("DecayTree")
#ch.Add("/eos/user/a/ausachov/DataRunII_phiphipipi/phiphipipi_RunII_merged_634.root")
#ch.Add("/eos/user/a/ausachov/DataRunII_phiphipipi/phiphipipi_RunII_merged_635.root")
ch.Add("phiphipipi_RunII_cutDef_v0.root")
h = TH1F("h","h",150,3000,4500)
h1 = TH1F("h1","h1",150,3000,4500)

#f = TFile("phiphipipi_RunII_cutDef_v0.root","recreate")
#tree = ch.CopyTree(cutDef_v0)
#f.Write()
#f.Close()


cutTightPhi = "abs(Phi1_M-1020)<5 && abs(Phi2_M-1020)<5"


p1 = "Kaon2"; p2 = "PiP"; var = "sqrt(({0}_PE+{1}_PE)**2-({0}_PX+{1}_PX)**2-({0}_PY+{1}_PY)**2-({0}_PZ+{1}_PZ)**2)".format(p1,p2)

p1 = "Kaon2"; p2 = "Kaon4"; newMass=139; var = "sqrt(({0}_PE+sqrt({1}_P**2+{2}**2))**2-({0}_PX+{1}_PX)**2-({0}_PY+{1}_PY)**2-({0}_PZ+{1}_PZ)**2)".format(p1,p2,newMass)

p1 = "PiP"; p2 = "PiM"; newMass1=497; newMass2=497; var = "sqrt((sqrt({0}_P**2+{2}**2)+sqrt({1}_P**2+{3}**2))**2-({0}_PX+{1}_PX)**2-({0}_PY+{1}_PY)**2-({0}_PZ+{1}_PZ)**2)".format(p1,p2,newMass1,newMass2)


p1 = "Etac"; p2 = "PiM"; p3 = "PiP"; newMass=496; var = "sqrt(({0}_PE+{1}_PE+sqrt({2}_P**2+{3}**2))**2-({0}_PX+{1}_PX+{2}_PX)**2-({0}_PY+{1}_PY+{2}_PY)**2-({0}_PZ+{1}_PZ+{2}_PZ)**2)".format(p1,p2,p3,newMass)


cutPhiW1_M = 



