from ROOT import *
from ROOT.TMath import Sqrt, Sq
from array import array

mPi = 139.57018
mK  = 493.67700
mP  = 938.27205 



def fillNewTuple(filename,newfilename):

   chain  = TChain("DecayTree")
   chain.Add(filename)
   newfile = TFile(newfilename,"recreate")
   newtree = chain.CopyTree("")


   Kaon1_P  = array('d',[0]); newtree.SetBranchAddress("Kaon1_P", Kaon1_P)
   Kaon1_PE = array('d',[0]); newtree.SetBranchAddress("Kaon1_PE",Kaon1_PE)
   Kaon1_PX = array('d',[0]); newtree.SetBranchAddress("Kaon1_PX",Kaon1_PX)
   Kaon1_PY = array('d',[0]); newtree.SetBranchAddress("Kaon1_PY",Kaon1_PY)
   Kaon1_PZ = array('d',[0]); newtree.SetBranchAddress("Kaon1_PZ",Kaon1_PZ)
   Kaon1_PT = array('d',[0]); newtree.SetBranchAddress("Kaon1_PT",Kaon1_PT)

   Kaon2_P  = array('d',[0]); newtree.SetBranchAddress("Kaon2_P",Kaon2_P)
   Kaon2_PE = array('d',[0]); newtree.SetBranchAddress("Kaon2_PE",Kaon2_PE)
   Kaon2_PX = array('d',[0]); newtree.SetBranchAddress("Kaon2_PX",Kaon2_PX)
   Kaon2_PY = array('d',[0]); newtree.SetBranchAddress("Kaon2_PY",Kaon2_PY)
   Kaon2_PZ = array('d',[0]); newtree.SetBranchAddress("Kaon2_PZ",Kaon2_PZ)
   Kaon2_PT = array('d',[0]); newtree.SetBranchAddress("Kaon2_PT",Kaon2_PT)

   Kaon3_P  = array('d',[0]); newtree.SetBranchAddress("Kaon3_P",Kaon3_P)
   Kaon3_PE = array('d',[0]); newtree.SetBranchAddress("Kaon3_PE",Kaon3_PE)
   Kaon3_PX = array('d',[0]); newtree.SetBranchAddress("Kaon3_PX",Kaon3_PX)
   Kaon3_PY = array('d',[0]); newtree.SetBranchAddress("Kaon3_PY",Kaon3_PY)
   Kaon3_PZ = array('d',[0]); newtree.SetBranchAddress("Kaon3_PZ",Kaon3_PZ)
   Kaon3_PT = array('d',[0]); newtree.SetBranchAddress("Kaon3_PT",Kaon3_PT)

   Kaon4_P  = array('d',[0]); newtree.SetBranchAddress("Kaon4_P",Kaon4_P)
   Kaon4_PE = array('d',[0]); newtree.SetBranchAddress("Kaon4_PE",Kaon4_PE)
   Kaon4_PX = array('d',[0]); newtree.SetBranchAddress("Kaon4_PX",Kaon4_PX)
   Kaon4_PY = array('d',[0]); newtree.SetBranchAddress("Kaon4_PY",Kaon4_PY)
   Kaon4_PZ = array('d',[0]); newtree.SetBranchAddress("Kaon4_PZ",Kaon4_PZ)
   Kaon4_PT = array('d',[0]); newtree.SetBranchAddress("Kaon4_PT",Kaon4_PT)


   Phi1_P  = array('d',[0]); newtree.SetBranchAddress("Phi1_P",Phi1_P)
   Phi1_PE = array('d',[0]); newtree.SetBranchAddress("Phi1_PE",Phi1_PE)
   Phi1_PX = array('d',[0]); newtree.SetBranchAddress("Phi1_PX",Phi1_PX)
   Phi1_PY = array('d',[0]); newtree.SetBranchAddress("Phi1_PY",Phi1_PY)
   Phi1_PZ = array('d',[0]); newtree.SetBranchAddress("Phi1_PZ",Phi1_PZ)
   Phi1_PT = array('d',[0]); newtree.SetBranchAddress("Phi1_PT",Phi1_PT)

   Phi2_P  = array('d',[0]); newtree.SetBranchAddress("Phi2_P",Phi2_P)
   Phi2_PE = array('d',[0]); newtree.SetBranchAddress("Phi2_PE",Phi2_PE)
   Phi2_PX = array('d',[0]); newtree.SetBranchAddress("Phi2_PX",Phi2_PX)
   Phi2_PY = array('d',[0]); newtree.SetBranchAddress("Phi2_PY",Phi2_PY)
   Phi2_PZ = array('d',[0]); newtree.SetBranchAddress("Phi2_PZ",Phi2_PZ)
   Phi2_PT = array('d',[0]); newtree.SetBranchAddress("Phi2_PT",Phi2_PT)


   PiP_P  = array('d',[0]); newtree.SetBranchAddress("PiP_P",PiP_P)
   PiP_PE = array('d',[0]); newtree.SetBranchAddress("PiP_PE",PiP_PE)
   PiP_PX = array('d',[0]); newtree.SetBranchAddress("PiP_PX",PiP_PX)
   PiP_PY = array('d',[0]); newtree.SetBranchAddress("PiP_PY",PiP_PY)
   PiP_PZ = array('d',[0]); newtree.SetBranchAddress("PiP_PZ",PiP_PZ)
   PiP_PT = array('d',[0]); newtree.SetBranchAddress("PiP_PT",PiP_PT)

   PiM_P  = array('d',[0]); newtree.SetBranchAddress("PiM_P",PiM_P)
   PiM_PE = array('d',[0]); newtree.SetBranchAddress("PiM_PE",PiM_PE)
   PiM_PX = array('d',[0]); newtree.SetBranchAddress("PiM_PX",PiM_PX)
   PiM_PY = array('d',[0]); newtree.SetBranchAddress("PiM_PY",PiM_PY)
   PiM_PZ = array('d',[0]); newtree.SetBranchAddress("PiM_PZ",PiM_PZ)
   PiM_PT = array('d',[0]); newtree.SetBranchAddress("PiM_PT",PiM_PT)

   f0_P  = array('d',[0]); newtree.SetBranchAddress("f0_P",f0_P)
   f0_PE = array('d',[0]); newtree.SetBranchAddress("f0_PE",f0_PE)
   f0_PX = array('d',[0]); newtree.SetBranchAddress("f0_PX",f0_PX)
   f0_PY = array('d',[0]); newtree.SetBranchAddress("f0_PY",f0_PY)
   f0_PZ = array('d',[0]); newtree.SetBranchAddress("f0_PZ",f0_PZ)
   f0_PT = array('d',[0]); newtree.SetBranchAddress("f0_PT",f0_PT)


   Jpsi_P  = array('d',[0]); newtree.SetBranchAddress("Jpsi_P",Jpsi_P)
   Jpsi_PE = array('d',[0]); newtree.SetBranchAddress("Jpsi_PE",Jpsi_PE)
   Jpsi_PX = array('d',[0]); newtree.SetBranchAddress("Jpsi_PX",Jpsi_PX)
   Jpsi_PY = array('d',[0]); newtree.SetBranchAddress("Jpsi_PY",Jpsi_PY)
   Jpsi_PZ = array('d',[0]); newtree.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
   Jpsi_PT = array('d',[0]); newtree.SetBranchAddress("Jpsi_PT",Jpsi_PT)




   PhiW1_M  = array('d',[0]); PhiW1_M_Branch  = newtree.Branch("PhiW1_M", PhiW1_M, "PhiW1_M/D")
   PhiW1_P  = array('d',[0]); PhiW1_P_Branch  = newtree.Branch("PhiW1_P", PhiW1_P, "PhiW1_P/D")
   PhiW1_PE = array('d',[0]); PhiW1_PE_Branch = newtree.Branch("PhiW1_PE",PhiW1_PE,"PhiW1_PE/D")

   PhiW2_M  = array('d',[0]); PhiW2_M_Branch  = newtree.Branch("PhiW2_M", PhiW2_M, "PhiW2_M/D")
   PhiW2_P  = array('d',[0]); PhiW2_P_Branch  = newtree.Branch("PhiW2_P", PhiW2_P, "PhiW2_P/D")
   PhiW2_PE = array('d',[0]); PhiW2_PE_Branch = newtree.Branch("PhiW2_PE",PhiW2_PE,"PhiW2_PE/D")

   Phi1ws_M  = array('d',[0]); Phi1ws_M_Branch  = newtree.Branch("Phi1ws_M", Phi1ws_M, "Phi1ws_M/D")
   Phi1ws_P  = array('d',[0]); Phi1ws_P_Branch  = newtree.Branch("Phi1ws_P", Phi1ws_P, "Phi1ws_P/D")
   Phi1ws_PE = array('d',[0]); Phi1ws_PE_Branch = newtree.Branch("Phi1ws_PE",Phi1ws_PE,"Phi1ws_PE/D")

   Phi2ws_M  = array('d',[0]); Phi2ws_M_Branch  = newtree.Branch("Phi2ws_M", Phi2ws_M, "Phi2ws_M/D")
   Phi2ws_P  = array('d',[0]); Phi2ws_P_Branch  = newtree.Branch("Phi2ws_P", Phi2ws_P, "Phi2ws_P/D")
   Phi2ws_PE = array('d',[0]); Phi2ws_PE_Branch = newtree.Branch("Phi2ws_PE",Phi2ws_PE,"Phi2ws_PE/D")


   Phi1_K2pi_M  = array('d',[0]); Phi1_K2pi_M_Branch  = newtree.Branch("Phi1_K2pi_M", Phi1_K2pi_M, "Phi1_K2pi_M/D")
   Phi1_K2pi_P  = array('d',[0]); Phi1_K2pi_P_Branch  = newtree.Branch("Phi1_K2pi_P", Phi1_K2pi_P, "Phi1_K2pi_P/D")
   Phi1_K2pi_PE = array('d',[0]); Phi1_K2pi_PE_Branch = newtree.Branch("Phi1_K2pi_PE",Phi1_K2pi_PE,"Phi1_K2pi_PE/D")

   Phi2_K2pi_M  = array('d',[0]); Phi2_K2pi_M_Branch  = newtree.Branch("Phi2_K2pi_M", Phi2_K2pi_M, "Phi2_K2pi_M/D")
   Phi2_K2pi_P  = array('d',[0]); Phi2_K2pi_P_Branch  = newtree.Branch("Phi2_K2pi_P", Phi2_K2pi_P, "Phi2_K2pi_P/D")
   Phi2_K2pi_PE = array('d',[0]); Phi2_K2pi_PE_Branch = newtree.Branch("Phi2_K2pi_PE",Phi2_K2pi_PE,"Phi2_K2pi_PE/D")



   Phi1_K2p_M  = array('d',[0]); Phi1_K2p_M_Branch  = newtree.Branch("Phi1_K2p_M", Phi1_K2p_M, "Phi1_K2p_M/D")
   Phi1_K2p_P  = array('d',[0]); Phi1_K2p_P_Branch  = newtree.Branch("Phi1_K2p_P", Phi1_K2p_P, "Phi1_K2p_P/D")
   Phi1_K2p_PE = array('d',[0]); Phi1_K2p_PE_Branch = newtree.Branch("Phi1_K2p_PE",Phi1_K2p_PE,"Phi1_K2p_PE/D")


   Phi2_K2p_M  = array('d',[0]); Phi2_K2p_M_Branch  = newtree.Branch("Phi2_K2p_M", Phi2_K2p_M, "Phi2_K2p_M/D")
   Phi2_K2p_P  = array('d',[0]); Phi2_K2p_P_Branch  = newtree.Branch("Phi2_K2p_P", Phi2_K2p_P, "Phi2_K2p_P/D")
   Phi2_K2p_PE = array('d',[0]); Phi2_K2p_PE_Branch = newtree.Branch("Phi2_K2p_PE",Phi2_K2p_PE,"Phi2_K2p_PE/D")





   PhiW1_Km2Pim_M  = array('d',[0]); PhiW1_Km2Pim_M_Branch  = newtree.Branch("PhiW1_Km2Pim_M", PhiW1_Km2Pim_M, "PhiW1_Km2Pim_M/D")
   PhiW1_Km2Pim_P  = array('d',[0]); PhiW1_Km2Pim_P_Branch  = newtree.Branch("PhiW1_Km2Pim_P", PhiW1_Km2Pim_P, "PhiW1_Km2Pim_P/D")
   PhiW1_Km2Pim_PE = array('d',[0]); PhiW1_Km2Pim_PE_Branch = newtree.Branch("PhiW1_Km2Pim_PE",PhiW1_Km2Pim_PE,"PhiW1_Km2Pim_PE/D")

   PhiW1_Kp2Pip_M  = array('d',[0]); PhiW1_Kp2Pip_M_Branch  = newtree.Branch("PhiW1_Kp2Pip_M", PhiW1_Kp2Pip_M, "PhiW1_Kp2Pip_M/D")
   PhiW1_Kp2Pip_P  = array('d',[0]); PhiW1_Kp2Pip_P_Branch  = newtree.Branch("PhiW1_Kp2Pip_P", PhiW1_Kp2Pip_P, "PhiW1_Kp2Pip_P/D")
   PhiW1_Kp2Pip_PE = array('d',[0]); PhiW1_Kp2Pip_PE_Branch = newtree.Branch("PhiW1_Kp2Pip_PE",PhiW1_Kp2Pip_PE,"PhiW1_Kp2Pip_PE/D")




   PhiW2_Pm2Pim_M  = array('d',[0]); PhiW2_Pm2Pim_M_Branch  = newtree.Branch("PhiW2_Pm2Pim_M", PhiW2_Pm2Pim_M, "PhiW2_Pm2Pim_M/D")
   PhiW2_Pm2Pim_P  = array('d',[0]); PhiW2_Pm2Pim_P_Branch  = newtree.Branch("PhiW2_Pm2Pim_P", PhiW2_Pm2Pim_P, "PhiW2_Pm2Pim_P/D")
   PhiW2_Pm2Pim_PE = array('d',[0]); PhiW2_Pm2Pim_PE_Branch = newtree.Branch("PhiW2_Pm2Pim_PE",PhiW2_Pm2Pim_PE,"PhiW2_Pm2Pim_PE/D")

   PhiW2_Pp2Pip_M  = array('d',[0]); PhiW2_Pp2Pip_M_Branch  = newtree.Branch("PhiW2_Pp2Pip_M", PhiW2_Pp2Pip_M, "PhiW2_Pp2Pip_M/D")
   PhiW2_Pp2Pip_P  = array('d',[0]); PhiW2_Pp2Pip_P_Branch  = newtree.Branch("PhiW2_Pp2Pip_P", PhiW2_Pp2Pip_P, "PhiW2_Pp2Pip_P/D")
   PhiW2_Pp2Pip_PE = array('d',[0]); PhiW2_Pp2Pip_PE_Branch = newtree.Branch("PhiW2_Pp2Pip_PE",PhiW2_Pp2Pip_PE,"PhiW2_Pp2Pip_PE/D")




# 3 body
   PpKK_M  = array('d',[0]); PpKK_M_Branch  = newtree.Branch("PpKK_M", PpKK_M, "PpKK_M/D")
   PpKK_P  = array('d',[0]); PpKK_P_Branch  = newtree.Branch("PpKK_P", PpKK_P, "PpKK_P/D")
   PpKK_PE = array('d',[0]); PpKK_PE_Branch = newtree.Branch("PpKK_PE",PpKK_PE,"PpKK_PE/D")

   PmKK_M  = array('d',[0]); PmKK_M_Branch  = newtree.Branch("PmKK_M", PmKK_M, "PmKK_M/D")
   PmKK_P  = array('d',[0]); PmKK_P_Branch  = newtree.Branch("PmKK_P", PmKK_P, "PmKK_P/D")
   PmKK_PE = array('d',[0]); PmKK_PE_Branch = newtree.Branch("PmKK_PE",PmKK_PE,"PmKK_PE/D")





   PpKm_Kp2Pip_M  = array('d',[0]); PpKm_Kp2Pip_M_Branch  = newtree.Branch("PpKm_Kp2Pip_M", PpKm_Kp2Pip_M, "PpKm_Kp2Pip_M/D")
   PpKm_Kp2Pip_P  = array('d',[0]); PpKm_Kp2Pip_P_Branch  = newtree.Branch("PpKm_Kp2Pip_P", PpKm_Kp2Pip_P, "PpKm_Kp2Pip_P/D")
   PpKm_Kp2Pip_PE = array('d',[0]); PpKm_Kp2Pip_PE_Branch = newtree.Branch("PpKm_Kp2Pip_PE",PpKm_Kp2Pip_PE,"PpKm_Kp2Pip_PE/D")




   PmKp_Km2Pim_M  = array('d',[0]); PmKp_Km2Pim_M_Branch  = newtree.Branch("PmKp_Km2Pim_M", PmKp_Km2Pim_M, "PmKp_Km2Pim_M/D")
   PmKp_Km2Pim_P  = array('d',[0]); PmKp_Km2Pim_P_Branch  = newtree.Branch("PmKp_Km2Pim_P", PmKp_Km2Pim_P, "PmKp_Km2Pim_P/D")
   PmKp_Km2Pim_PE = array('d',[0]); PmKp_Km2Pim_PE_Branch = newtree.Branch("PmKp_Km2Pim_PE",PmKp_Km2Pim_PE,"PmKp_Km2Pim_PE/D")


   PhiW1_Km2Pm_M  = array('d',[0]); PhiW1_Km2Pm_M_Branch  = newtree.Branch("PhiW1_Km2Pm_M", PhiW1_Km2Pm_M, "PhiW1_Km2Pm_M/D")
   PhiW1_Km2Pm_P  = array('d',[0]); PhiW1_Km2Pm_P_Branch  = newtree.Branch("PhiW1_Km2Pm_P", PhiW1_Km2Pm_P, "PhiW1_Km2Pm_P/D")
   PhiW1_Km2Pm_PE = array('d',[0]); PhiW1_Km2Pm_PE_Branch = newtree.Branch("PhiW1_Km2Pm_PE",PhiW1_Km2Pm_PE,"PhiW1_Km2Pm_PE/D")


   PhiW1_Kp2Pp_M  = array('d',[0]); PhiW1_Kp2Pp_M_Branch  = newtree.Branch("PhiW1_Kp2Pp_M", PhiW1_Kp2Pp_M, "PhiW1_Kp2Pp_M/D")
   PhiW1_Kp2Pp_P  = array('d',[0]); PhiW1_Kp2Pp_P_Branch  = newtree.Branch("PhiW1_Kp2Pp_P", PhiW1_Kp2Pp_P, "PhiW1_Kp2Pp_P/D")
   PhiW1_Kp2Pp_PE = array('d',[0]); PhiW1_Kp2Pp_PE_Branch = newtree.Branch("PhiW1_Kp2Pp_PE",PhiW1_Kp2Pp_PE,"PhiW1_Kp2Pp_PE/D")



   PhiW2_Pm2Km_M  = array('d',[0]); PhiW2_Pm2Km_M_Branch  = newtree.Branch("PhiW2_Pm2Km_M", PhiW2_Pm2Km_M, "PhiW2_Pm2Km_M/D")
   PhiW2_Pm2Km_P  = array('d',[0]); PhiW2_Pm2Km_P_Branch  = newtree.Branch("PhiW2_Pm2Km_P", PhiW2_Pm2Km_P, "PhiW2_Pm2Km_P/D")
   PhiW2_Pm2Km_PE = array('d',[0]); PhiW2_Pm2Km_PE_Branch = newtree.Branch("PhiW2_Pm2Km_PE",PhiW2_Pm2Km_PE,"PhiW2_Pm2Km_PE/D")


   PhiW2_Pp2Kp_M  = array('d',[0]); PhiW2_Pp2Kp_M_Branch  = newtree.Branch("PhiW2_Pp2Kp_M", PhiW2_Pp2Kp_M, "PhiW2_Pp2Kp_M/D")
   PhiW2_Pp2Kp_P  = array('d',[0]); PhiW2_Pp2Kp_P_Branch  = newtree.Branch("PhiW2_Pp2Kp_P", PhiW2_Pp2Kp_P, "PhiW2_Pp2Kp_P/D")
   PhiW2_Pp2Kp_PE = array('d',[0]); PhiW2_Pp2Kp_PE_Branch = newtree.Branch("PhiW2_Pp2Kp_PE",PhiW2_Pp2Kp_PE,"PhiW2_Pp2Kp_PE/D")



   PhiW2_PpPm2KpKm_M  = array('d',[0]); PhiW2_PpPm2KpKm_M_Branch  = newtree.Branch("PhiW2_PpPm2KpKm_M", PhiW2_PpPm2KpKm_M, "PhiW2_PpPm2KpKm_M/D")
   PhiW2_PpPm2KpKm_P  = array('d',[0]); PhiW2_PpPm2KpKm_P_Branch  = newtree.Branch("PhiW2_PpPm2KpKm_P", PhiW2_PpPm2KpKm_P, "PhiW2_PpPm2KpKm_P/D")
   PhiW2_PpPm2KpKm_PE = array('d',[0]); PhiW2_PpPm2KpKm_PE_Branch = newtree.Branch("PhiW2_PpPm2KpKm_PE",PhiW2_PpPm2KpKm_PE,"PhiW2_PpPm2KpKm_PE/D")


   PPKm_M  = array('d',[0]); PPKm_M_Branch  = newtree.Branch("PPKm_M", PPKm_M, "PPKm_M/D")
   PPKm_P  = array('d',[0]); PPKm_P_Branch  = newtree.Branch("PPKm_P", PPKm_P, "PPKm_P/D")
   PPKm_PE = array('d',[0]); PPKm_PE_Branch = newtree.Branch("PPKm_PE",PPKm_PE,"PPKm_PE/D")

   PmKm_Pp2Pip_M  = array('d',[0]); PmKm_Pp2Pip_M_Branch  = newtree.Branch("PmKm_Pp2Pip_M", PmKm_Pp2Pip_M, "PmKm_Pp2Pip_M/D")
   PmKm_Pp2Pip_P  = array('d',[0]); PmKm_Pp2Pip_P_Branch  = newtree.Branch("PmKm_Pp2Pip_P", PmKm_Pp2Pip_P, "PmKm_Pp2Pip_P/D")
   PmKm_Pp2Pip_PE = array('d',[0]); PmKm_Pp2Pip_PE_Branch = newtree.Branch("PmKm_Pp2Pip_PE",PmKm_Pp2Pip_PE,"PmKm_Pp2Pip_PE/D")


   PPKp_M  = array('d',[0]); PPKp_M_Branch  = newtree.Branch("PPKp_M", PPKp_M, "PPKp_M/D")
   PPKp_P  = array('d',[0]); PPKp_P_Branch  = newtree.Branch("PPKp_P", PPKp_P, "PPKp_P/D")
   PPKp_PE = array('d',[0]); PPKp_PE_Branch = newtree.Branch("PPKp_PE",PPKp_PE,"PPKp_PE/D")


   PpKp_Pm2Pim_M  = array('d',[0]); PpKp_Pm2Pim_M_Branch  = newtree.Branch("PpKp_Pm2Pim_M", PpKp_Pm2Pim_M, "PpKp_Pm2Pim_M/D")
   PpKp_Pm2Pim_P  = array('d',[0]); PpKp_Pm2Pim_P_Branch  = newtree.Branch("PpKp_Pm2Pim_P", PpKp_Pm2Pim_P, "PpKp_Pm2Pim_P/D")
   PpKp_Pm2Pim_PE = array('d',[0]); PpKp_Pm2Pim_PE_Branch = newtree.Branch("PpKp_Pm2Pim_PE",PpKp_Pm2Pim_PE,"PpKp_Pm2Pim_PE/D")


   nEn = newtree.GetEntries()
   for i in range(nEn):
      newtree.GetEntry(i)
      PhiW1_PE[0] = Kaon1_PE[0] + Kaon2_PE[0]
      PhiW1_P[0]  = Sqrt((Kaon1_PX[0] + Kaon2_PX[0])**2 + \
                       (Kaon1_PY[0] + Kaon2_PY[0])**2 + \
                       (Kaon1_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = PhiW1_PE[0]**2-PhiW1_P[0]**2
      if(valM2>0):
         PhiW1_M[0] = Sqrt(valM2)
      else:
         PhiW1_M[0] = -999


      PhiW2_PE[0] = Kaon3_PE[0] + Kaon4_PE[0]
      PhiW2_P[0]  = Sqrt((Kaon3_PX[0] + Kaon4_PX[0])**2 + \
                        (Kaon3_PY[0] + Kaon4_PY[0])**2 + \
                        (Kaon3_PZ[0] + Kaon4_PZ[0])**2)
      valM2 = PhiW2_PE[0]**2-PhiW2_P[0]**2
      if(valM2>0):
         PhiW2_M[0] = Sqrt(valM2)
      else:
         PhiW2_M[0] = -999


      Phi1ws_PE[0] = Kaon1_PE[0] + Kaon3_PE[0]
      Phi1ws_P[0] = Sqrt((Kaon1_PX[0] + Kaon3_PX[0])**2 + \
                         (Kaon1_PY[0] + Kaon3_PY[0])**2 + \
                         (Kaon1_PZ[0] + Kaon3_PZ[0])**2)
      valM2 = Phi1ws_PE[0]**2-Phi1ws_P[0]**2
      if(valM2>0):
         Phi1ws_M[0] = Sqrt(valM2)
      else:
         Phi1ws_M[0] = -999


      Phi2ws_PE[0] = Kaon2_PE[0] + Kaon4_PE[0]
      Phi2ws_P[0]  = Sqrt((Kaon2_PX[0] + Kaon4_PX[0])**2 + \
                          (Kaon2_PY[0] + Kaon4_PY[0])**2 + \
                          (Kaon2_PZ[0] + Kaon4_PZ[0])**2)
      valM2 = Phi2ws_PE[0]**2-Phi2ws_P[0]**2
      if(valM2>0):
         Phi2ws_M[0] = Sqrt(valM2)
      else:
         Phi2ws_M[0] = -999

      # Phi1:Lambda(1520)0 -> p+ K-
      Phi1_K2pi_PE[0] = Kaon4_PE[0] + Sqrt(Kaon1_P[0]**2 + mPi**2)
      Phi1_K2pi_P[0]  = Sqrt((Kaon4_PX[0] + Kaon1_PX[0])**2 + \
                             (Kaon4_PY[0] + Kaon1_PY[0])**2 + \
                             (Kaon4_PZ[0] + Kaon1_PZ[0])**2)
      valM2 = Phi1_K2pi_PE[0]**2-Phi1_K2pi_P[0]**2
      if(valM2>0):
         Phi1_K2pi_M[0] = Sqrt(valM2)
      else:
         Phi1_K2pi_M[0] = -999

      Phi1_K2p_PE[0] = Kaon4_PE[0] + Sqrt(Kaon1_P[0]**2 + mP**2)
      Phi1_K2p_P[0]  = Sqrt((Kaon4_PX[0] + Kaon1_PX[0])**2 + \
                            (Kaon4_PY[0] + Kaon1_PY[0])**2 + \
                            (Kaon4_PZ[0] + Kaon1_PZ[0])**2)
      valM2 = Phi1_K2p_PE[0]**2-Phi1_K2p_P[0]**2
      if(valM2>0):
         Phi1_K2p_M[0] = Sqrt(valM2)
      else:
         Phi1_K2p_M[0] = -999


      Phi1_p2pi_PE[0] = Kaon1_PE[0] + Sqrt(Kaon4_P[0]**2 + mPi**2)
      Phi1_p2pi_P[0]  = Sqrt((Kaon4_PX[0] + Kaon1_PX[0])**2 + \
                             (Kaon4_PY[0] + Kaon1_PY[0])**2 + \
                             (Kaon4_PZ[0] + Kaon1_PZ[0])**2)
      valM2 = Phi1_p2pi_PE[0]**2-Phi1_p2pi_P[0]**2
      if(valM2>0):
         Phi1_p2pi_M[0] = Sqrt(valM2)
      else:
         Phi1_p2pi_M[0] = -999

      Phi1_p2K_PE[0] = Kaon1_PE[0] + Sqrt(Kaon4_P[0]**2 + mK**2)
      Phi1_p2K_P[0]  = Sqrt((Kaon4_PX[0] + Kaon1_PX[0])**2 + \
                            (Kaon4_PY[0] + Kaon1_PY[0])**2 + \
                            (Kaon4_PZ[0] + Kaon1_PZ[0])**2)
      valM2 = Phi1_p2K_PE[0]**2-Phi1_p2K_P[0]**2
      if(valM2>0):
         Phi1_p2K_M[0] = Sqrt(valM2)
      else:
         Phi1_p2K_M[0] = -999


      # Phi2:Lambda(1520)~0 -> p~- K+
      Phi2_K2pi_PE[0] = Kaon3_PE[0] + Sqrt(Kaon2_P[0]**2 + mPi**2)
      Phi2_K2pi_P[0]  = Sqrt((Kaon3_PX[0] + Kaon2_PX[0])**2 + \
                             (Kaon3_PY[0] + Kaon2_PY[0])**2 + \
                             (Kaon3_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = Phi2_K2pi_PE[0]**2-Phi2_K2pi_P[0]**2
      if(valM2>0):
         Phi2_K2pi_M[0] = Sqrt(valM2)
      else:
         Phi2_K2pi_M[0] = -999

      Phi2_K2p_PE[0] = Kaon3_PE[0] + Sqrt(Kaon2_P[0]**2 + mP**2)
      Phi2_K2p_P[0]  = Sqrt((Kaon3_PX[0] + Kaon2_PX[0])**2 + \
                            (Kaon3_PY[0] + Kaon2_PY[0])**2 + \
                            (Kaon3_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = Phi2_K2p_PE[0]**2-Phi2_K2p_P[0]**2
      if(valM2>0):
         Phi2_K2p_M[0] = Sqrt(valM2)
      else:
         Phi2_K2p_M[0] = -999


      Phi2_p2pi_PE[0] = Kaon2_PE[0] + Sqrt(Kaon3_P[0]**2 + mPi**2)
      Phi2_p2pi_P[0]  = Sqrt((Kaon3_PX[0] + Kaon2_PX[0])**2 + \
                             (Kaon3_PY[0] + Kaon2_PY[0])**2 + \
                             (Kaon3_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = Phi2_p2pi_PE[0]**2-Phi2_p2pi_P[0]**2
      if(valM2>0):
         Phi2_p2pi_M[0] = Sqrt(valM2)
      else:
         Phi2_p2pi_M[0] = -999

      Phi2_p2K_PE[0] = Kaon2_PE[0] + Sqrt(Kaon3_P[0]**2 + mK**2)
      Phi2_p2K_P[0]  = Sqrt((Kaon3_PX[0] + Kaon2_PX[0])**2 + \
                            (Kaon3_PY[0] + Kaon2_PY[0])**2 + \
                            (Kaon3_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = Phi2_p2K_PE[0]**2-Phi2_p2K_P[0]**2
      if(valM2>0):
         Phi2_p2K_M[0] = Sqrt(valM2)
      else:
         Phi2_p2K_M[0] = -999


      #Kp Km->Pim
      PhiW1_Km2Pim_PE[0] = Kaon2_PE[0] + Sqrt(Kaon1_P[0]**2 + mPi**2)
      PhiW1_Km2Pim_P[0]  = PhiW1_P[0]
      valM2 = PhiW1_Km2Pim_PE[0]**2-PhiW1_Km2Pim_P[0]**2
      if(valM2>0):
         PhiW1_Km2Pim_M[0] = Sqrt(valM2)
      else:
         PhiW1_Km2Pim_M[0] = -999


      #Kp Km->Pm
      PhiW1_Km2Pm_PE[0] = Kaon2_PE[0] + Sqrt(Kaon1_P[0]**2 + mP**2)
      PhiW1_Km2Pm_P[0]  = PhiW1_P[0]
      valM2 = PhiW1_Km2Pm_PE[0]**2-PhiW1_Km2Pm_P[0]**2
      if(valM2>0):
         PhiW1_Km2Pm_M[0] = Sqrt(valM2)
      else:
         PhiW1_Km2Pm_M[0] = -999

      #Km Kp->Pip
      PhiW1_Kp2Pip_PE[0] = Kaon1_PE[0] + Sqrt(Kaon2_P[0]**2 + mPi**2)
      PhiW1_Kp2Pip_P[0]  = PhiW1_P[0]
      valM2 = PhiW1_Kp2Pip_PE[0]**2-PhiW1_Kp2Pip_P[0]**2
      if(valM2>0):
         PhiW1_Kp2Pip_M[0] = Sqrt(valM2)
      else:
         PhiW1_Kp2Pip_M[0] = -999


      #Km Kp->Pp
      PhiW1_Kp2Pp_PE[0] = Kaon1_PE[0] + Sqrt(Kaon2_P[0]**2 + mP**2)
      PhiW1_Kp2Pp_P[0]  = PhiW1_P[0]
      valM2 = PhiW1_Kp2Pp_PE[0]**2-PhiW1_Kp2Pp_P[0]**2
      if(valM2>0):
         PhiW1_Kp2Pp_M[0] = Sqrt(valM2)
      else:
         PhiW1_Kp2Pp_M[0] = -999



      #Pp Pm->Pim
      PhiW2_Pm2Pim_PE[0] = Kaon4_PE[0] + Sqrt(Kaon3_P[0]**2 + mPi**2)
      PhiW2_Pm2Pim_P[0]  = PhiW2_P[0]
      valM2 = PhiW2_Pm2Pim_PE[0]**2-PhiW2_Pm2Pim_P[0]**2
      if(valM2>0):
         PhiW2_Pm2Pim_M[0] = Sqrt(valM2)
      else:
         PhiW2_Pm2Pim_M[0] = -999


      #Pm Pp->Pip
      PhiW2_Pp2Pip_PE[0] = Kaon3_PE[0] + Sqrt(Kaon4_P[0]**2 + mPi**2)
      PhiW2_Pp2Pip_P[0]  = PhiW2_P[0]
      valM2 = PhiW2_Pp2Pip_PE[0]**2-PhiW2_Pp2Pip_P[0]**2
      if(valM2>0):
         PhiW2_Pp2Pip_M[0] = Sqrt(valM2)
      else:
         PhiW2_Pp2Pip_M[0] = -999


      #Pp Pm->Km
      PhiW2_Pm2Km_PE[0] = Kaon4_PE[0] + Sqrt(Kaon3_P[0]**2 + mK**2)
      PhiW2_Pm2Km_P[0]  = PhiW2_P[0]
      valM2 = PhiW2_Pm2Km_PE[0]**2-PhiW2_Pm2Km_P[0]**2
      if(valM2>0):
         PhiW2_Pm2Km_M[0] = Sqrt(valM2)
      else:
         PhiW2_Pm2Km_M[0] = -999


      #Pm Pp->Kp
      PhiW2_Pp2Kp_PE[0] = Kaon3_PE[0] + Sqrt(Kaon4_P[0]**2 + mK**2)
      PhiW2_Pp2Kp_P[0]  = PhiW2_P[0]
      valM2 = PhiW2_Pp2Kp_PE[0]**2-PhiW2_Pp2Kp_P[0]**2
      if(valM2>0):
         PhiW2_Pp2Kp_M[0] = Sqrt(valM2)
      else:
         PhiW2_Pp2Kp_M[0] = -999


      #Pp->Kp Pm->Km
      PhiW2_PpPm2KpKm_PE[0] = Sqrt(Kaon4_P[0]**2 + mK**2) + Sqrt(Kaon3_P[0]**2 + mK**2)
      PhiW2_PpPm2KpKm_P[0]  = PhiW2_P[0]
      valM2 = PhiW2_PpPm2KpKm_PE[0]**2-PhiW2_PpPm2KpKm_P[0]**2
      if(valM2>0):
         PhiW2_PpPm2KpKm_M[0] = Sqrt(valM2)
      else:
         PhiW2_PpPm2KpKm_M[0] = -999




      # 3 body
      PpKK_PE[0] = Kaon1_PE[0] + Kaon2_PE[0] + Kaon4_PE[0]
      PpKK_P[0]  = Sqrt((Kaon4_PX[0] + Kaon1_PX[0] + Kaon2_PX[0])**2 + \
                        (Kaon4_PY[0] + Kaon1_PY[0] + Kaon2_PY[0])**2 + \
                        (Kaon4_PZ[0] + Kaon1_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = PpKK_PE[0]**2-PpKK_P[0]**2
      if(valM2>0):
         PpKK_M[0] = Sqrt(valM2)
      else:
         PpKK_M[0] = -999


      PpKm_Kp2Pip_PE[0] = Kaon4_PE[0] + Kaon1_PE[0] + Sqrt(Kaon2_P[0]**2 + mPi**2)
      PpKm_Kp2Pip_P[0]  = PpKK_P[0]
      valM2 = PpKm_Kp2Pip_PE[0]**2-PpKm_Kp2Pip_P[0]**2
      if(valM2>0):
         PpKm_Kp2Pip_M[0] = Sqrt(valM2)
      else:
         PpKm_Kp2Pip_M[0] = -999


      
      PmKK_PE[0] = Kaon1_PE[0] + Kaon2_PE[0] + Kaon3_PE[0]
      PmKK_P[0]  = Sqrt((Kaon3_PX[0] + Kaon1_PX[0] + Kaon2_PX[0])**2 + \
                        (Kaon3_PY[0] + Kaon1_PY[0] + Kaon2_PY[0])**2 + \
                        (Kaon3_PZ[0] + Kaon1_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = PmKK_PE[0]**2-PmKK_P[0]**2
      if(valM2>0):
         PmKK_M[0] = Sqrt(valM2)
      else:
         PmKK_M[0] = -999

      PmKp_Km2Pim_PE[0] = Kaon3_PE[0] + Kaon2_PE[0] + Sqrt(Kaon1_P[0]**2 + mPi**2)
      PmKp_Km2Pim_P[0]  = PmKK_P[0]
      valM2 = PmKp_Km2Pim_PE[0]**2-PmKp_Km2Pim_P[0]**2
      if(valM2>0):
         PmKp_Km2Pim_M[0] = Sqrt(valM2)
      else:
         PmKp_Km2Pim_M[0] = -999



      PPKm_PE[0] = Kaon3_PE[0] + Kaon4_PE[0] + Kaon1_PE[0]
      PPKm_P[0]  = Sqrt((Kaon4_PX[0] + Kaon3_PX[0] + Kaon1_PX[0])**2 + \
                        (Kaon4_PY[0] + Kaon3_PY[0] + Kaon1_PY[0])**2 + \
                        (Kaon4_PZ[0] + Kaon3_PZ[0] + Kaon1_PZ[0])**2)
      valM2 = PPKm_PE[0]**2-PPKm_P[0]**2
      if(valM2>0):
         PPKm_M[0] = Sqrt(valM2)
      else:
         PPKm_M[0] = -999


      PmKm_Pp2Pip_PE[0] = Kaon3_PE[0] + Kaon1_PE[0] + Sqrt(Kaon4_P[0]**2 + mPi**2)
      PmKm_Pp2Pip_P[0]  = PPKm_P[0]
      valM2 = PmKm_Pp2Pip_PE[0]**2-PmKm_Pp2Pip_P[0]**2
      if(valM2>0):
         PmKm_Pp2Pip_M[0] = Sqrt(valM2)
      else:
         PmKm_Pp2Pip_M[0] = -999



      PPKp_PE[0] = Kaon3_PE[0] + Kaon4_PE[0] + Kaon2_PE[0]
      PPKp_P[0]  = Sqrt((Kaon4_PX[0] + Kaon3_PX[0] + Kaon2_PX[0])**2 + \
                        (Kaon4_PY[0] + Kaon3_PY[0] + Kaon2_PY[0])**2 + \
                        (Kaon4_PZ[0] + Kaon3_PZ[0] + Kaon2_PZ[0])**2)
      valM2 = PPKp_PE[0]**2-PPKp_P[0]**2
      if(valM2>0):
         PPKp_M[0] = Sqrt(valM2)
      else:
         PPKp_M[0] = -999


      PpKp_Pm2Pim_PE[0] = Kaon4_PE[0] + Kaon2_PE[0] + Sqrt(Kaon3_P[0]**2 + mPi**2)
      PpKp_Pm2Pim_P[0]  = PPKp_P[0]
      valM2 = PpKp_Pm2Pim_PE[0]**2-PpKp_Pm2Pim_P[0]**2
      if(valM2>0):
         PpKp_Pm2Pim_M[0] = Sqrt(valM2)
      else:
         PpKp_Pm2Pim_M[0] = -999



      PhiW1_M_Branch.Fill()
      PhiW1_P_Branch.Fill()
      PhiW1_PE_Branch.Fill()


      PhiW2_M_Branch.Fill()
      PhiW2_P_Branch.Fill()
      PhiW2_PE_Branch.Fill()


      Phi1ws_M_Branch.Fill()
      Phi1ws_P_Branch.Fill()
      Phi1ws_PE_Branch.Fill()



      Phi2ws_M_Branch.Fill()
      Phi2ws_P_Branch.Fill()
      Phi2ws_PE_Branch.Fill()



      Phi1_K2pi_M_Branch.Fill()
      Phi1_K2pi_P_Branch.Fill()
      Phi1_K2pi_PE_Branch.Fill()


      Phi2_K2pi_M_Branch.Fill()
      Phi2_K2pi_P_Branch.Fill()
      Phi2_K2pi_PE_Branch.Fill()


      Phi1_K2p_M_Branch.Fill()
      Phi1_K2p_P_Branch.Fill()
      Phi1_K2p_PE_Branch.Fill()


      Phi2_K2p_M_Branch.Fill()
      Phi2_K2p_P_Branch.Fill()
      Phi2_K2p_PE_Branch.Fill()


      Phi1_p2pi_M_Branch.Fill()
      Phi1_p2pi_P_Branch.Fill()
      Phi1_p2pi_PE_Branch.Fill()


      Phi2_p2pi_M_Branch.Fill()
      Phi2_p2pi_P_Branch.Fill()
      Phi2_p2pi_PE_Branch.Fill()


      Phi1_p2K_M_Branch.Fill()
      Phi1_p2K_P_Branch.Fill()
      Phi1_p2K_PE_Branch.Fill()


      Phi2_p2K_M_Branch.Fill()
      Phi2_p2K_P_Branch.Fill()
      Phi2_p2K_PE_Branch.Fill()


      PhiW1_Km2Pim_M_Branch.Fill()
      PhiW1_Km2Pim_P_Branch.Fill()
      PhiW1_Km2Pim_PE_Branch.Fill()


      PhiW1_Kp2Pip_M_Branch.Fill()
      PhiW1_Kp2Pip_P_Branch.Fill()
      PhiW1_Kp2Pip_PE_Branch.Fill()



      PpKK_M_Branch.Fill()
      PpKK_P_Branch.Fill()
      PpKK_PE_Branch.Fill()


      PpKm_Kp2Pip_M_Branch.Fill()
      PpKm_Kp2Pip_P_Branch.Fill()
      PpKm_Kp2Pip_PE_Branch.Fill()



      PmKK_M_Branch.Fill()
      PmKK_P_Branch.Fill()
      PmKK_PE_Branch.Fill()


      PmKp_Km2Pim_M_Branch.Fill()
      PmKp_Km2Pim_P_Branch.Fill()
      PmKp_Km2Pim_PE_Branch.Fill()




      #Kp Km->Pm
      PhiW1_Km2Pm_PE_Branch.Fill()
      PhiW1_Km2Pm_P_Branch.Fill()
      PhiW1_Km2Pm_M_Branch.Fill()


      #Km Kp->Pp
      PhiW1_Kp2Pp_PE_Branch.Fill()
      PhiW1_Kp2Pp_P_Branch.Fill()
      PhiW1_Kp2Pp_M_Branch.Fill()



      #Pp Pm->Pim
      PhiW2_Pm2Pim_PE_Branch.Fill()
      PhiW2_Pm2Pim_P_Branch.Fill()
      PhiW2_Pm2Pim_M_Branch.Fill()


      #Pm Pp->Pip
      PhiW2_Pp2Pip_PE_Branch.Fill()
      PhiW2_Pp2Pip_P_Branch.Fill()
      PhiW2_Pp2Pip_M_Branch.Fill()


      #Pp Pm->Km
      PhiW2_Pm2Km_PE_Branch.Fill()
      PhiW2_Pm2Km_P_Branch.Fill()
      PhiW2_Pm2Km_M_Branch.Fill()


      #Pm Pp->Kp
      PhiW2_Pp2Kp_PE_Branch.Fill()
      PhiW2_Pp2Kp_P_Branch.Fill()
      PhiW2_Pp2Kp_M_Branch.Fill()


      #Pp->Kp Pm->Km
      PhiW2_PpPm2KpKm_PE_Branch.Fill()
      PhiW2_PpPm2KpKm_P_Branch.Fill()
      PhiW2_PpPm2KpKm_M_Branch.Fill()



      PPKm_PE_Branch.Fill()
      PPKm_P_Branch.Fill()
      PPKm_M_Branch.Fill()


      PmKm_Pp2Pip_PE_Branch.Fill()
      PmKm_Pp2Pip_P_Branch.Fill()
      PmKm_Pp2Pip_M_Branch.Fill()



      PPKp_PE_Branch.Fill()
      PPKp_P_Branch.Fill()
      PPKp_M_Branch.Fill()


      PpKp_Pm2Pim_PE_Branch.Fill()
      PpKp_Pm2Pim_P_Branch.Fill()
      PpKp_Pm2Pim_M_Branch.Fill()


      

#   newtree.Print()
   newfile.Write()
   newfile.Close()




jobIDs = [556,557,565,566]
for jobID in jobIDs:
   for i in range(0,600):
      filename = "/afs/cern.ch/work/a/ausachov/LstLstTuples/"+str(jobID)+"/"+str(i)+"/triggeredTuple.root"
      newfilename = "/afs/cern.ch/work/a/ausachov/LstLstTuples/"+str(jobID)+"/"+str(i)+"/triggeredTupleAdd.root"
      try:
         fillNewTuple(filename,newfilename)
         print jobID, i, "processed \n"
      except:
         pass











