from ROOT import *
from array import *

gROOT.Reset()
gROOT.SetStyle("Plain")

chain = TChain("DecayTree")
chain.Add("PhiPi.root")

min_mass = 5366 - 200
max_mass = 5366 + 200

minMassJpsi = 5300
maxMassJpsi = 5450
binWidthJpsi = 10.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)

minMassDs  = 1850
maxMassDs  = 2200
binWidthDs = 10.
binNDs     = int((maxMassDs - minMassDs) / binWidthDs)

Phi1Pi_M   = RooRealVar("Phi1Pi_M", "Phi1Pi_M", minMassDs, maxMassDs, "MeV")
Phi2Pi_M   = RooRealVar("Phi2Pi_M", "Phi2Pi_M", minMassDs, maxMassDs, "MeV")
Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")

varPhiPi   = RooRealVar("varPhiPi", "varPhiPi", 1019.46+134.98)
varA1      = RooRealVar("varA1", "varA1", -1, 1)
varA2      = RooRealVar("varA2", "varA2", -1, 1)
varE1      = RooRealVar("varE1", "varE1", -1, 1)
varE2      = RooRealVar("varE2", "varE2", -1, 1)

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "(1+(@0-{0}.)/100.*@2)".format(minMassDs), 
	RooArgList(Phi1Pi_M, varPhiPi, varA1, varE1))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "(1+(@0-{0}.)/100.*@2)".format(minMassDs), 
	RooArgList(Phi1Pi_M, varPhiPi, varA2, varE2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "(1+(@0-{0}.)/100.*@2)".format(minMassDs), 
	RooArgList(Phi2Pi_M, varPhiPi, varA1, varE1))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "(1+(@0-{0}.)/100.*@2)".format(minMassDs), 
	RooArgList(Phi2Pi_M, varPhiPi, varA2, varE2))

varNDsDs         = RooRealVar("varNDsDs", "varNDsDs", -1e2, 1e9)
varNDsPhiPi      = RooRealVar("varNDsPhiPi", "varNDsPhiPi", -1e2, 1e9)
varNPhiPhiPiPi   = RooRealVar ("varNPhiPhiPiPi", "varNPhiPhiPiPi", -1e2, 1e9)

def fit(i, mass, sigma, step):
	cuts = 'PiP_IPCHI2_OWNPV > 9 && PiM_IPCHI2_OWNPV > 9 && Jpsi_M > {} && Jpsi_M < {}'.format(i, i + step)
	tree = chain.CopyTree(cuts)

	varDsMass  = RooRealVar("varDsMass", "varDsMass", mass)
	varDsSigma = RooRealVar("varDsSigma", "varDsSigma", sigma)

	pdfDs1 = RooGenericPdf("pdfDs1", "pdfDs1", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi1Pi_M, varDsMass, varDsSigma))
	pdfDs2 = RooGenericPdf("pdfDs2", "pdfDs2", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi2Pi_M, varDsMass, varDsSigma))

	pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfDs1, pdfDs2))
	pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfDs1, pdfRoot2A1))
	pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfDs2))
	pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

	pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfSS,    pdfSB,       pdfBS,       pdfBB),
                                                  RooArgList(varNDsDs, varNDsPhiPi, varNDsPhiPi, varNPhiPhiPiPi))

	dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Phi1Pi_M, Phi2Pi_M),"","")

	res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.Extended(True),RooFit.Offset(True),RooFit.NumCPU(8))
	res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.Extended(True),RooFit.Offset(True),RooFit.NumCPU(8))

	frame1 = Phi1Pi_M.frame(RooFit.Title("#Ds_{1} mass"))
	dsetFull.plotOn(frame1, RooFit.Binning(binNDs, minMassDs, maxMassDs))
	#pdfModel1.plotOn(frame1)
	pdfModel.plotOn(frame1)

	frame2 = Phi2Pi_M.frame(RooFit.Title("#Ds_{2} mass"))
	dsetFull.plotOn(frame2, RooFit.Binning(binNDs, minMassDs, maxMassDs))
	#pdfModel2.plotOn(frame2)
	pdfModel.plotOn(frame2)

	canvA = TCanvas("canvA", "canvA", 1000, 400)
	canvA.Divide(2,1)
	canvA.cd(1)
	frame1.Draw()
	canvA.cd(2)
	frame2.Draw()
	canvA.SaveAs("DsDs_plots/PhiPiMass_{}_{}.pdf".format(i, i + step))

	sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDsDs,varNDsPhiPi,varNPhiPhiPiPi))
	dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDsPhiPi_sw")

	frame3 = Jpsi_M.frame(RooFit.Title("DsDs mass"))
	dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
	canvB = TCanvas("canvB", "canvB", 800, 400)
	frame3.Draw()
	canvB.SaveAs("Jpsi_M.pdf")

	file = TFile("SplotDsDsPiPi.root","recreate")
	tree = dset.tree()
	tree.SetName("DecayTree")
	tree.SetTitle("DecayTree")
	tree.Write()
	file.Close()


	return varNDsDs.getValV(), varNDsDs.getErrorHi()

def fit_null():
	cuts = 'PiP_IPCHI2_OWNPV > 9 && PiM_IPCHI2_OWNPV > 9 && Jpsi_M > 5300 && Jpsi_M < 5450'
	tree = chain.CopyTree(cuts)

	varDsMass  = RooRealVar("varDsMass", "varDsMass", 1970, 1940, 2010)
	varDsSigma = RooRealVar("varDsSigma", "varDsSigma", 8, 4, 16)
	pdfDs1 = RooGenericPdf("pdfDs1", "pdfDs1", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi1Pi_M, varDsMass, varDsSigma))
	pdfDs2 = RooGenericPdf("pdfDs2", "pdfDs2", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi2Pi_M, varDsMass, varDsSigma))

	pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfDs1, pdfDs2))
	pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfDs1, pdfRoot2A1))
	pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfDs2))
	pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

	pdfModel = RooAddPdf("pdfModel", "pdfModel", RooArgList(pdfSS,    pdfSB,       pdfBS,       pdfBB),
                                                 RooArgList(varNDsDs, varNDsPhiPi, varNDsPhiPi, varNPhiPhiPiPi))

	dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Phi1Pi_M, Phi2Pi_M),"","")

	res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.Extended(True),RooFit.Offset(True),RooFit.NumCPU(8))
	res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.Extended(True),RooFit.Offset(True),RooFit.NumCPU(8))

	frame1 = Phi1Pi_M.frame(RooFit.Title("#Ds_{1} mass"))
	dsetFull.plotOn(frame1, RooFit.Binning(binNDs, minMassDs, maxMassDs))
	#pdfModel1.plotOn(frame1)
	pdfModel.plotOn(frame1)

	frame2 = Phi2Pi_M.frame(RooFit.Title("#Ds_{2} mass"))
	dsetFull.plotOn(frame2, RooFit.Binning(binNDs, minMassDs, maxMassDs))
	#pdfModel2.plotOn(frame2)
	pdfModel.plotOn(frame2)

	canvA = TCanvas("canvA", "canvA", 1000, 400)
	canvA.Divide(2,1)
	canvA.cd(1)
	frame1.Draw()
	canvA.cd(2)
	frame2.Draw()
	canvA.SaveAs("PhiPiMass.pdf")

	sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDsDs,varNDsPhiPi,varNPhiPhiPiPi))
	dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDsDs_sw")

	frame3 = Jpsi_M.frame(RooFit.Title("DsDs mass"))
	dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
	canvB = TCanvas("canvB", "canvB", 800, 400)
	frame3.Draw()
	canvB.SaveAs("Jpsi_M.pdf")

#	file = TFile("SplotDsDsPiPi.root","recreate")
#	tree = dset.tree()
#	tree.SetName("DecayTree")
##	tree.SetTitle("DecayTree")
#	tree.Write()
#	file.Close()

	return varDsMass.getValV(), varDsSigma.getValV() 

def main():
	NEtac = array('d', [])
	Error = array('d', [])
	x     = array('d', [])
	ex    = array('d', [])

	mass, sigma = fit_null()

	step = 1000

	for i in range(min_mass, max_mass, step):
	        try:
	            	N, Eh = fit(i, mass, sigma, step)
	        except:
	               	break
	        print(N, Eh)
	        NEtac.append(N)
	        Error.append(Eh)
	        x.append(i + step/2)
	        ex.append(step/2)

	print(i)

	file = open('DsDs.txt', 'w')
	for i in range(len(NEtac)):
	        l = (NEtac[i], Error[i], x[i], ex[i])
	        file.write(str(l) + '\n')
	file.close()
