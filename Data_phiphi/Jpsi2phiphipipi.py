YEARS  = ('2016', '2017')
MAGNET = ('Up', 'Down')

for year in YEARS:
	for magnet in MAGNET:
		if year == '2016':
			DaVinci_version = 'v42r6p1'
			stripping = 'Stripping28r1'
		elif year == '2017':
			DaVinci_version = 'v42r7p2'
			stripping = 'Stripping29r2'

		DaVinci_directory = '/afs/cern.ch/user/p/ppalinic/cmtuser/DaVinciDev_' + DaVinci_version

		import os
		if os.path.exists(DaVinci_directory):
			myApplication = GaudiExec()
			myApplication.directory = DaVinci_directory
		else:
			myApplication = prepareGaudiExec('DaVinci', DaVinci_version, myPath = '$Home/cmtuser')

		myJobName = 'Jpsi2PhiPhiPiPi_R16S28r1p1_Mag' + magnet + '_' + year

		myApplication.options = ['DaVinci_Jpsi2PhiPhiPiPi_' + year + '.py']

		data  = BKQuery('/LHCb/Collision16/Beam6500GeV-VeloClosed-Mag' + magnet + '/Real Data/Reco' + year[2:] + '/' + stripping + '/90000000/CHARM.MDST', dqflag=['OK']).getDataset()

		validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

		mySplitter = SplitByFiles(filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit = False )

		myBackend = Dirac()
		j = Job (
		         name         = myJobName,
		         application  = myApplication,
		         splitter     = mySplitter,
		         outputfiles  = [ DiracFile('Tuple' + year + '.root'),
		                         DiracFile('DVHistos' + year + '.root')
		                         ],
		         backend      = myBackend,
		         inputdata    = validData,
		         do_auto_resubmit = True,
		         parallel_submit = True
		         )
		j.submit(keep_going=True, keep_on_fail=True)