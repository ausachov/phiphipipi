from ROOT import *
from array import *

gROOT.Reset()
gROOT.SetStyle("Plain")

chain = TChain("DecayTree")
chain.Add("PhiPi.root")

min_mass = 5366 - 200
max_mass = 5366 + 200

minMassJpsi = 5300
maxMassJpsi = 5450
binWidthJpsi = 10.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)

minMassDs  = 1850
maxMassDs  = 2200
binWidthDs = 10.
binNDs     = int((maxMassDs - minMassDs) / binWidthDs)

Phi1Pi_M   = RooRealVar("Phi1Pi_M", "Phi1Pi_M", minMassDs, maxMassDs, "MeV")
Phi2Pi_M   = RooRealVar("Phi2Pi_M", "Phi2Pi_M", minMassDs, maxMassDs, "MeV")
Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")

varA2      = RooRealVar("varA2", "varA2", -1, 1)

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "(1+(@0-{0})/100.)".format(minMassDs), RooArgList(Phi1Pi_M))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "(1+(@0-{0})/100.*@1)".format(minMassDs), RooArgList(Phi1Pi_M, varA2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "(1+(@0-{0})/100.)".format(minMassDs), RooArgList(Phi2Pi_M))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "(1+(@0-{0})/100.*@1)".format(minMassDs), RooArgList(Phi2Pi_M, varA2))

varNDsDs         = RooRealVar("varNDsDs", "varNDsDs", 0, 1e9)
varNDsPhiPi      = RooRealVar("varNDsPhiPi", "varNDsPhiPi", 0, 1e9)
varNPhiPhiPiPi   = RooRealVar ("varNPhiPhiPiPi", "varNPhiPhiPiPi", 0, 1e9)

cuts = 'PiP_IPCHI2_OWNPV > 9 && PiM_IPCHI2_OWNPV > 9'
tree = chain.CopyTree(cuts)

varDsMass  = RooRealVar("varDsMass", "varDsMass", 1970, 1940, 2010)
varDsSigma = RooRealVar("varDsSigma", "varDsSigma", 8, 4, 16)
pdfDs1 = RooGenericPdf("pdfDs1", "pdfDs1", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi1Pi_M, varDsMass, varDsSigma))
pdfDs2 = RooGenericPdf("pdfDs2", "pdfDs2", "TMath::Gaus(@0,@1,@2)",RooArgList(Phi2Pi_M, varDsMass, varDsSigma))

pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfDs1, pdfDs2))
pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfDs1, pdfRoot2A1))
pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfDs2))
pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

pdfModel = RooAddPdf("pdfModel", "pdfModel", RooArgList(pdfSS, pdfSB, pdfBS, pdfBB), RooArgList(varNDsDs, varNDsPhiPi, varNDsPhiPi, varNPhiPhiPiPi))

dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Phi1Pi_M, Phi2Pi_M),"","")

res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True),RooFit.NumCPU(8))

frame1 = Phi1Pi_M.frame(RooFit.Title("#Ds_{1} mass"))
dsetFull.plotOn(frame1, RooFit.Binning(binNDs, minMassDs, maxMassDs))
pdfModel.plotOn(frame1)

frame2 = Phi2Pi_M.frame(RooFit.Title("#Ds_{2} mass"))
dsetFull.plotOn(frame2, RooFit.Binning(binNDs, minMassDs, maxMassDs))
pdfModel.plotOn(frame2)

canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()
canvA.SaveAs("PhiPiMass.pdf")

sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDsDs,varNDsPhiPi,varNPhiPhiPiPi))
dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDsDs_sw")

frame3 = Jpsi_M.frame(RooFit.Title("DsDs mass"))
dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
canvB = TCanvas("canvB", "canvB", 800, 400)
frame3.Draw()
canvB.SaveAs("Jpsi_M.pdf")