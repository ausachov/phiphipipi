def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    # TISTOS for Jpsi
    from Configurables import TupleToolTISTOS, TupleToolDecay
    from Configurables import LoKi__Hybrid__TupleTool
    tuple.addTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)"
    }
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)


    tuple.addTool(TupleToolDecay, name = 'Psi')
    tuple.Psi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPsi" ]
    tuple.Psi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPsi" )
    tuple.Psi.TupleToolTISTOSForPsi.Verbose=True
    tuple.Psi.TupleToolTISTOSForPsi.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Psi=LoKi__Hybrid__TupleTool("LoKi_Psi")
    LoKi_Psi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)"
    }    
    tuple.Psi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Psi"]
    tuple.Psi.addTool(LoKi_Psi)


    tuple.addTool(TupleToolDecay, name = 'f0')
    tuple.f0.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForf0" ]
    tuple.f0.addTool(TupleToolTISTOS, name="TupleToolTISTOSForf0" )
    tuple.f0.TupleToolTISTOSForf0.Verbose=True
    tuple.f0.TupleToolTISTOSForf0.TriggerList = myTriggerList
    LoKi_f0=LoKi__Hybrid__TupleTool("LoKi_f0")
    LoKi_f0.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)"
    }
    tuple.f0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_f0"]
    tuple.f0.addTool(LoKi_f0)




    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)


# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 "Hlt1AllL0Decision",
                 
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
    ]

year = "2016"
Jpsi2PpbarPiPiLocation = "/Event/CharmCompleteEvent/Phys/Ccbar2PpbarDetachedLine/Particles"

from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2PpbarPiPiLocation) 
inputData = MomentumScaling(inputData, Turbo = False, Year = year)



from PhysSelPython.Wrappers import (
                                    Selection,
                                    SelectionSequence,
                                    DataOnDemand,
                                    AutomaticData,
                                    SimpleSelection
                                    )
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from Configurables import CombineParticles, DaVinci, FilterInTrees, PrintDecayTree
from StandardParticles import StdAllNoPIDsPions as Pions

f0 = CombineParticles('Combine_f0',
                      DecayDescriptor='f_0(980) -> pi+ pi-',
                      DaughtersCuts={"pi+": "(PT > 250) & (TRGHOSTPROB < 0.4) & (PROBNNpi>0.1) & (BPVIPCHI2()>9)",
	           		     "pi-": "(PT > 250) & (TRGHOSTPROB < 0.4) & (PROBNNpi>0.1) & (BPVIPCHI2()>9)"},
                      CombinationCut="AALL",
                      MotherCut="ALL"
                      )
f0_sel = Selection('Sel_f0',
                   Algorithm=f0,
                   RequiredSelections=[Pions])


JpsiFromStr = FilterInTrees("JpsiFromStr",Code="('J/psi(1S)' == ABSID)")
Jpsi = Selection("Jpsi",Algorithm=JpsiFromStr,
                  RequiredSelections=[inputData])

psi_sel = SimpleSelection('Sel_psi',
                           ConfigurableGenerators.CombineParticles,
                           [Jpsi, f0_sel],
                           DecayDescriptor='psi(2S) -> J/psi(1S) f_0(980)',
                           DaughtersCuts={},
                           CombinationCut="(AM > 2.7*GeV) & (AM<100.*GeV)",
                           MotherCut="(BPVDLS > 7) & (VFASPF(VCHI2/VDOF) < 9)"
                           )
psi_seq = SelectionSequence('psi_Seq', TopSelection=psi_sel)



Psi2PpbarPiPiTuple = DecayTreeTuple("Psi2PpbarPiPiTuple")
Psi2PpbarPiPiTuple.Decay = "psi(2S) -> ^(J/psi(1S) -> ^p+ ^p~-) ^(f_0(980) ->^pi+ ^pi-)"
Psi2PpbarPiPiTuple.Inputs = [psi_seq.outputLocation()]





Psi2PpbarPiPiBranches = {
     "ProtonP" :  "psi(2S) -> (J/psi(1S) ->^p+ p~-) (f_0(980)-> pi+ pi-)"
    ,"ProtonM" :  "psi(2S) -> (J/psi(1S) -> p+^p~-) (f_0(980)-> pi+ pi-)"
    ,"Jpsi"    :  "psi(2S) ->^(J/psi(1S) -> p+ p~-) (f_0(980)-> pi+ pi-)"
    ,"PiP"     :  "psi(2S) -> (J/psi(1S) -> p+ p~-) (f_0(980)->^pi+ pi-)"
    ,"PiM"     :  "psi(2S) -> (J/psi(1S) -> p+ p~-) (f_0(980)-> pi+^pi-)"
    ,"f0"      :  "psi(2S) -> (J/psi(1S) -> p+ p~-)^(f_0(980)-> pi+ pi-)"
    ,"Psi"     :  "(psi(2S)-> (J/psi(1S) -> p+ p~-) (f_0(980)-> pi+ pi-))"
    }
fillTuple( Psi2PpbarPiPiTuple, Psi2PpbarPiPiBranches, myTriggerList )

#Psi2PpbarPiPiTuple.Psi.addTupleTool( 'TupleToolSubMass' )
#Psi2PpbarPiPiTuple.Psi.ToolList += [ "TupleToolSubMass" ]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.Substitution += ["p+ => pi+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.Substitution += ["p+ => K+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.Substitution += ["pi+ => K+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.Substitution += ["pi+ => p+"]

#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/pi+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/pi+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/K+"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/K+"]

#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => pi+/K-"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => pi+/p~-"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => K+/K-"]
#Psi2PpbarPiPiTuple.Psi.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => K+/p~-"]



"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2PpbarDetachedFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2PpbarDetachedLineDecision')"""
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Ccbar2PpbarDetachedFilters.filters('Ccbar2PpbarDetachedFilters')
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [psi_seq.sequence(),
                            Psi2PpbarPiPiTuple ]        # The algorithms
# MDST
DaVinci().InputType = "DST"
# DaVinci().RootInTES = "/Event/CharmCompleteEvent"

# Get Luminosity
DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20150724"
# DaVinci().CondDBtag = "cond-20161004"


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00069601_00000124_1.charmcompleteevent.dst'
#                       ], clear=True)

