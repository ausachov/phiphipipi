YEARS  = ['2016']
MAGNET = ['Up', 'Down']

import os
for year in YEARS:
  for magnet in MAGNET:
     if year == '2016':
        DaVinci_version = 'v42r6p1'
	stripping = 'Stripping28r1'
     elif year == '2017':
	DaVinci_version = 'v42r7p2'
	stripping = 'Stripping29r2'
     elif year == '2018':
        DaVinci_version = 'v44r3'
	stripping = 'Stripping34'

     DaVinci_directory = '/afs/cern.ch/user/a/ausachov/cmtuser/DaVinciDev_' + DaVinci_version
     if os.path.exists(DaVinci_directory):
        myApplication = GaudiExec()
        myApplication.directory = DaVinci_directory
     else:
        myApplication = prepareGaudiExec('DaVinci', DaVinci_version, myPath = '/afs/cern.ch/user/a/ausachov/cmtuser')

     myJobName = 'Psi2PpbarPiPi_'+stripping+'_Mag'+magnet+'_'+year
     myApplication.options = ['DaVinci_Psi2PpbarPiPi_'+year+'.py']
     data  = BKQuery('/LHCb/Collision'+ year[2:]+'/Beam6500GeV-VeloClosed-Mag' + magnet + '/Real Data/Reco' + year[2:] + '/' + stripping + '/90000000/CHARMCOMPLETEEVENT.DST', dqflag=['OK']).getDataset()
     validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])
     mySplitter = SplitByFiles(filesPerJob = 20, maxFiles = -1, ignoremissing = True, bulksubmit = False )

     myBackend = Dirac()
     j = Job (name         = myJobName,
	      application  = myApplication,
	      splitter     = mySplitter,
	      outputfiles  = [ DiracFile('Tuple.root'),
	                       DiracFile('DVHistos.root')
		             ],
	      backend      = myBackend,
	      inputdata    = validData,
              do_auto_resubmit = True,
              parallel_submit = True
             )
     j.submit(keep_going=True, keep_on_fail=True)
