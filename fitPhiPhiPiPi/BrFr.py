from ROOT import *
from array import *

filename = "../bin_fit_Ds.txt"

min_val_Etac = 3375
max_val_Etac = 3995

min_val_Bs = 5305
max_val_Bs = 5445

file = open(filename, "r")

lines = []
for line in file:
        lines.append(line)

NEtac     = array('d', [])
ErrorEtac = array('d', [])
x_Etac     = array('d', [])
exEtac    = array('d', [])

NBs     = array('d', [])
ErrorBs = array('d', [])
xBs     = array('d', [])
exBs    = array('d', [])

for line in lines:
        a = line.split(',')
        if min_val_Etac < float(a[2]) < max_val_Etac:
                NEtac.append(float(a[0]))
                ErrorEtac.append(float(a[1]))
                x_Etac.append(float(a[2]))
                exEtac.append(float(a[3]))
        if min_val_Bs < float(a[2]) < max_val_Bs:
                NBs.append(float(a[0]))
                ErrorBs.append(float(a[1]))
                xBs.append(float(a[2]))
                exBs.append(float(a[3]))

histEtac   = TH1D("histEtac", "histEtac", 61, 3380, 3990)
for i in range(len(NEtac)):
    nbin = (int(x_Etac[i]) - 3385) / 10
    #histEtac[nbin] = NEtac[i]
    histEtac.SetBinContent(nbin, NEtac[i])
    histEtac.SetBinError(nbin, ErrorEtac[i])
#   print(histEtac[nbin])

histEtac.SetBinError(61, 10)
histEtac.SetBinError(62, 10)

histBs   = TH1D("histBs", "histBs", 14, 5300, 5450)
for i in range(len(NBs)):
    nbinBs = (int(xBs[i]) - 5305) / 10
    histBs.SetBinContent(nbinBs, NBs[i])
    histBs.SetBinError(nbinBs, ErrorBs[i])
histBs.SetBinError(0, 10)
histBs.SetBinError(13, 10)
histBs.SetBinError(14, 10)

xEtac = RooRealVar("xEtac", "xEtac", 3375, 3995)
xBs   = RooRealVar("xBs",   "xBs",   5305, 5445)
#xEtac.setRange(3375, 3990)
datahistX = RooDataHist("datahistX", "datahistX", RooArgList(xEtac), histEtac)
#xEtac.setRange(5000, 5995)
datahistBs = RooDataHist("datahistBs", "datahistBs", RooArgList(xBs), histBs)

min_val = 3380

varMass3872    = RooRealVar("varMass3872", "varMass3872", 3872)
varSigma3872   = RooRealVar("varSigma3872", "varSigma3872", 8)
pdf3872        = RooGaussian("pdf3872", "pdf3872", xEtac, varMass3872, varSigma3872)
varMass3823    = RooRealVar("varMass3823", "varMass3823", 3823)
varSigma3823   = RooRealVar("varSigma3823", "varSigma3823", 8)
pdf3823        = RooGaussian("pdf3823", "pdf3823", xEtac, varMass3823, varSigma3823)
varMass3915    = RooRealVar("varMass3915", "varMass3915", 3915)
varSigma3915   = RooRealVar("varSigma3915", "varSigma3915", 8)
pdf3915        = RooGaussian("pdf3915", "pdf3915", xEtac, varMass3915, varSigma3915)
varMassEtac2S  = RooRealVar("varMassEtac2S", "varMassEtac2S", 3639.4)
varSigmaEtac2S = RooRealVar("varSigmaEtac2S", "varSigmaEtac2S", 8)
pdfEtac2S      = RooGaussian("pdfEtac2S", "pdfEtac2S", xEtac, varMassEtac2S, varSigmaEtac2S)
varMassChic0   = RooRealVar("varMasschic0", "varMasschic0", 3414.75)
varSigmaChic0  = RooRealVar("varSigmachic0", "varSigmachic0", 8)
pdfChi0        = RooGaussian("pdfChi0", "pdfChi0", xEtac, varMassChic0, varSigmaChic0)
varMassChic1   = RooRealVar("varMasschic1", "varMasschic1", 3510.66)
varSigmaChic1  = RooRealVar("varSigmachic1", "varSigmachic1", 8)
pdfChi1        = RooGaussian("pdfChi1", "pdfChi1", xEtac, varMassChic1, varSigmaChic1)
varMassChic2   = RooRealVar("varMasschic2", "varMasschic2", 3556.2)
varSigmaChic2  = RooRealVar("varSigmachic2", "varSigmachic2", 8)
pdfChi2        = RooGaussian("pdfChi2", "pdfChi2", xEtac, varMassChic2, varSigmaChic2)
varMassBs      = RooRealVar("varMassBs", "varMassBs", 5366.77)
varSigmaBs     = RooRealVar("varSigmaBs", "varSigmaBs", 10.18)
pdfBs          = RooGaussian("pdfBs", "pdfBs", xBs, varMassBs, varSigmaBs)

EE1 = RooRealVar("EE1","EE1",0,1e-2)
AE1 = RooRealVar("AE1","AE1",-1,100)
#EB1 = RooRealVar("EB1","EB1",1e-3,1e-2)
#AB1 = RooRealVar("AB1","AB1",-1,1e5)

pdfBkgX  = RooGenericPdf("pdfBkgX",  "pdfBkgX",  "TMath::Exp(-(@0-{0}.)*@1)*(1+(@0-{0}.)/620.*@2)*TMath::Sqrt(abs(@0 - {0}))".format(3375), RooArgList(xEtac,EE1,AE1))
pdfBkgBs = RooGenericPdf("pdfBkgBs", "pdfBkgBs", "0", RooArgList())

def fitnull():
  s_min, s_max = 0, 100

  NX3872_NBs = RooRealVar("NX3872_NBs", "NX3872_NBs", s_min, s_max)
  NX3823_NBs = RooRealVar("NX3823_NBs", "NX3823_NBs", s_min, s_max)
  NX3915_NBs = RooRealVar("NX3915_NBs", "NX3915_NBs", s_min, s_max)
  NChic0_NBs = RooRealVar("NChic0_NBs", "NChic0_NBs", s_min, s_max)
  NChic1_NBs = RooRealVar("NChic1_NBs", "NChic1_NBs", s_min, s_max)
  NChic2_NBs = RooRealVar("NChic2_NBs", "NChic2_NBs", s_min, s_max)
  NEtac2_NBs = RooRealVar("NEtac2_NBs", "NEtac2_NBs", s_min, s_max)
  NBs        = RooRealVar("NBs",        "NBs",        s_min, s_max)

  NX3823 = RooFormulaVar("NX3823", "NX3823", "@0*@1", RooArgList(NX3823_NBs, NBs))
  NX3915 = RooFormulaVar("NX3915", "NX3915", "@0*@1", RooArgList(NX3915_NBs, NBs))
  NEtac2 = RooFormulaVar("NEtac2", "NEtac2", "@0*@1", RooArgList(NEtac2_NBs, NBs))
  NChic0 = RooFormulaVar("NChic0", "NChic0", "@0*@1", RooArgList(NChic0_NBs, NBs))
  NChic1 = RooFormulaVar("NChic1", "NChic1", "@0*@1", RooArgList(NChic1_NBs, NBs))
  NChic2 = RooFormulaVar("NChic2", "NChic2", "@0*@1", RooArgList(NChic2_NBs, NBs))
  NX3872 = RooFormulaVar("NX3872", "NX3872", "@0*@1", RooArgList(NX3872_NBs, NBs))

  NBsBkg = RooRealVar("NBsBkg", "NBsBkg", 0, 1e5)
  NXBkg  = RooRealVar("NXBkg",  "NXBkg",  0, 1e5)

  pdfModelX  = RooAddPdf("pdfModelX", 
                         "pdfModelX", 
                         RooArgList(pdf3872, pdf3823, pdf3915, pdfEtac2S, pdfChi0, pdfChi1, pdfChi2, pdfBkgX), 
                         RooArgList(NX3872,  NX3823,  NX3915,  NEtac2,    NChic0,  NChic1,  NChic2,  NXBkg))

  pdfModelBs = RooAddPdf("pdfModelBs",
                         "pdfModelBs",
                          RooArgList(pdfBkgBs, pdfBs),
                          RooArgList(NBsBkg, NBs))

  nllX  = pdfModelX.createChi2(datahistX, RooFit.SumW2Error(True))
  nllBs = pdfModelBs.createChi2(datahistBs, RooFit.SumW2Error(True))

  nllsum = RooAddition("nllsum", "nllsum", RooArgList(nllX, nllBs))
  m = RooMinuit(nllsum)
  m.migrad()
  m.hesse()
  m.improve()
  m.minos(RooArgSet(NBs))
  m.fit("shr")
  m.fit("")
  m.migrad()
  m.hesse()
  return nllsum.getVal(), NBs.getVal()

def fit(value, valuebs):
  s_min, s_max = 0, 100

  NX3872_NBs = RooRealVar("NX3872_NBs", "NX3872_NBs", value)
  NX3823_NBs = RooRealVar("NX3823_NBs", "NX3823_NBs", s_min, s_max)
  NX3915_NBs = RooRealVar("NX3915_NBs", "NX3915_NBs", s_min, s_max)
  NChic0_NBs = RooRealVar("NChic0_NBs", "NChic0_NBs", s_min, s_max)
  NChic1_NBs = RooRealVar("NChic1_NBs", "NChic1_NBs", s_min, s_max)
  NChic2_NBs = RooRealVar("NChic2_NBs", "NChic2_NBs", s_min, s_max)
  NEtac2_NBs = RooRealVar("NEtac2_NBs", "NEtac2_NBs", s_min, s_max)
  NBs        = RooRealVar("NBs",        "NBs",        s_min, s_max)

  NX3823 = RooFormulaVar("NX3823", "NX3823", "@0*@1", RooArgList(NX3823_NBs, NBs))
  NX3915 = RooFormulaVar("NX3915", "NX3915", "@0*@1", RooArgList(NX3915_NBs, NBs))
  NEtac2 = RooFormulaVar("NEtac2", "NEtac2", "@0*@1", RooArgList(NEtac2_NBs, NBs))
  NChic0 = RooFormulaVar("NChic0", "NChic0", "@0*@1", RooArgList(NChic0_NBs, NBs))
  NChic1 = RooFormulaVar("NChic1", "NChic1", "@0*@1", RooArgList(NChic1_NBs, NBs))
  NChic2 = RooFormulaVar("NChic2", "NChic2", "@0*@1", RooArgList(NChic2_NBs, NBs))
  NX3872 = RooFormulaVar("NX3872", "NX3872", "@0*@1", RooArgList(NX3872_NBs, NBs))

  NBsBkg = RooRealVar("NBsBkg", "NBsBkg", 0, 1e5)
  NXBkg  = RooRealVar("NXBkg",  "NXBkg",  0, 1e5)

  pdfModelX  = RooAddPdf("pdfModelX", 
                         "pdfModelX", 
                         RooArgList(pdf3872, pdf3823, pdf3915, pdfEtac2S, pdfChi0, pdfChi1, pdfChi2, pdfBkgX), 
                         RooArgList(NX3872,  NX3823,  NX3915,  NEtac2,    NChic0,  NChic1,  NChic2,  NXBkg))

  pdfModelBs = RooAddPdf("pdfModelBs",
                         "pdfModelBs",
                          RooArgList(pdfBkgBs, pdfBs),
                          RooArgList(NBsBkg, NBs))

  nllX  = pdfModelX.createChi2(datahistX, RooFit.SumW2Error(True))
  nllBs = pdfModelBs.createChi2(datahistBs, RooFit.SumW2Error(True))

  nllsum = RooAddition("nllsum", "nllsum", RooArgList(nllX, nllBs))
  m = RooMinuit(nllsum)
  m.migrad()
  m.hesse()
  m.improve()
  m.minos(RooArgSet(NBs))
  m.fit("shr")
  m.fit("")
  m.migrad()
  m.hesse()

  #m.minos(RooArgSet(NX3872, NBs))

#  frame1 = xEtac.frame()
#  datahistX.plotOn(frame1)
#  pdfModelX.plotOn(frame1)

#  frame2 = xBs.frame()
#  datahistBs.plotOn(frame2)
#  pdfModelBs.plotOn(frame2)

#  canvA = TCanvas("canvA", "canvA", 1000, 400)
#  canvA.Divide(2,1)
#  canvA.cd(1)
#  frame1.Draw()
#  canvA.cd(2)
#  frame2.Draw()
#  canvA.SaveAs("plot.pdf")
  return nllsum.getVal()

chi2    = array('d', [])
x_coord = array('d', [])
value   = 0
chi2nullvalue, valuebs = fitnull()

chi2_value = chi2nullvalue
value = 0
index = 0
while abs(chi2_value - chi2nullvalue) < 9:
  if index > 100:
  	break
  index += 1
  x_coord.append(value)
  chi2_value = fit(value, valuebs)
  chi2.append(chi2_value - chi2nullvalue)
  value += 1

hist_low = 0
hist_up  = len(chi2)
binwidth = 1
nbins    = (hist_up - hist_low)/binwidth

canvas1 = TCanvas()

hist1 = TH1D("hist", "hist", nbins - 1, hist_low, hist_up)
hist1.SetXTitle("Signal value")
hist1.SetYTitle("Chi2")
gStyle.SetOptStat(000)

for i in range(len(chi2)):
    nbin = int(x_coord[i]) // binwidth
    hist1[nbin - hist_low] = chi2[i]

name = "X3872"

hist1.Draw()
canvas1.SaveAs("{}_chi2profile.pdf".format(name))
hist1.SaveAs("profile_chi2_{}.root".format(name))
file = open(name + '.txt', 'w')
for i in range(len(chi2)):
  li = (chi2[i], x_coord[i])
  file.write(str(li))

