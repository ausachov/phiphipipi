from ROOT import *
from array import *

filename = "../bin_fit.txt"

min_val_Etac = 3380
max_val_Etac = 3990

min_val_Bs = 5000
max_val_Bs = 5990

file = open(filename, "r")
lines = []
for line in file:
        lines.append(line)

NEtac     = array('d', [])
ErrorEtac = array('d', [])
x_Etac     = array('d', [])
exEtac    = array('d', [])

NBs     = array('d', [])
ErrorBs = array('d', [])
xBs     = array('d', [])
exBs    = array('d', [])

for line in lines:
        a = line.split(',')
        if min_val_Etac < float(a[2]) < max_val_Etac:
                NEtac.append(float(a[0]))
                ErrorEtac.append(float(a[1]))
                x_Etac.append(float(a[2]))
                exEtac.append(float(a[3]))
        if min_val_Bs < float(a[2]) < max_val_Bs:
                NBs.append(float(a[0]))
                ErrorBs.append(float(a[1]))
                xBs.append(float(a[2]))
                exBs.append(float(a[3]))

nbinsxEtac = (max_val_Etac - min_val_Etac) // 10
xlowEtac   = min(x_Etac) - 5
xupEtac    = max(x_Etac) + 5
histEtac   = TH1D("histEtac", "histEtac", nbinsxEtac, xlowEtac, xupEtac)
for i in range(len(NEtac) - 1):
    nbin = ((int(x_Etac[i]) + 5) % 1000) // 10
    nbin -= max_val_Etac % 1000 // 10 - nbinsxEtac
    #histEtac[nbin] = NEtac[i]
    histEtac.SetBinContent(nbin, NEtac[i])
    histEtac.SetBinError(nbin, ErrorEtac[i])
 #   print(histEtac[nbin])

nbinsxBs = (max_val_Bs - 3380) // 10
xlowBs   = 3380
xupBs    = max(xBs) + 5
histBs   = TH1D("histBs", "histBs", nbinsxBs, xlowBs, xupBs)
for i in range(len(NBs) - 1):
    nbinBs = ((int(xBs[i]) + 5) % 1000) // 10
    nbinBs-= max_val_Bs % 1000 // 10 - nbinsxBs
    histBs.SetBinContent(nbinBs, NBs[i])
    histBs.SetBinError(nbinBs+1, ErrorBs[i])

xEtac = RooRealVar("xEtac", "xEtac", 3375, 5995)
#xBs = RooRealVar("xBs", "xBs", 5005, 5995)

varMass3872    = RooRealVar("varMass3872", "varMass3872", 3872)
varSigma3872   = RooRealVar("varSigma3872", "varSigma3872", 5.45)
pdf3872        = RooGenericPdf("pdf3872", "pdf3872", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380), 
                             RooArgList(xEtac, varMass3872, varSigma3872))
varMass3823    = RooRealVar("varMass3823", "varMass3823", 3823)
varSigma3823   = RooRealVar("varSigma3823", "varSigma3823", 5.23)
pdf3823        = RooGenericPdf("pdf3823", "pdf3823", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380), 
                             RooArgList(xEtac, varMass3823, varSigma3823))
varMass3915    = RooRealVar("varMass3915", "varMass3915", 3915)
varSigma3915   = RooRealVar("varSigma3915", "varSigma3915", 5.64)
pdf3915        = RooGenericPdf("pdf3915", "pdf3915", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380), 
                             RooArgList(xEtac, varMass3915, varSigma3915))
varMassEtac2S  = RooRealVar("varMassEtac2S", "varMassEtac2S", 3639.4)
varSigmaEtac2S = RooRealVar("varSigmaEtac2S", "varSigmaEtac2S", 4.29)
pdfEtac2S      = RooGenericPdf("pdfEtac2S", "pdfEtac2S", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380), 
                             RooArgList(xEtac, varMassEtac2S, varSigmaEtac2S))
varMassChic0   = RooRealVar("varMasschic0", "varMasschic0", 3414.75)
varSigmaChic0  = RooRealVar("varSigmachic0", "varSigmachic0", 2.73)
pdfChi0        = RooGenericPdf("pdfChi0", "pdfChi0", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380),
                           RooArgList(xEtac, varMassChic0, varSigmaChic0))
varMassChic1   = RooRealVar("varMasschic1", "varMasschic1", 3510.66)
varSigmaChic1  = RooRealVar("varSigmachic1", "varSigmachic1", 3.48)
pdfChi1        = RooGenericPdf("pdfChi1", "pdfChi1", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380),
                           RooArgList(xEtac, varMassChic1, varSigmaChic1))
varMassChic2   = RooRealVar("varMasschic2", "varMasschic2", 3556.2)
varSigmaChic2  = RooRealVar("varSigmachic2", "varSigmachic2", 3.79)
pdfChi2        = RooGenericPdf("pdfChi2", "pdfChi2", "TMath::Gaus(@0, @1, @2)*TMath::Sqrt(@0 - {0} + 1)".format(3380),
                           RooArgList(xEtac, varMassChic2, varSigmaChic2))
varMassBs      = RooRealVar("varMassBs", "varMassBs", 5366.77)
varSigmaBs     = RooRealVar("varSigmaBs", "varSigmaBs", 10.18)
pdfBs          = RooGenericPdf("pdfBs", "pdfBs", "TMath::Gaus(@0, @1, @2)",
                           RooArgList(xEtac, varMassBs, varSigmaBs))

EE1 = RooRealVar("E1","E1",1)
AE1 = RooRealVar("A1","A1",1)
EB1 = RooRealVar("E1","E1",1)
AB1 = RooRealVar("A1","A1",1)

#pdfBkgX  = RooGenericPdf("pdfBkgX",  "pdfBkgX",  "TMath::Exp(-(@0-{0}.)*@1)*TMath::Sqrt(@0 - {0})".format(3380),RooArgList(xEtac,EE1,AE1))
pdfBkgX  = RooGenericPdf("pdfBkgX",  "pdfBkgX",  "TMath::Exp(-(@0-{0}.)*@1)*(1+(@0-{0}.)/100.*@2)*TMath::Sqrt(@0 - {0} + 1)".format(0),RooArgList(xEtac,EE1,AE1))
pdfBkgBs = RooGenericPdf("pdfBkgBs", "pdfBkgBs", "TMath::Exp(-(@0-{0}.)*@1)*(1+(@0-{0}.)/100.*@2)*TMath::Sqrt(@0 - {0} + 1)".format(5000),RooArgList(xEtac,EB1,AB1))

minMassBs = 5300

NX3823  = RooRealVar("NX3823",  "NX3823",  1)
NX3915  = RooRealVar("NX3915",  "NX3915",  1)
NEtac2S = RooRealVar("NEtac2S", "NEtac2S", 1)
NChi0   = RooRealVar("NChi0",   "NChi0",   1)
NChi1   = RooRealVar("NChi1",   "NChi1",   1)
NChi2   = RooRealVar("NChi2",   "NChi2",   1)
NX3872  = RooRealVar("NX3872",  "NX3872",  1)
NBs    = RooRealVar("varNBsX",  "varNBsX", 1)

varNBsBkg = RooRealVar("varNBsBkg", "varNBsBkg", 0, 1e2)
varNXBkg  = RooRealVar("varNXBkg", "varNXBkg", 0, 1e2)

pdfModelX  = RooAddPdf("pdfModelX", 
                       "pdfModelX", 
                       RooArgList(pdf3872, pdf3823, pdf3915, pdfEtac2S, pdfChi0, pdfChi1, pdfChi2, pdfBkgX), 
                       RooArgList(NX3872,  NX3823,  NX3915,  NEtac2S,   NChi0,   NChi1,   NChi2,   varNXBkg))

pdfModelBs = RooAddPdf("pdfModelBs",
                       "pdfModelBs",
                        RooArgList(pdfBkgX,   pdfBs), 
                        RooArgList(varNBsBkg, NBs))

pdfModel = RooAddPdf("pdfModel",
                     "pdfModel",
                     RooArgList(pdfModelX, pdfModelBs))

sample = RooCategory("sample", "sample")
sample.defineType("Bs")
sample.defineType("Etac")

combData = RooDataHist("combData",
                       "combined data",
                       RooArgList(xEtac),
                       RooFit.Index(sample),
                       RooFit.Import("Bs", histBs),
                       RooFit.Import("Etac", histEtac)
                       )

#combData = RooDataHist("combData", "combined data", RooArgList(xEtac), RooFit.Index(sample), RooFit.Import("Etac", histEtac))



simPdf = RooSimultaneous("simPdf", "simPdf", sample)
simPdf.addPdf(pdfModelX,  "Etac")
simPdf.addPdf(pdfModelBs, "Bs")

currentlist = RooLinkedList()
cmd = RooFit.Save(True)
currentlist.Add(cmd)
#cmd1 = RooFit.Extended(True)
#currentlist.Add(cmd1)
#cmd2 = RooFit.Offset(True)
#currentlist.Add(cmd2)
cmd3 = RooFit.NumCPU(8)
currentlist.Add(cmd3)
cmd4 = RooFit.SumW2Error(True)
currentlist.Add(cmd4)

simPdf.chi2FitTo(combData, currentlist)



#simPdf.fitTo(combData, currentlist)
#print('Start Fitting')
#pdfModel.fitTo(combData, RooFit.Save(True),RooFit.NumCPU(8), RooFit.Extended(True), RooFit.SumW2Error(True), RooFit.Range("R1,R2"))

frame1 = xEtac.frame(RooFit.Range(3380, 4000), RooFit.Title("#eta_{c}"))
combData.plotOn(frame1, RooFit.Cut("sample==sample::Etac"))
simPdf.plotOn(frame1,RooFit.Slice(sample,"Etac"),RooFit.ProjWData(RooArgSet(sample),combData))

frame2 = xEtac.frame(RooFit.Range(5000, 6000), RooFit.Title("B_{s}"))
combData.plotOn(frame2, RooFit.Cut("sample==sample::Bs"))
simPdf.plotOn(frame2,RooFit.Slice(sample,"Bs"),RooFit.ProjWData(RooArgSet(sample),combData))

canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()

print('NX3872 ' + str(NX3872.getVal()))
print('NX3823 ' + str(NX3823.getVal()))
print('NX3915 ' + str(NX3915.getVal()))
print('NEtac2S ' + str(NEtac2S.getVal()))
print('NChi0 ' + str(NChi0.getVal()))
print('NChi1 ' + str(NChi1.getVal()))
print('NChi2 ' + str(NChi2.getVal()))
print('varNXBkg ' + str(varNXBkg.getVal()))
print('varNBsBkg ' + str(varNBsBkg.getVal()))
print('NBs ' + str(NBs.getVal()))
#canvA.SaveAs("PhiPiMass.pdf")

#simPdf.plotOn(frame1, RooFit.Slice(sample, "Etac"), RooFit.ProjWData(sample, combData))
#combData.plotOn(frame1, RooFit.Cut("sample==sample::Etac"))
#combData.plotOn(frame1, RooFit.Slice(sample, "Etac"),ProjWData(sample,combData))#, RooFit.Binning(99, 3000, 4000))
#pdfModel.plotOn(frame1)
