from ROOT import *

Kaon_cuts = [#0,
			2000,
		#	4000,
		#	6000
			 ]

Etac_cuts = [#0,
		#	2500,
		#	5000,
			7500,
		#	10000		 
		]

ch = TChain("DecayTree")
ch.Add('/eos/user/a/ausachov/DataRunII_phiphipipi/phiphipipi_RunII_merged_635.root')
ch.Add('/eos/user/a/ausachov/DataRunII_phiphipipi/phiphipipi_RunII_merged_634.root')
ch.SetBranchStatus("*",0)
ch.SetBranchStatus("Etac_L0*",1)
ch.SetBranchStatus("Etac_Hlt*",1)
ch.SetBranchStatus("Etac_M",1)
ch.SetBranchStatus("nCandidate",1)
ch.SetBranchStatus("Etac_FDCHI2_OWNPV",1)
ch.SetBranchStatus("Etac_ENDVERTEX_CHI2",1)
ch.SetBranchStatus("Etac_PT",1)
ch.SetBranchStatus("Phi1_M",1)
ch.SetBranchStatus("Phi2_M",1)
ch.SetBranchStatus("Kaon1_PT", 1)
ch.SetBranchStatus("Kaon2_PT", 1)
ch.SetBranchStatus("Kaon3_PT", 1)
ch.SetBranchStatus("Kaon4_PT", 1)
ch.SetBranchStatus("Etac_PT", 1)

ch1 = TChain("DecayTree")
ch1.Add('MagDown2012_P8_MC.root')
ch1.Add('MagDown2012_P6_MC.root')
ch1.Add('MagUp2012_P8_MC.root')
ch1.Add('MagUp2012_P6_MC.root')
ch1.SetBranchStatus("*",0)
ch1.SetBranchStatus("JpsiL0HadronDecision_TOS*",1)
ch1.SetBranchStatus("JpsiL0Global_TIS*",1)
ch1.SetBranchStatus("JpsiHlt1TrackAllL0Decision_TOS",1)
ch1.SetBranchStatus("nCandidate",1)
ch1.SetBranchStatus("Jpsi_ENDVERTEX_CHI2",1)
ch1.SetBranchStatus("Jpsi_PT",1)
ch1.SetBranchStatus("Phi1_M",1)
ch1.SetBranchStatus("Phi2_M",1)
ch1.SetBranchStatus("Kaon1_PT", 1)
ch1.SetBranchStatus("Kaon2_PT", 1)
ch1.SetBranchStatus("Kaon3_PT", 1)
ch1.SetBranchStatus("Kaon4_PT", 1)
ch1.SetBranchStatus("Jpsi_PT", 1)
ch1.SetBranchStatus("JpsiHlt2IncPhiDecision_TOS", 1)
ch1.SetBranchStatus("Jpsi_FDCHI2_OWNPV", 1)

def fit_data(value, value1):
	list_of_cuts = ('(nCandidate == 0)',
		                '(Etac_L0HadronDecision_TOS == 1 || Etac_L0Global_TIS == 1)',
		                '(Etac_Hlt1TrackMVADecision_TOS == 1 || Etac_Hlt1TwoTrackMVADecision_TOS == 1 || Etac_Hlt1IncPhiDecision_TOS == 1)',
		                '(Etac_Hlt2PhiIncPhiDecision_TOS == 1)',
		                '(Etac_FDCHI2_OWNPV>100)',
		                '(Etac_ENDVERTEX_CHI2<20)',
		                '(Etac_PT>3000)',
		                '(Phi1_M-1020)<20', 
		                '(Phi2_M-1020)<20')
	plotcut_value = str(value)
	plotcut_value1 = str(value1)
	plotcut = ('Kaon1_PT > ' + plotcut_value,
               'Kaon2_PT > ' + plotcut_value,
               'Kaon3_PT > ' + plotcut_value,
               'Kaon4_PT > ' + plotcut_value,
               'Etac_PT > ' + plotcut_value1)
	cuts = ""
	for cut in list_of_cuts:
	            cuts += cut + ' && '
	for cut in plotcut:
                cuts += cut + ' && '
	cuts = cuts[:len(cuts) - 4]
	tree = ch.CopyTree(cuts)
	Etac_M = RooRealVar("Etac_M","M(#phi#phi)",2850,3300)
	dsetFull = RooDataSet("dsetFull", "dsetFull", tree, RooArgSet(Etac_M),"","")

	varEtacMass  = RooRealVar("varEtacMass", "varEtacMass", 2980, 2950, 3020)
	varSigma    = RooRealVar("varSigma", "varSigma", 8, 4, 16)
	varEtacGamma = RooRealVar("varEtacGamma", "varEtacGamma", 31.8)

	pdfEtac = RooGenericPdf("pdfEtac", "pdfEtac", "TMath::Voigt(@0-@1,@2,@3)",RooArgList(Etac_M, varEtacMass,varSigma,varEtacGamma))

	E1 = RooRealVar("E1","E1",0,1e-2)
	A1 = RooRealVar("A1","A1",0,-1,1)
	pdfBkg  = RooGenericPdf("pdfBkg", "pdfBkg", "TMath::Exp(-(@0-2950.)*@1)*(1+(@0-2950.)/100.*@2)",RooArgList(Etac_M,E1,A1))

	NEtac = RooRealVar("NEtac","NEtac",1e4,1e2,1e6)
	NBkg = RooRealVar("NBkg","NBkg",1e4,1e2,1e7)
	pdfModel = RooAddPdf("pdfModel","pdfModel", RooArgList(pdfEtac,pdfBkg), RooArgList(NEtac,NBkg))

	xframe = Etac_M.frame(RooFit.Title(""))
	dsetFull.plotOn(xframe,RooFit.Binning(45,2850,3300))
	pdfModel.fitTo(dsetFull,RooFit.Save(True),RooFit.Extended(),RooFit.Offset(True),RooFit.NumCPU(8))
	pdfModel.fitTo(dsetFull,RooFit.Save(True),RooFit.Extended(),RooFit.Offset(True),RooFit.NumCPU(8))
	
	pdfModel.plotOn(xframe)

	#c = TCanvas()
	#xframe.Draw()
	
	Etac_mass = varEtacMass.getValV()
	#Etac_sigma = varSigma.getValV()

	exp_A1 = A1.getValV()
	exp_E1 = E1.getValV()

	N_Bkg = NBkg.getValV() #4.17E04
	N_Etac = NEtac.getValV() #1.28E03

	return N_Bkg, exp_A1, exp_E1, Etac_mass

def fit_MC(value, value1):
	list_of_cuts = ('(nCandidate == 0)',
                '(JpsiL0HadronDecision_TOS == 1 || JpsiL0Global_TIS == 1)',
                '(JpsiHlt1TrackAllL0Decision_TOS == 1)',
                '(JpsiHlt2IncPhiDecision_TOS == 1)',
                '(Jpsi_FDCHI2_OWNPV>100)',
                '(Jpsi_ENDVERTEX_CHI2<20)',
                '(Jpsi_PT>3000)',
                '(Phi1_M-1020)<20', 
                '(Phi2_M-1020)<20')
	plotcut_value = str(value)
	plotcut_value1 = str(value1)
	plotcut = ('Kaon1_PT > ' + plotcut_value,
               'Kaon2_PT > ' + plotcut_value,
               'Kaon3_PT > ' + plotcut_value,
               'Kaon4_PT > ' + plotcut_value,
               'Jpsi_PT > ' + plotcut_value1)
	cuts = ""
	for cut in list_of_cuts:
	            cuts += cut + ' && '
	for cut in plotcut:
                cuts += cut + ' && '
	cuts = cuts[:len(cuts) - 4]
	tree1 = ch1.CopyTree(cuts)
	
	Jpsi_M = RooRealVar("Jpsi_M","M(#phi#phi)",2850,3300)
	print(tree1)
	dsetFull_Jpsi = RooDataSet("dsetFull_Jpsi", "dsetFull_Jpsi", tree1, RooArgSet(Jpsi_M),"","")

	varJpsiMass  = RooRealVar("varJpsiMass", "varJpsiMass", 2980, 2950, 3020)
	varJpsiSigma    = RooRealVar("varJpsiSigma", "varJpsiSigma", 8, 4, 16)
	varJpsiGamma = RooRealVar("varJpsiGamma", "varJpsiGamma", 31.8)

	pdfJpsi = RooGenericPdf("pdfJpsi", "pdfJpsi", "TMath::Voigt(@0-@1,@2,@3)",RooArgList(Jpsi_M, varJpsiMass,varJpsiSigma,varJpsiGamma))

	E2 = RooRealVar("E2","E2",0,1e-2)
	A2 = RooRealVar("A2","A2",0,-1,1)
	pdfBkg  = RooGenericPdf("pdfBkg", "pdfBkg", "TMath::Exp(-(@0-2950.)*@1)*(1+(@0-2950.)/100.*@2)",RooArgList(Jpsi_M,E2,A2))

	NJpsi = RooRealVar("NJpsi","NJ",1e4,1e2,1e6)
	NBkgj = RooRealVar("NBkgj","NBkgj",1e4,1e2,1e7)
	pdfModel1 = RooAddPdf("pdfModel1","pdfModel1", RooArgList(pdfJpsi,pdfBkg), RooArgList(NJpsi,NBkgj))

	xframe = Jpsi_M.frame(RooFit.Title(""))
	dsetFull_Jpsi.plotOn(xframe,RooFit.Binning(45,2850,3300))
	pdfModel1.fitTo(dsetFull_Jpsi,RooFit.Save(True),RooFit.Extended(),RooFit.Offset(True),RooFit.NumCPU(8))
	pdfModel1.fitTo(dsetFull_Jpsi,RooFit.Save(True),RooFit.Extended(),RooFit.Offset(True),RooFit.NumCPU(8))
	
	pdfModel1.plotOn(xframe)

	#c = TCanvas()
	#xframe.Draw()
	
	#Etac_mass = varJpsiMass.getValV()
	#Etac_sigma = varJpsiSigma.getValV()

	#exp_A1 = A2.getValV()
	#exp_E1 = E2.getValV()

	#N_Bkg = NBkgj.getValV() #4.17E04
	N_Etac = NJpsi.getValV() #1.28E03

	return N_Etac

def ans(N_Bkg_Sig, N_Etac_MC):
	from math import sqrt
	k = 30.5351
	return N_Etac_MC*k/sqrt(N_Bkg_Sig + N_Etac_MC*k)

def integral(exp_A1, exp_E1, a, b):
	from math import exp
	return -exp(-exp_E1*(b - 2950))*(exp_E1*exp_A1*(b - 2950) + exp_A1 + 100*exp_E1)/(100*exp_E1**2) + exp(-exp_E1*(a - 2950))*(exp_E1*exp_A1*(a - 2950) + exp_A1 + 100*exp_E1)/(100*exp_E1**2)

def background_near_signal(N_Bkg, Etac_mass, exp_A1, exp_E1):
	weight_full = 450
	gamma = 31.8
	a_gamma = Etac_mass - 2 * gamma
	b_gamma = Etac_mass + 2 * gamma
	N_Bkg_Sig = N_Bkg * integral(exp_A1, exp_E1, a_gamma, b_gamma)/integral(exp_A1, exp_E1, 2850, 3300)
	return N_Bkg_Sig

ansv = []

for value in Kaon_cuts:
	for value1 in Etac_cuts:
		N_Etac_MC = fit_MC(value, value1)
		N_Bkg, exp_A1, exp_E1, Etac_mass = fit_data(value, value1)
		print(value, value1)
		N_Bkg_Sig = background_near_signal(N_Bkg, Etac_mass, exp_A1, exp_E1)
		S = (value, value1, ans(N_Bkg_Sig, N_Etac_MC))
		ansv.append(S)

x = []
y = []
z = []
for char in ansv:
    x.append(char[0])
    y.append(char[1])
    z.append(char[2])

array = []
for i in range(len(Kaon_cuts)):
    array.append(z[i*len(Etac_cuts):i*len(Etac_cuts)+len(Etac_cuts)])

print(ansv)

def signif(i,j):
	return array[i][j]

from ROOT import *

c = TCanvas()
nBinsx = len(Kaon_cuts)
minx   = min(Kaon_cuts)
maxx   = max(Kaon_cuts)
nBinsy = len(Etac_cuts)
miny   = min(Etac_cuts)
maxy   = max(Etac_cuts)
hist = TH2F("h","h",nBinsx,minx,maxx,nBinsy,miny,maxy)
for i in range(nBinsx):
	for j in range(nBinsy):
   		hist.SetBinContent(i,j,signif(i,j))
hist.Draw("COLZ")

c.SaveAs('2dhist.pdf')