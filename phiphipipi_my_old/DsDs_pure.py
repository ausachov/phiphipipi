from ROOT import *
import numpy

gROOT.Reset()
gROOT.SetStyle("Plain")
#    gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
#gStyle.SetOptStat(000)
TProof.Open("")


#Type = "varNDiD"
#Type = "varNDDs"
Type = "varNDiDs"




w = RooWorkspace()

minMassJpsi = 3500
maxMassJpsi = 11500
#minMassJpsi = 5200
#maxMassJpsi = 5500
binWidthJpsi = 5
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassDs = 1920
maxMassDs = 2020
binWidthDs = 5.
binNDs = int((maxMassDs-minMassDs)/binWidthDs)





Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
Ds1_M = RooRealVar("Phi1PiP_Mass", "Phi1PiP_Mass", minMassDs, maxMassDs, "MeV")
Ds2_M = RooRealVar("Phi2PiM_Mass", "Phi2PiM_Mass", minMassDs, maxMassDs, "MeV")

    
chain = TChain("DecayTree")
chain.Add("Data/AllPhiPhiPiPi_Snd_Soft.root")
#chain.Add("Data/AllPhiPhiPiPi_Snd_Tight.root")
#chain.Add("Data/AllDsDsPiPi_Snd_Tight_ID03All.root")



dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Ds1_M, Ds2_M),"","")



varDsMass = RooRealVar("varDsMass", "varDsMass", 1970, 1940, 2010)
varDMass = RooRealVar("varDMass", "varDMass", 1870)


varSigma = RooRealVar("varSigma", "varSigma", 5, 1, 15)
varDsGamma = RooRealVar("varDsGamma", "varDsGamma", 0.01)
varPhiPi = RooRealVar ("varPhiPi", "varPhiPi", 1019.46+134.98)
    

varA1 = RooRealVar("varA1", "varA1", 0)
varA2 = RooRealVar("varA2", "varA2", 0)

varExp = RooRealVar("varExp", "varExp",0)
varExp2 = RooRealVar("varExp2", "varExp2",0)
    
pdfDs1 = RooGenericPdf("pdfDs1", "pdfDs1", "sqrt(@0-@1)*TMath::Gaus(@0,@2,@3)",RooArgList(Ds1_M, varPhiPi, varDsMass,varSigma))
pdfDs2 = RooGenericPdf("pdfDs2", "pdfDs2", "sqrt(@0-@1)*TMath::Gaus(@0,@2,@3)",RooArgList(Ds2_M, varPhiPi, varDsMass,varSigma))

pdfD1 = RooGenericPdf("pdfD1", "pdfD1", "sqrt(@0-@1)*TMath::Gaus(@0,@2,@3)",RooArgList(Ds1_M, varPhiPi, varDMass,varSigma))
pdfD2 = RooGenericPdf("pdfD2", "pdfD2", "sqrt(@0-@1)*TMath::Gaus(@0,@2,@3)",RooArgList(Ds2_M, varPhiPi, varDMass,varSigma))


pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "(1+(@1-@0)*@2)*TMath::Exp(-(@0-@1)*@3)", RooArgList(Ds1_M, varPhiPi,varA1,varExp))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "1+(@1-@0)*@2", RooArgList(Ds1_M, varPhiPi,varA2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "(1+(@1-@0)*@2)*TMath::Exp(-(@0-@1)*@3)", RooArgList(Ds2_M, varPhiPi,varA1,varExp))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "1+(@1-@0)*@2", RooArgList(Ds2_M, varPhiPi,varA2))
                          

pdfDsDs = RooProdPdf("pdfDsDs", "pdfDsDs", RooArgList(pdfDs1, pdfDs2))
pdfDsB = RooProdPdf("pdfDsB", "pdfDsB", RooArgList(pdfDs1, pdfRoot2A1))
pdfBDs = RooProdPdf("pdfBDs", "pdfBDs", RooArgList(pdfRoot1A1, pdfDs2))

pdfDD = RooProdPdf("pdfDD", "pdfDD", RooArgList(pdfD1, pdfD2))
pdfDB = RooProdPdf("pdfDB", "pdfDB", RooArgList(pdfD1, pdfRoot2A1))
pdfBD = RooProdPdf("pdfBDs", "pdfBD", RooArgList(pdfRoot1A1, pdfD2))


pdfDDs = RooProdPdf("pdfDDs", "pdfDDs", RooArgList(pdfD1, pdfDs2))
pdfDsD = RooProdPdf("pdfDsD", "pdfDsD", RooArgList(pdfDs1, pdfD2))

pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

                          
                          
varNDiDs = RooRealVar("varNDiDs", "varNDiDs", 0, 1e4)
varNDsPhiPi  = RooRealVar("varNDsPhiPi", "varNDsPhiPi", 0, 1e4)

varNDiD = RooRealVar("varNDiD", "varNDiD", 0, 1e4)
varNDPhiPi  = RooRealVar("varNDPhiPi", "varNDPhiPi", 0, 1e4)

varNDDs = RooRealVar("varNDDs", "varNDDs", 0, 1e4)

varNPhiPhiPiPi   = RooRealVar ("varNPhiPhiPiPi", "varNPhiPhiPiPi", 0, 1e4)
                          
                          
                          

#
#pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfDs1,pdfDs1,pdfRoot1A1,pdfD1,pdfD1,pdfRoot1A1,pdfD1,pdfDs1,pdfRoot1A2),
#                     RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNDiD,varNDPhiPi,varNDPhiPi,varNDDs,varNDDs,varNPhiPhiPiPi))
#pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfDs2,pdfRoot2A1,pdfDs2,pdfD2,pdfRoot2A1,pdfD2,pdfDs2,pdfD2,pdfRoot2A2),
#                      RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNDiD,varNDPhiPi,varNDPhiPi,varNDDs,varNDDs,varNPhiPhiPiPi))
#pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfDsDs,pdfDsB,pdfBDs,pdfDD,pdfDB,pdfBD,pdfDDs,pdfDsD,pdfBB),
#RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNDiD,varNDPhiPi,varNDPhiPi,varNDDs,varNDDs,varNPhiPhiPiPi))



pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfDs1,pdfDs1,pdfRoot1A1,pdfRoot1A2),
                      RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))
pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfDs2,pdfRoot2A1,pdfDs2,pdfRoot2A2),
                      RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfDsDs,pdfDsB,pdfBDs,pdfBB),
                     RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))

res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1),RooFit.Extended())
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1), RooFit.Minos(True))
varDsMass.setConstant(True)
varDMass.setConstant(True)
varSigma.setConstant(True)
varA1.setConstant(True)
varA2.setConstant(True)
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1),RooFit.Minos(True),RooFit.Extended())





frame1 = Ds1_M.frame(RooFit.Title("#Ds_{1} mass"))
dsetFull.plotOn(frame1, RooFit.Binning(binNDs, minMassDs, maxMassDs))
pdfModel1.plotOn(frame1)

frame2 = Ds2_M.frame(RooFit.Title("#Ds_{2} mass"))
dsetFull.plotOn(frame2, RooFit.Binning(binNDs, minMassDs, maxMassDs))
pdfModel2.plotOn(frame2)


canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()
canvA.SaveAs("PhiPhiPiPiMass.pdf")



histJpsiDsDs        = TH1F("histJpsiDsDs", "histJpsiDsDs", binNJpsi, minMassJpsi, maxMassJpsi);
histJpsiDsPhiPi     = TH1F("histJpsiDsPhiPi", "histJpsiDsPhiPi", binNJpsi, minMassJpsi, maxMassJpsi);
histJpsiPhiPiPhiPi  = TH1F("histJpsiPhiPiPhiPi", "histJpsiPhiPiPhiPi", binNJpsi, minMassJpsi, maxMassJpsi);



centralNDiDs = varNDiDs.getVal()
lower = varNDiDs.getVal()-10*varNDiDs.getError()
if lower<0:
    lower=0
varNDiDs.setRange(lower,varNDiDs.getVal()+10*varNDiDs.getError())


lower = varNDsPhiPi.getVal()-10*varNDsPhiPi.getError()
if lower<0:
    lower=0
varNDsPhiPi.setRange(lower,varNDsPhiPi.getVal()+10*varNDsPhiPi.getError())


lower = varNPhiPhiPiPi.getVal()-10*varNPhiPhiPiPi.getError()
if lower<0:
    lower=0
varNPhiPhiPiPi.setRange(lower,varNPhiPhiPiPi.getVal()+10*varNPhiPhiPiPi.getError())


#lower = varNDiD.getVal()-10*varNDiD.getError()
#if lower<0:
#    lower=0
#varNDiD.setRange(lower,varNDiD.getVal()+10*varNDiD.getError())
#
#
#lower = varNDPhiPi.getVal()-10*varNDPhiPi.getError()
#if lower<0:
#    lower=0
#varNDPhiPi.setRange(lower,varNDPhiPi.getVal()+10*varNDPhiPi.getError())
#
#
#lower = varNDDs.getVal()-10*varNDDs.getError()
#if lower<0:
#    lower=0
#varNDDs.setRange(lower,varNDDs.getVal()+10*varNDDs.getError())





getattr(w,'import')(Jpsi_M)
getattr(w,'import')(pdfModel)


#if Type=="varNDiDs":
#    poi = RooArgSet(w.var("varNDiDs"))
#    nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDDs"),w.var("varNDiD"))
#if Type=="varNDiD":
#    poi = RooArgSet(w.var("varNDiD"))
#    nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDDs"),w.var("varNDiDs"))
#if Type=="varNDDs":
#    poi = RooArgSet(w.var("varNDDs"))
#    nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDiDs"),w.var("varNDiD"))



poi = RooArgSet(w.var("varNDiDs"))
nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"))



mc = RooStats.ModelConfig("ModelConfig",w)
mc.SetPdf("pdfModel")
mc.SetParametersOfInterest(poi)
mc.SetObservables(RooArgSet(w.var("Jpsi_M"),w.var("Phi1PiP_Mass"),w.var("Phi2PiM_Mass")))
mc.SetNuisanceParameters(nuis)

getattr(w,'import')(mc)






##mcmc = RooStats.BayesianCalculator(dsetFull,mc)
##mcmc.SetConfidenceLevel(0.683)
#
#mcmc = RooStats.MCMCCalculator(dsetFull,mc)
sp = RooStats.SequentialProposal(0.1)
#mcmc.SetProposalFunction(sp)
#mcmc.SetConfidenceLevel(0.683)
#mcmc.SetLeftSideTailFraction(0.5)
#mcmc.SetNumIters(50000);
#mcmc.SetNumBurnInSteps(500)
#
##  // default is shortest
##mcmc.SetIntervalType(RooStats.MCMCInterval.kShortest)
#
#
###mcmc.SetShortestInterval()
##mcmc.SetLeftSideTailFraction(0.5)
##mcmc.SetNumIters(10000)
##mcmc.SetIntegrationType("")
##mcmc.SetScanOfPosterior(100)
#
#
#DsDsPOI = mc.GetParametersOfInterest()["varNDiDs"]
##DDPOI = mc.GetParametersOfInterest()["varNDiD"]
##DDsPOI = mc.GetParametersOfInterest()["varNDDs"]
#
#interval = mcmc.GetInterval()
#if ( not interval):
#    print "Error computing Bayesian interval - exit \n"
#
#  
#lowerLimit = interval.LowerLimit(DsDsPOI)
#upperLimit = interval.UpperLimit(DsDsPOI)
##lowerLimit = interval.LowerLimit()
##upperLimit = interval.UpperLimit()
#
#
#if lowerLimit>centralNDiDs:
#    print "central value is to close to zero"
#    mcmc.SetLeftSideTailFraction(0.)
#    mcmc.SetConfidenceLevel(0.683)
#    interval = mcmc.GetInterval()
##    lowerLimit = interval.LowerLimit()
##    upperLimit = interval.UpperLimit()
#    lowerLimit = interval.LowerLimit(DsDsPOI)
#    upperLimit = interval.UpperLimit(DsDsPOI)
#
#print "\n68% interval on ",varNDiDs.GetName()," is : [",lowerLimit,", ",upperLimit,"] "
#  
#
#canv = TCanvas("canv", "canv", 1000, 400)
#
#plot = RooStats.MCMCIntervalPlot(interval)
##plot = mcmc.GetPosteriorPlot()
#plot.Draw()
#canv.Draw()
#canv.SaveAs("bayesTest.pdf")

try:
    del mcmc, interval, mc, w
except:
    pass




X      = numpy.ndarray(binNJpsi)
Xerr   = numpy.ndarray(binNJpsi)

YDsDs      = numpy.ndarray(binNJpsi)
YDsDsErrLo = numpy.ndarray(binNJpsi)
YDsDsErrHi = numpy.ndarray(binNJpsi)

YDD      = numpy.ndarray(binNJpsi)
YDDErrLo = numpy.ndarray(binNJpsi)
YDDErrHi = numpy.ndarray(binNJpsi)

YDDs      = numpy.ndarray(binNJpsi)
YDDsErrLo = numpy.ndarray(binNJpsi)
YDDsErrHi = numpy.ndarray(binNJpsi)


for i in range(binNJpsi):
    
    print "Bin ",i," is processing\n"
    
    varNDiDs.setRange(0,1e4)
    varNDsPhiPi.setRange(0,1e4)
    varNPhiPhiPiPi.setRange(0,1e4)
    
    varNDiD.setRange(0,1e4)
    varNDPhiPi.setRange(0,1e4)
    
    varNDDs.setRange(0,1e4)
    
    massJpsiLo = minMassJpsi + i*binWidthJpsi
    massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi
    label =  "Jpsi_M>"+str(massJpsiLo)+"&&Jpsi_M<"+str(massJpsiHi)
    dset = RooDataSet(dsetFull.reduce(RooFit.Cut(label), RooFit.Name("dset"+str(i)), RooFit.Title("dset"+str(i))))

    res = pdfModel.fitTo(dset, RooFit.Save(True),RooFit.SumW2Error(True), RooFit.Extended(True), RooFit.PrintLevel(0),RooFit.Minos(True))
    res = pdfModel.fitTo(dset, RooFit.Save(True),RooFit.SumW2Error(True), RooFit.Extended(True), RooFit.PrintLevel(0),RooFit.Minos(True))

    EDM = res.edm()



    frame1b = Ds1_M.frame(RooFit.Title("#Ds_{1} mass in bin"))
    dset.plotOn(frame1b, RooFit.Binning(binNDs, minMassDs, maxMassDs))
    #pdfModel1.plotOn(frame1)
    pdfModel1.plotOn(frame1b)

    frame2b = Ds2_M.frame(RooFit.Title("#Ds_{2} mass in bin"))
    dset.plotOn(frame2b, RooFit.Binning(binNDs, minMassDs, maxMassDs))
    #pdfModel2.plotOn(frame2)
    pdfModel2.plotOn(frame2b)


    canvBin = TCanvas("canvBin", "canvBin", 1000, 400)
    canvBin.Divide(2,1)
    canvBin.cd(1)
    frame1b.Draw()
    canvBin.cd(2)
    frame2b.Draw()
    canvBin.SaveAs("binsFit/"+str(i)+".pdf")



    histJpsiDsDs.SetBinContent(i+1, varNDiDs.getVal())
    histJpsiDsDs.SetBinError(i+1, varNDiDs.getErrorHi())
                        
    histJpsiDsPhiPi.SetBinContent(i+1, varNDsPhiPi.getVal())
    histJpsiDsPhiPi.SetBinError(i+1, varNDsPhiPi.getErrorHi())


    histJpsiPhiPiPhiPi.SetBinContent(i+1, varNPhiPhiPiPi.getVal())
    histJpsiPhiPiPhiPi.SetBinError(i+1, varNPhiPhiPiPi.getErrorHi())


    centralNDiDs = varNDiDs.getVal()
    lower = varNDiDs.getVal()-10*varNDiDs.getError()
    if lower<0:
        lower=0
    varNDiDs.setRange(lower,varNDiDs.getVal()+10*varNDiDs.getError())


    lower = varNDsPhiPi.getVal()-10*varNDsPhiPi.getError()
    if lower<0:
        lower=0
    varNDsPhiPi.setRange(lower,varNDsPhiPi.getVal()+10*varNDsPhiPi.getError())


    lower = varNPhiPhiPiPi.getVal()-10*varNPhiPhiPiPi.getError()
    if lower<0:
        lower=0
    varNPhiPhiPiPi.setRange(lower,varNPhiPhiPiPi.getVal()+10*varNPhiPhiPiPi.getError())


#    centralNDiD = varNDiD.getVal()
#    lower = varNDiD.getVal()-10*varNDiD.getError()
#    if lower<0:
#        lower=0
#    varNDiD.setRange(lower,varNDiD.getVal()+10*varNDiD.getError())
#
#
#    lower = varNDPhiPi.getVal()-10*varNDPhiPi.getError()
#    if lower<0:
#        lower=0
#    varNDPhiPi.setRange(lower,varNDPhiPi.getVal()+10*varNDPhiPi.getError())
#
#
#    centralNDDs = varNDDs.getVal()
#    lower = varNDDs.getVal()-10*varNDDs.getError()
#    if lower<0:
#        lower=0
#    varNDDs.setRange(lower,varNDDs.getVal()+10*varNDDs.getError())


    w = RooWorkspace()
    getattr(w,'import')(Jpsi_M)
    getattr(w,'import')(pdfModel)

#    if Type=="varNDiDs":
#        poi = RooArgSet(w.var("varNDiDs"))
#        nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDDs"),w.var("varNDiD"))
#    if Type=="varNDiD":
#        poi = RooArgSet(w.var("varNDiD"))
#        nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDDs"),w.var("varNDiDs"))
#    if Type=="varNDDs":
#        poi = RooArgSet(w.var("varNDDs"))
#        nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"),w.var("varNDPhiPi"),w.var("varNDiDs"),w.var("varNDiD"))

    poi = RooArgSet(w.var("varNDiDs"))
    nuis = RooArgSet(w.var("varNDsPhiPi"),w.var("varNPhiPhiPiPi"))

    mc = RooStats.ModelConfig("ModelConfig",w)
    mc.SetPdf("pdfModel")
    mc.SetParametersOfInterest(poi)
    mc.SetObservables(RooArgSet(w.var("Jpsi_M"),w.var("Phi1PiP_Mass"),w.var("Phi2PiM_Mass")))
    mc.SetNuisanceParameters(nuis)

    getattr(w,'import')(mc)

    mcmc = RooStats.MCMCCalculator(dset,mc)
    mcmc.SetProposalFunction(sp)
    mcmc.SetConfidenceLevel(0.683)
    mcmc.SetLeftSideTailFraction(0.5)
    mcmc.SetNumIters(100000);
    mcmc.SetNumBurnInSteps(5000)


    centralSignal = w.var(Type).getVal()
    DsDsPOI = mc.GetParametersOfInterest()[Type]

    canvB = TCanvas("canvB", "canvB", 1000, 400)

    interval = mcmc.GetInterval()
    if ( not interval):
        print "Error computing Bayesian interval - exit \n"

    lowerLimitDsDs = interval.LowerLimit(DsDsPOI)
    upperLimitDsDs = interval.UpperLimit(DsDsPOI)
    if lowerLimitDsDs>centralSignal:
        print "central value is to close to zero"
        del interval
        mcmc.SetLeftSideTailFraction(0.)
        mcmc.SetConfidenceLevel(0.683)
        interval = mcmc.GetInterval()
        lowerLimitDsDs = interval.LowerLimit(DsDsPOI)
        upperLimitDsDs = interval.UpperLimit(DsDsPOI)
    plot = RooStats.MCMCIntervalPlot(interval)
    plot.Draw()
    canvB.Draw()
    canvB.SaveAs("bayes/"+Type+"_"+str(i)+".pdf")



          
    X[i]      = minMassJpsi + (i+0.5)*binWidthJpsi
    Xerr[i]   = 0.5*binWidthJpsi

    YDsDs[i]      = centralSignal
    YDsDsErrLo[i] = centralSignal-lowerLimitDsDs
    YDsDsErrHi[i] = upperLimitDsDs-centralSignal


    del mcmc,interval,mc,w, plot, canvB, DsDsPOI


          

DiDsPureGraph = TGraphAsymmErrors(binNJpsi,X,YDsDs,Xerr,Xerr,YDsDsErrLo,YDsDsErrHi)

file = TFile("All"+str(Type)+".root","recreate");
histJpsiDsDs.Write()
histJpsiDsPhiPi.Write()
histJpsiPhiPiPhiPi.Write()
DiDsPureGraph.Write()
file.Close()





