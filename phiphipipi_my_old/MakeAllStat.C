
//0 - Down
//1 - Up
void MakeAllStatSingle(int Type=2,int DuoType=1)
{
    TChain *DecayTree = new TChain("DecayTree");
    
    if(Type==0)
        DecayTree->Add("Reduced_MagDown/*.root");
    if(Type==1)
        DecayTree->Add("Reduced_MagUp/*.root");
    if(Type==2)
        DecayTree->Add("Reduced_MagDown/*.root");
        DecayTree->Add("Reduced_MagUp/*.root");

    
    
//    TCut cutKaonPt = "Kaon1_PT>500 && Kaon2_PT>500 && Kaon3_PT>500 && Kaon4_PT>500"; // added artif
//    TCut cutPiPt = "PiP_PT>500 && PiM_PT>500"; // 1500
//    TCut cutID = "Kaon1_ProbNNk>0.05 && Kaon2_ProbNNk>0.05 && Kaon3_ProbNNk>0.05 && Kaon4_ProbNNk>0.05"; // 15
//    TCut cutPiID = "PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2"; // 15
//    
//    TCut cutTrack = "Kaon1_TRACK_CHI2NDOF<5 && Kaon2_TRACK_CHI2NDOF<5 && Kaon3_TRACK_CHI2NDOF<5 && Kaon4_TRACK_CHI2NDOF<5"; // 5
//    TCut cutPiTrack = "PiP_TRACK_CHI2NDOF<5 && PiM_TRACK_CHI2NDOF<5"; // 5
//    
//    
//    TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<81"; // 9
//    
//    
//    TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
//    TCut trigger1Cut = "(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS || Jpsi_Hlt1IncPhiDecision_TOS)";
//    TCut trigger2Cut = "(Jpsi_Hlt2PhiIncPhiDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)";
 

    
    TCut cutID = "Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.1 && Kaon4_ProbNNk>0.1"; // 15
    TCut cutSnd = "Jpsi_FDCHI2_OWNPV>36 && PiP_IPCHI2_OWNPV>4 && PiM_IPCHI2_OWNPV>4";
    

    TCut totCut = cutID && cutSnd;   //Additional cut
    
    if(DuoType==1)
    {
        if(Type==0)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_Down_1.root","recreate");
        if(Type==1)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_Up_1.root","recreate");
        if(Type==2)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_1.root","recreate");
    }
    
    if(DuoType==2)
    {
        if(Type==0)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_Down_2.root","recreate");
        if(Type==1)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_Up_2.root","recreate");
        if(Type==2)
            TFile *newfile = new TFile("AllPhiPhiPiPi_Snd_2.root","recreate");
    }

    
    TTree *newtree = DecayTree->CopyTree(totCut);
    
    
    
    Int_t NEn=newtree->GetEntries();
    Double_t Branch_Value, PE1, PX1, PY1,PZ1, PE2, PX2, PY2,PZ2, PE, PX, PY,PZ, PhiPhiMass, Phi1PiPiMass, Phi2PiPiMass, Phi1PiPMass, Phi2PiPMass, Phi1PiMMass, Phi2PiMMass, PhiPhiPiPMass, PhiPhiPiMMass;
    Double_t  pe1, px1, py1,pz1, pe2, px2, py2,pz2,pe, px, py,pz, PiPiMass, PPiMass, PpPipMass, PiPMass, PipPhiPhiMass;
    newtree->SetBranchAddress("Jpsi_m_scaled",&Branch_Value);
    newtree->SetBranchAddress("Phi1_PE",&PE1);
    newtree->SetBranchAddress("Phi1_PX",&PX1);
    newtree->SetBranchAddress("Phi1_PY",&PY1);
    newtree->SetBranchAddress("Phi1_PZ",&PZ1);
    
    newtree->SetBranchAddress("Phi2_PE",&PE2);
    newtree->SetBranchAddress("Phi2_PX",&PX2);
    newtree->SetBranchAddress("Phi2_PY",&PY2);
    newtree->SetBranchAddress("Phi2_PZ",&PZ2);
    
    newtree->SetBranchAddress("PiP_PE",&pe1);
    newtree->SetBranchAddress("PiP_PX",&px1);
    newtree->SetBranchAddress("PiP_PY",&py1);
    newtree->SetBranchAddress("PiP_PZ",&pz1);
    
    newtree->SetBranchAddress("PiM_PE",&pe2);
    newtree->SetBranchAddress("PiM_PX",&px2);
    newtree->SetBranchAddress("PiM_PY",&py2);
    newtree->SetBranchAddress("PiM_PZ",&pz2);
    
    
    TBranch *PhiPhi_Mass_Branch = newtree->Branch("PhiPhi_Mass", &PhiPhiMass, "PhiPhi_Mass/D");
    TBranch *PiPi_Mass_Branch = newtree->Branch("PiPi_Mass", &PiPiMass, "PiPi_Mass/D");
    TBranch *Phi1PiPi_Mass_Branch = newtree->Branch("Phi1PiPi_Mass", &Phi1PiPiMass, "Phi1PiPi_Mass/D");
    TBranch *Phi2PiPi_Mass_Branch = newtree->Branch("Phi2PiPi_Mass", &Phi2PiPiMass, "Phi2PiPi_Mass/D");
    
    TBranch *Phi1PiP_Mass_Branch = newtree->Branch("Phi1PiP_Mass", &Phi1PiPMass, "Phi1PiP_Mass/D");
    TBranch *Phi2PiP_Mass_Branch = newtree->Branch("Phi2PiP_Mass", &Phi2PiPMass, "Phi2PiP_Mass/D");
    TBranch *Phi1PiM_Mass_Branch = newtree->Branch("Phi1PiM_Mass", &Phi1PiMMass, "Phi1PiM_Mass/D");
    TBranch *Phi2PiM_Mass_Branch = newtree->Branch("Phi2PiM_Mass", &Phi2PiMMass, "Phi2PiM_Mass/D");
    TBranch *PhiPhiPiP_Mass_Branch = newtree->Branch("PhiPhiPiP_Mass", &PhiPhiPiPMass, "PhiPhiPiP_Mass/D");
    TBranch *PhiPhiPiM_Mass_Branch = newtree->Branch("PhiPhiPiM_Mass", &PhiPhiPiMMass, "PhiPhiPiM_Mass/D");
    
    Double_t ppe,ppx,ppy,ppz;
    Double_t pee,pxx,pyy,pzz;
    Double_t ppe2,ppx2,ppy2,ppz2;
    Double_t pee2,pxx2,pyy2,pzz2;


    for (Int_t i=0; i<NEn; i++)
    {
        newtree->GetEntry(i);
        
        PE=PE1+PE2;
        PX=PX1+PX2;
        PY=PY1+PY2;
        PZ=PZ1+PZ2;
        PhiPhiMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
        
        pe=pe1+pe2;
        px=px1+px2;
        py=py1+py2;
        pz=pz1+pz2;
        PiPiMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);

        
        
        pe=PE1+pe1+pe2;
        px=PX1+px1+px2;
        py=PY1+py1+py2;
        pz=PZ1+pz1+pz2;
        Phi1PiPiMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        pe=PE2+pe1+pe2;
        px=PX2+px1+px2;
        py=PY2+py1+py2;
        pz=PZ2+pz1+pz2;
        Phi2PiPiMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);

     
        
        
        
        
        
        pe=PE2+pe1;
        px=PX2+px1;
        py=PY2+py1;
        pz=PZ2+pz1;
        if(DuoType==1)Phi2PiPMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        if(DuoType==2)Phi1PiPMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        pe=PE2+pe2;
        px=PX2+px2;
        py=PY2+py2;
        pz=PZ2+pz2;
        if(DuoType==1)Phi1PiMMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        if(DuoType==2)Phi2PiMMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        pe=PE1+pe1;
        px=PX1+px1;
        py=PY1+py1;
        pz=PZ1+pz1;
        if(DuoType==1)Phi1PiPMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        if(DuoType==2)Phi2PiPMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        
        pe=PE1+pe2;
        px=PX1+px2;
        py=PY1+py2;
        pz=PZ1+pz2;
        if(DuoType==1)Phi2PiMMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        if(DuoType==2)Phi1PiMMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);

        
        
        
        
        
        pe=PE1+PE2+pe1;
        px=PX1+PX2+px1;
        py=PY1+PY2+py1;
        pz=PZ1+PZ2+pz1;
        PhiPhiPiPMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        pe=PE1+PE2+pe2;
        px=PX1+PX2+px2;
        py=PY1+PY2+py2;
        pz=PZ1+PZ2+pz2;
        PhiPhiPiMMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        

        PhiPhi_Mass_Branch->Fill();
        PiPi_Mass_Branch->Fill();
        Phi1PiPi_Mass_Branch->Fill();
        Phi2PiPi_Mass_Branch->Fill();
        
        
        Phi1PiP_Mass_Branch->Fill();
        Phi2PiP_Mass_Branch->Fill();
        Phi1PiM_Mass_Branch->Fill();
        Phi2PiM_Mass_Branch->Fill();
        PhiPhiPiP_Mass_Branch->Fill();
        PhiPhiPiM_Mass_Branch->Fill();

    }
    
    newtree->Print();
    newfile->Write();
    
    delete DecayTree;
    delete newfile;
}


void MakeAllStat()
{
    MakeAllStatSingle(2,1);
}

void MakeAllStatDuo()
{
    MakeAllStatSingle(2,1);
    MakeAllStatSingle(2,2);
    
    TChain *DecayTree = new TChain("DecayTree");
    DecayTree->Add("AllPhiPhiPiPi_Snd_1.root");
    DecayTree->Add("AllPhiPhiPiPi_Snd_2.root");
    
    TFile *newfile = new TFile("AllPhiPhiPiPi_Snd.root","recreate");
    TTree *newtree = DecayTree->CopyTree("");

    newtree->Print();
    newfile->Write();
    
    delete DecayTree;
    delete newfile;

}