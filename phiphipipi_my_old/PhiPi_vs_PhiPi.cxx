void Draw()
{
    TChain *DecayTree = new TChain("DecayTree");
    DecayTree->Add("Data/AllPhiPhiPiPi_Snd_Soft.root");
   
    
//    newtree = DecayTree->CopyTree("Jpsi_M<5325 && Jpsi_M>5235");
    newtree = DecayTree->CopyTree("Jpsi_M>5325 && Jpsi_M<5400");
    
    
    Float_t minMass = 1400;
    Float_t maxMass  = 2500;
    Float_t binWidth = 2.5;
    
    int nBins = int((maxMass-minMass)/binWidth);
    
    
    
    
    Double_t m1p,m2p,m1m,m2m;
    newtree->SetBranchAddress("Phi1PiP_Mass",&m1p);
    newtree->SetBranchAddress("Phi1PiM_Mass",&m1m);
    newtree->SetBranchAddress("Phi2PiP_Mass",&m2p);
    newtree->SetBranchAddress("Phi2PiM_Mass",&m2m);
    
    
    Float_t x[10000000],y[10000000];
    int iterator = 0;
    
    for(int i=0;i<DecayTree->GetEntries();i++)
    {
        newtree->GetEntry(i);
        x[iterator]=m1p;y[iterator]=m2m;
        iterator++;
        x[iterator]=m2p;y[iterator]=m1m;
        iterator++;
    }
    
    int nPoints = iterator;
    
    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    
    TGraph* gr = new TGraph(nPoints,x,y);
    gr->SetMarkerStyle(7);
    gr->SetMarkerSize(0.1);
    gr->GetYaxis()->SetRangeUser(1200,3300);
    gr->GetXaxis()->SetLimits(1200,3300);
   
    gr->Draw("AP");
    
    c1->SaveAs("Ds_vs_Ds.png");
}


//void ReFill()
//{
//    TChain *DecayTree = new TChain("DecayTree");
//    DecayTree->Add("Data/AllPhiPhiPiPi_Snd_Tight.root");
//    
//    
//   
//    
//    Double_t m1p,m2p,m1m,m2m;
//    DecayTree->SetBranchAddress("Phi1PiP_Mass",&m1p);
//    DecayTree->SetBranchAddress("Phi1PiM_Mass",&m1m);
//    DecayTree->SetBranchAddress("Phi2PiP_Mass",&m2p);
//    DecayTree->SetBranchAddress("Phi2PiM_Mass",&m2m);
//    
//    
//    TTree* newtree = new TTree("DecayTree","DecayTree");
//    TBranch *Jpsi_M_Branch = newtree->Branch("PhiPiP_Mass_Branch", &PhiPiPMass, "Phi1PiP_Mass/D");
//    TBranch *PhiPiP_Mass_Branch = newtree->Branch("PhiPiP_Mass", &PhiPiPMass, "Phi1PiP_Mass/D");
//    TBranch *PhiPiM_Mass_Branch = newtree->Branch("PhiPiM_Mass_Branch", &PhiPiPMass, "Phi2PiP_Mass/D");
//
//}