from ROOT import *
import numpy

gROOT.Reset()
gROOT.SetStyle("Plain")
gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
#gStyle.SetOptStat(000)
TProof.Open("")




w = RooWorkspace()

minMassJpsi = 3400
#maxMassJpsi = 3600
maxMassJpsi = 11500
binWidthJpsi = 10
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassPhiPhi = 2900
maxMassPhiPhi = 3050
binWidthPhiPhi = 5.
binNPhiPhi = int((maxMassPhiPhi-minMassPhiPhi)/binWidthPhiPhi)

minMassPiPi = 530
maxMassPiPi = 2020
binWidthPiPi = 5.
binNPiPi = int((maxMassPiPi-minMassPiPi)/binWidthPiPi)





Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
PhiPhi_M = RooRealVar("PhiPhi_Mass", "PhiPhi_Mass", minMassPhiPhi, maxMassPhiPhi, "MeV")
PiPi_M = RooRealVar("PiPi_Mass", "PiPi_Mass", minMassPiPi, maxMassPiPi, "MeV")
    
chain = TChain("DecayTree")
chain.Add("Data/AllPhiPhiPiPi_Snd_Soft_1.root")
tree = chain.CopyTree("nCandidate<10")


#chain.Add("Data/AllPhiPhiPiPi_Snd_Tight.root")
#chain.Add("Data/AllDsDsPiPi_Snd_Tight_ID03All.root")



dsetFull = RooDataSet("dsetFull", "dsetFull", tree, RooArgSet(Jpsi_M, PhiPhi_M, PiPi_M),"","")


varSigma = RooRealVar("varSigma", "varSigma", 5, 1, 15)
varEtacMass = RooRealVar("varEtacMass", "varEtacMass", 2983, 2960, 3000)
varEtacGamma = RooRealVar("varEtacGamma", "varEtacGamma", 31.8)

radius = RooRealVar("radius", "radius", 1.5)
massa  = RooRealVar("massa", "massa", 497., "MeV")
massb  = RooRealVar("massb", "massb", 497., "MeV")

pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", PhiPhi_M, varEtacMass, varEtacGamma,RooFit.RooConst(0.), radius, massa, massb)
pdfEtacGaussN = RooGaussian("pdfEtacGaussN","pdfEtacGaussN",PhiPhi_M,RooFit.RooConst(0.),varSigma)
pdfEtacN = RooFFTConvPdf("pdfEtacN","pdfEtacN",PhiPhi_M,pdfEtacBWrel,pdfEtacGaussN)


TwoPhiMass = RooRealVar("TwoPhiMass","TwoPhiMass",2*1019.46)
varExp = RooRealVar("varExp", "varExp",0,1e-2)
pdfBg = RooGenericPdf("pdfBg", "pdfBg", "TMath::Exp(-(@0-@1)*@2)", RooArgList(PhiPhi_M, TwoPhiMass,varExp))




varNEtac = RooRealVar("varNEtac", "varNEtac", 0, 1e7)
varNBg   = RooRealVar("varNBg", "varNBg", 0, 1e7)
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfEtacN,pdfBg),RooArgList(varNEtac,varNBg))



res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1),RooFit.Extended(),RooFit.NumCPU(8))
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1), RooFit.Minos(True),RooFit.NumCPU(8))

varEtacMass.setConstant(True)
varSigma.setConstant(True)
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(1),RooFit.Minos(True),RooFit.Extended(),RooFit.NumCPU(8))



frame1 = PhiPhi_M.frame(RooFit.Title("PhiPhi_M"))
dsetFull.plotOn(frame1, RooFit.Binning(binNPhiPhi, minMassPhiPhi, maxMassPhiPhi))
pdfModel.plotOn(frame1)


canvA = TCanvas("canvA", "canvA", 1000, 400)
frame1.Draw()
canvA.SaveAs("PhiPhiMass_Fit.pdf")



histEtac = TH1F("histEtac", "histEtac", binNJpsi, minMassJpsi, maxMassJpsi);
histBg   = TH1F("histBg", "histBg",     binNJpsi, minMassJpsi, maxMassJpsi);



centralNEtac = varNEtac.getVal()
lower = varNEtac.getVal()-10*varNEtac.getError()
if lower<0:
    lower=0
varNEtac.setRange(lower,varNEtac.getVal()+10*varNEtac.getError())


getattr(w,'import')(Jpsi_M)
getattr(w,'import')(pdfModel)




poi = RooArgSet(w.var("varNEtac"))
nuis = RooArgSet(w.var("varNBg"),w.var("varExp"))
mc = RooStats.ModelConfig("ModelConfig",w)
mc.SetPdf("pdfModel")
mc.SetParametersOfInterest(poi)
mc.SetObservables(RooArgSet(w.var("Jpsi_M"),w.var("PhiPhi_Mass")))
mc.SetNuisanceParameters(nuis)

getattr(w,'import')(mc)


#mcmc = RooStats.BayesianCalculator(dsetFull,mc)
#mcmc.SetConfidenceLevel(0.683)

mcmc = RooStats.MCMCCalculator(dsetFull,mc)
sp = RooStats.SequentialProposal(0.1)
mcmc.SetProposalFunction(sp)
mcmc.SetConfidenceLevel(0.683)
mcmc.SetLeftSideTailFraction(0.5)
mcmc.SetNumIters(50000);
mcmc.SetNumBurnInSteps(500)

#  // default is shortest
#mcmc.SetIntervalType(RooStats.MCMCInterval.kShortest)
##mcmc.SetShortestInterval()
#mcmc.SetLeftSideTailFraction(0.5)
#mcmc.SetNumIters(10000)
#mcmc.SetIntegrationType("")
#mcmc.SetScanOfPosterior(100)


POI = mc.GetParametersOfInterest()["varNEtac"]

interval = mcmc.GetInterval()
if ( not interval):
    print "Error computing Bayesian interval - exit \n"

  
lowerLimit = interval.LowerLimit(POI)
upperLimit = interval.UpperLimit(POI)
#lowerLimit = interval.LowerLimit()
#upperLimit = interval.UpperLimit()


if lowerLimit>centralNEtac:
    print "central value is to close to zero"
    mcmc.SetLeftSideTailFraction(0.)
    mcmc.SetConfidenceLevel(0.683)
    interval = mcmc.GetInterval()
#    lowerLimit = interval.LowerLimit()
#    upperLimit = interval.UpperLimit()
    lowerLimit = interval.LowerLimit(POI)
    upperLimit = interval.UpperLimit(POI)

print "\n68% interval on ",varNEtac.GetName()," is : [",lowerLimit,", ",upperLimit,"] "
  

canv = TCanvas("canv", "canv", 1000, 400)

plot = RooStats.MCMCIntervalPlot(interval)
#plot = mcmc.GetPosteriorPlot()
plot.Draw()
canv.Draw()
canv.SaveAs("bayesEtacAll.pdf")

try:
    del mcmc, interval, mc, w
except:
    pass




X      = numpy.ndarray(binNJpsi)
Xerr   = numpy.ndarray(binNJpsi)

Y      = numpy.ndarray(binNJpsi)
YErrLo = numpy.ndarray(binNJpsi)
YErrHi = numpy.ndarray(binNJpsi)



for i in range(binNJpsi):
    
    print "Bin ",i," is processing\n"
    
    varNEtac.setRange(0,1e4)
    varNBg.setRange(0,1e4)

    
    massJpsiLo = minMassJpsi + i*binWidthJpsi
    massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi
    label =  "Jpsi_M>"+str(massJpsiLo)+"&&Jpsi_M<"+str(massJpsiHi)
    dset = RooDataSet(dsetFull.reduce(RooFit.Cut(label), RooFit.Name("dset"+str(i)), RooFit.Title("dset"+str(i))))

#    res = pdfModel.fitTo(dset, RooFit.Save(True),RooFit.SumW2Error(True), RooFit.Extended(True), RooFit.PrintLevel(0),RooFit.Minos(True),RooFit.NumCPU(8))
    res = pdfModel.fitTo(dset, RooFit.Save(True),RooFit.SumW2Error(True), RooFit.Extended(True), RooFit.PrintLevel(0),RooFit.Minos(True),RooFit.NumCPU(8))

    EDM = res.edm()



    frame1b = PhiPhi_M.frame(RooFit.Title("etac mass in bin"))
    dset.plotOn(frame1b, RooFit.Binning(binNPhiPhi, minMassPhiPhi, maxMassPhiPhi))
    pdfModel.plotOn(frame1b)


    canvBin = TCanvas("canvBin", "canvBin", 1000, 400)
    frame1b.Draw()
    canvBin.SaveAs("binsFitEtac/"+str(i)+".pdf")



    histEtac.SetBinContent(i+1, varNEtac.getVal())
    histEtac.SetBinError(i+1, varNEtac.getErrorHi())
                        
    histBg.SetBinContent(i+1, varNBg.getVal())
    histBg.SetBinError(i+1, varNBg.getErrorHi())



    centralNEtac = varNEtac.getVal()
    lower = varNEtac.getVal()-10*varNEtac.getError()
    if lower<0:
        lower=0
    varNEtac.setRange(lower,varNEtac.getVal()+10*varNEtac.getError())


    lower = varNBg.getVal()-10*varNBg.getError()
    if lower<0:
        lower=0
    varNBg.setRange(lower,varNBg.getVal()+10*varNBg.getError())


    lower = varExp.getVal()-10*varExp.getError()
    if lower<0:
        lower=0
    upper = varExp.getVal()+10*varExp.getError()
    if upper>0.1:
        upper=0.1
    varExp.setRange(lower,upper)




    w = RooWorkspace()
    getattr(w,'import')(Jpsi_M)
    getattr(w,'import')(pdfModel)


    poi = RooArgSet(w.var("varNEtac"))
    nuis = RooArgSet(w.var("varNBg"),w.var("varExp"))

    mc = RooStats.ModelConfig("ModelConfig",w)
    mc.SetPdf("pdfModel")
    mc.SetParametersOfInterest(poi)
    mc.SetObservables(RooArgSet(w.var("Jpsi_M"),w.var("PhiPhi_Mass")))
    mc.SetNuisanceParameters(nuis)

    getattr(w,'import')(mc)

    mcmc = RooStats.MCMCCalculator(dset,mc)
    mcmc.SetProposalFunction(sp)
    mcmc.SetConfidenceLevel(0.683)
    mcmc.SetLeftSideTailFraction(0.5)
    mcmc.SetNumIters(100000);
    mcmc.SetNumBurnInSteps(5000)


    centralSignal = w.var("varNEtac").getVal()
    POI = mc.GetParametersOfInterest()["varNEtac"]

    canvB = TCanvas("canvB", "canvB", 1000, 400)

    interval = mcmc.GetInterval()
    if ( not interval):
        print "Error computing Bayesian interval - exit \n"

    lowerLimitEtac = interval.LowerLimit(POI)
    upperLimitEtac = interval.UpperLimit(POI)
    if lowerLimitEtac>centralSignal:
        print "central value is to close to zero"
        del interval
        mcmc.SetLeftSideTailFraction(0.)
        mcmc.SetConfidenceLevel(0.683)
        interval = mcmc.GetInterval()
        lowerLimitEtac = interval.LowerLimit(POI)
        upperLimitEtac = interval.UpperLimit(POI)
    plot = RooStats.MCMCIntervalPlot(interval)
    plot.Draw()
    canvB.Draw()
    canvB.SaveAs("bayesEtac/etac_"+str(i)+".pdf")



          
    X[i]      = minMassJpsi + (i+0.5)*binWidthJpsi
    Xerr[i]   = 0.5*binWidthJpsi

    Y[i]      = centralSignal
    YErrLo[i] = centralSignal-lowerLimitEtac
    YErrHi[i] = upperLimitEtac-centralSignal

    del mcmc,interval,mc,w, plot, canvB, POI


          

EtacPureGraph = TGraphAsymmErrors(binNJpsi,X,Y,Xerr,Xerr,YErrLo,YErrHi)

file = TFile("AllEtac.root","recreate");
histEtac.Write()
histBg.Write()
EtacPureGraph.Write()
file.Close()





