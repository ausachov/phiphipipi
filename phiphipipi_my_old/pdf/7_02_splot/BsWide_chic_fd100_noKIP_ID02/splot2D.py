from ROOT import *

gROOT.Reset()
gROOT.SetStyle("Plain")
#    gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
#gStyle.SetOptStat(000)
TProof.Open("")




#B0 and Bs
minMassJpsi = 5200
maxMassJpsi = 5500

##B0
#minMassJpsi = 5250
#maxMassJpsi = 5310


##Bs
#minMassJpsi = 5310
#maxMassJpsi = 5420



##High1
#minMassJpsi = 5500
#maxMassJpsi = 7000


##High2
#minMassJpsi = 7000
#maxMassJpsi = 9000

##High3
#minMassJpsi = 9000
#maxMassJpsi = 11000

##ccbar
#minMassJpsi = 4580
#maxMassJpsi = 4680


binWidthJpsi = 5.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassPhi = 1009
maxMassPhi = 1031
binWidthPhi = 1.
binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi)



##etac
#minMassPhiPhi = 2880
#maxMassPhiPhi = 3080


#chic
minMassPhiPhi = 3400
maxMassPhiPhi = 3700

##ccbar
#minMassPhiPhi = 2880
#maxMassPhiPhi = 3980

binWidthPhiPhi = 10.
binNPhiPhi = int((maxMassPhiPhi-minMassPhiPhi)/binWidthPhiPhi)


#minMassPiPi = 400
#maxMassPiPi = 1200

#minMassPiPi = 1200
#maxMassPiPi = 2000

minMassPiPi = 400
maxMassPiPi = 2000

binWidthPiPi = 10.
binNPiPi = int((maxMassPiPi-minMassPiPi)/binWidthPiPi)

    
PhiMass = 1019.46
Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
Phi1_M = RooRealVar("Phi1_M", "Phi1_M", minMassPhi, maxMassPhi, "MeV")
Phi2_M = RooRealVar("Phi2_M", "Phi2_M", minMassPhi, maxMassPhi, "MeV")
PhiPhi_Mass = RooRealVar("PhiPhi_Mass", "PhiPhi_Mass", minMassPhiPhi, maxMassPhiPhi, "MeV")
PiPi_Mass   = RooRealVar("PiPi_Mass", "PiPi_Mass", minMassPiPi, maxMassPiPi, "MeV")
    

    
chain = TChain("DecayTree")
chain.Add("Data/AllPhiPhiPiPi_Snd_Soft.root")
#chain.Add("Data/AllPhiPhiPiPi_Snd_Tight.root")
#chain.Add("Data/AllPhiPhiPiPi_Snd_Tight_ID03All.root")



dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Phi1_M, Phi2_M, PhiPhi_Mass, PiPi_Mass),"","")



varPhiMass = RooRealVar("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1)


varSigma = RooRealVar("varSigma", "varSigma", 1, 0.1, 4)
varPhiGamma = RooRealVar("varPhiGamma", "varPhiGamma", 4.26)
var2KMass = RooRealVar ("var2KMass", "var2KMass", 493.67*2)
    
    
varA1 = RooRealVar("varA1", "varA1", 0,1)
varA2 = RooRealVar("varA2", "varA2", 0,1)
    
    
pdfPhi1 = RooGenericPdf("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Phi1_M, var2KMass, varPhiMass,varSigma,varPhiGamma))
pdfPhi2 = RooGenericPdf("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Phi2_M, var2KMass, varPhiMass,varSigma,varPhiGamma))

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_M, var2KMass,varA1))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_M, var2KMass,varA2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_M, var2KMass,varA1))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_M, var2KMass,varA2))
                          

pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfPhi1, pdfPhi2))
pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfPhi1, pdfRoot2A1))
pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfPhi2))
pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

                          
                          
varNDiPhi = RooRealVar("varNDiPhi", "varNDiPhi", 0, 1e7)
varNPhiK  = RooRealVar("varNPhiK", "varNPhiK", 0, 1e7)
varNDiK   = RooRealVar ("varNDiK", "varNDiK", 0, 1e7)
                          
                          
                          

pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2),
                                              RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK))
pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2),
                                              RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK))
                          
                          
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK))
                          
                          
                          
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
varPhiMass.setConstant(True)
varSigma.setConstant(True)
varA1.setConstant(True)
varA2.setConstant(True)


frame1 = Phi1_M.frame(RooFit.Title("#phi_{1} mass"))
dsetFull.plotOn(frame1, RooFit.Binning(binNPhi, minMassPhi, maxMassPhi))
#pdfModel1.plotOn(frame1)
pdfModel.plotOn(frame1)

frame2 = Phi2_M.frame(RooFit.Title("#phi_{2} mass"))
dsetFull.plotOn(frame2, RooFit.Binning(binNPhi, minMassPhi, maxMassPhi))
#pdfModel2.plotOn(frame2)
pdfModel.plotOn(frame2)


canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()
canvA.SaveAs("diKMass.pdf")








sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDiPhi,varNPhiK,varNDiK))
dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDiPhi_sw")


frame3 = Jpsi_M.frame(RooFit.Title("phiphipipi mass"))
dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
canvB = TCanvas("canvB", "canvB", 800, 400)
frame3.Draw()
canvB.SaveAs("Jpsi_M.pdf")


frame4 = PhiPhi_Mass.frame(RooFit.Title("#phi#phi mass"))
dset.plotOn(frame4,RooFit.Binning(binNPhiPhi, minMassPhiPhi, maxMassPhiPhi),RooFit.DataError(RooAbsData.SumW2))
canvC = TCanvas("canvC", "canvC", 800, 400)
frame4.Draw()
canvC.SaveAs("PhiPhiMass.pdf")


frame5 = PiPi_Mass.frame(RooFit.Title("#pi#pi mass"))
dset.plotOn(frame5,RooFit.Binning(binNPiPi, minMassPiPi, maxMassPiPi),RooFit.DataError(RooAbsData.SumW2))
canvD = TCanvas("canvD", "canvD", 800, 400)
frame5.Draw()
canvD.SaveAs("PiPiMass.pdf")


file = TFile("SplotPhiPhiPiPi.root","recreate")
tree = dset.tree()
tree.SetName("DecayTree")
tree.SetTitle("DecayTree")
tree.Write()
file.Close()



