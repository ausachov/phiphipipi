from ROOT import *


gROOT.Reset()
gROOT.SetStyle("Plain")
#    gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
#gStyle.SetOptStat(000)
TProof.Open("")


minMassJpsi = 3800
maxMassJpsi = 6000
#minMassJpsi = 5200
#maxMassJpsi = 5500




##B0 and Bs
#minMassJpsi = 5200
#maxMassJpsi = 5500

##B0
#minMassJpsi = 5250
#maxMassJpsi = 5310


##Bs
#minMassJpsi = 5310
#maxMassJpsi = 5420



##High1
#minMassJpsi = 5500
#maxMassJpsi = 7000


##High2
#minMassJpsi = 7000
#maxMassJpsi = 9000

##High3
#minMassJpsi = 9000
#maxMassJpsi = 11000

##ccbar
#minMassJpsi = 3600
#maxMassJpsi = 4100


binWidthJpsi = 10.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassDs = 1920
maxMassDs = 2020
binWidthDs = 2.5
binNDs = int((maxMassDs-minMassDs)/binWidthDs)





    
DsMass = 1019.46
Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
Ds1_M = RooRealVar("Phi1PiP_Mass", "Phi1PiP_Mass", minMassDs, maxMassDs, "MeV")
Ds2_M = RooRealVar("Phi2PiM_Mass", "Phi2PiM_Mass", minMassDs, maxMassDs, "MeV")

    
chain = TChain("DecayTree")
chain.Add("Data/AllPhiPhiPiPi_Snd_Soft.root")
#chain.Add("Data/AllPhiPhiPiPi_Snd_Tight.root")
#chain.Add("Data/AllDsDsPiPi_Snd_Tight_ID03All.root")



dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Ds1_M, Ds2_M),"","")



varDsMass = RooRealVar("varDsMass", "varDsMass", 1970, 1940, 2010)


varSigma = RooRealVar("varSigma", "varSigma", 5, 1, 15)
#varSigma = RooRealVar("varSigma", "varSigma", 7)
varDsGamma = RooRealVar("varDsGamma", "varDsGamma", 0.1)
varPhiPi = RooRealVar ("varPhiPi", "varPhiPi", 1019.46+134.98)
    
    
#varA1 = RooRealVar("varA1", "varA1", 0.0001,0,1)
#varA2 = RooRealVar("varA2", "varA2", 0.0001,0,1)
varA1 = RooRealVar("varA1", "varA1", 0)
varA2 = RooRealVar("varA2", "varA2", 0)

    
pdfDs1 = RooGenericPdf("pdfDs1", "pdfDs1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Ds1_M, varPhiPi, varDsMass,varSigma,varDsGamma))
pdfDs2 = RooGenericPdf("pdfDs2", "pdfDs2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Ds2_M, varPhiPi, varDsMass,varSigma,varDsGamma))

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "1+(@1-@0)*@2", RooArgList(Ds1_M, varPhiPi,varA1))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "1+(@1-@0)*@2", RooArgList(Ds1_M, varPhiPi,varA2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "1+(@1-@0)*@2", RooArgList(Ds2_M, varPhiPi,varA1))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "1+(@1-@0)*@2", RooArgList(Ds2_M, varPhiPi,varA2))
                          

pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfDs1, pdfDs2))
pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfDs1, pdfRoot2A1))
pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfDs2))
pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

                          
                          
varNDiDs = RooRealVar("varNDiDs", "varNDiDs", 0, 1e9)
varNDsPhiPi  = RooRealVar("varNDsPhiPi", "varNDsPhiPi", 0, 1e9)
varNPhiPhiPiPi   = RooRealVar ("varNPhiPhiPiPi", "varNPhiPhiPiPi", 0, 1e9)
                          
                          
                          

pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfDs1,pdfDs1,pdfRoot1A1,pdfRoot1A2),
                                              RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))
pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfDs2,pdfRoot2A1,pdfDs2,pdfRoot2A2),
                                              RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))
                          
                          
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiDs,varNDsPhiPi,varNDsPhiPi,varNPhiPhiPiPi))
                          
                          
                          
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
varDsMass.setConstant(True)
varSigma.setConstant(True)
varA1.setConstant(True)
varA2.setConstant(True)


frame1 = Ds1_M.frame(RooFit.Title("#Ds_{1} mass"))
dsetFull.plotOn(frame1, RooFit.Binning(binNDs, minMassDs, maxMassDs))
#pdfModel1.plotOn(frame1)
pdfModel.plotOn(frame1)

frame2 = Ds2_M.frame(RooFit.Title("#Ds_{2} mass"))
dsetFull.plotOn(frame2, RooFit.Binning(binNDs, minMassDs, maxMassDs))
#pdfModel2.plotOn(frame2)
pdfModel.plotOn(frame2)


canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()
canvA.SaveAs("PhiPhiPiPiMass.pdf")








sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDiDs,varNDsPhiPi,varNPhiPhiPiPi))
dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDsPhiPi_sw")


frame3 = Jpsi_M.frame(RooFit.Title("DsDs mass"))
dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
canvB = TCanvas("canvB", "canvB", 800, 400)
frame3.Draw()
canvB.SaveAs("Jpsi_M.pdf")




file = TFile("SplotDsDsPiPi.root","recreate")
tree = dset.tree()
tree.SetName("DecayTree")
tree.SetTitle("DecayTree")
tree.Write()
file.Close()



