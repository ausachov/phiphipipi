void Dalitz()
{
    gROOT->Reset();
    gStyle->SetOptStat(kFALSE);
    

    TChain * DecayTree=new TChain("DecayTree");
    DecayTree->Add("Data/AllPPpipi_TightIDCut.root");
    
    const Int_t NEn=DecayTree->GetEntries();
    Double_t PPMass, PPiMass, PiPiMass, PpPipMass, PipPpMass, PiPMass;
   
    
    DecayTree->SetBranchAddress("PP_Mass",&PPMass);
    DecayTree->SetBranchAddress("PPi_Mass",&PPiMass);
    DecayTree->SetBranchAddress("PiP_Mass",&PiPMass);
    DecayTree->SetBranchAddress("PpPip_Mass",&PpPipMass);
    DecayTree->SetBranchAddress("PipPp_Mass",&PipPpMass);
    DecayTree->SetBranchAddress("PiPi_Mass",&PiPiMass);
    

    
    TH2D * histPP_vs_PiPi = new TH2D("histPP_vs_PiPi","histPP_vs_PiPi",140,2800.,4200.,  120,200.,1400.);
    TH2D * histPiP_vs_PPi = new TH2D("histPiP_vs_PPi","histPiP_vs_PPi",240,1000.,3400.,  240,1000.,3400.);
    TH2D * histPpPip_vs_PipPp = new TH2D("histPpPip_vs_PipPp","histPpPip_vs_PipPp",240,1000.,3400.,  240,1000.,3400.);
    

    for (Int_t i=0; i<NEn; i++)
    {
        DecayTree->GetEntry(i);
        
        histPP_vs_PiPi->Fill(PPMass,PiPiMass);
        histPiP_vs_PPi->Fill(PPiMass,PiPMass);
        histPpPip_vs_PipPp->Fill(PpPipMass,PipPpMass);
    }
    
    
    TCanvas *canv1 = new TCanvas("canv1","PP_vs_PiPi",5,85,800,600);
    histPP_vs_PiPi->SetXTitle("PP_vs_PiPi");
    histPP_vs_PiPi->DrawCopy("COLZ");
    canv1->SaveAs("PP_vs_PiPi.pdf");
    
    TCanvas *canv2 = new TCanvas("canv2","PiP_vs_PPi",5,85,800,600);
    histPiP_vs_PPi->SetXTitle("PiP_vs_PPi");
    histPiP_vs_PPi->DrawCopy("COLZ");
    canv2->SaveAs("PiP_vs_PPi.pdf");
    
    TCanvas *canv3 = new TCanvas("canv3","PpPip_vs_PipPp",5,85,800,600);
    histPpPip_vs_PipPp->SetXTitle("PpPip_vs_PipPp");
    histPpPip_vs_PipPp->DrawCopy("COLZ");
    canv3->SaveAs("PpPip_vs_PipPp.pdf");
    
}