#include <TMath.h>




void BranchDraw()
{
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  
  Double_t range0=2800;
  Double_t range1=4200;
  Int_t nch=140;
  
  TChain * DecayTreeCh=new TChain("DecayTree");
  DecayTreeCh->Add("AllPPpipi_CleanLKs.root");
    
    
    TCut cutProtonPt = "ProtonP_PT>300 && ProtonM_PT>300"; // 1500
    TCut cutPiPt = "ProtonP_PT>250 && ProtonM_PT>250"; // 1500
    TCut cutID = "ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4"; // 15
    TCut cutPiID = "PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2"; // 15
    
    TCut cutProtonTrack = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3"; // 5
    TCut cutPiTrack = "PiP_TRACK_CHI2NDOF<3 && PiM_TRACK_CHI2NDOF<3"; // 5
    
    TCut cutIpChi2 = "(ProtonP_IPCHI2_OWNPV > 9)&&(ProtonM_IPCHI2_OWNPV > 9)&&(PiP_IPCHI2_OWNPV > 9)&&(PiM_IPCHI2_OWNPV > 9)"; //
    
    TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<20"; // 9
    
    TCut cutFDChi2 = "Jpsi_FDCHI2_OWNPV>100";
    
    TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
    TCut trigger1Cut = "Jpsi_Hlt1TrackAllL0Decision_TOS || Jpsi_Hlt1DiProtonDecision_TOS";
    TCut trigger2Cut = "Jpsi_Hlt2Topo2BodyBBDTDecision_TOS || Jpsi_Hlt2Topo3BodyBBDTDecision_TOS || Jpsi_Hlt2Topo4BodyBBDTDecision_TOS || Jpsi_Hlt2Topo2BodySimpleDecision_TOS || Jpsi_Hlt2Topo3BodySimpleDecision_TOS || Jpsi_Hlt2Topo4BodySimpleDecision_TOS || Jpsi_Hlt2DiProtonDecision_TOS";
    
    TCut addCut = "Jpsi_m_scaled>2800 && Jpsi_m_scaled<4200 && ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4 && PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2 && Jpsi_FDCHI2_OWNPV>100 && Jpsi_ENDVERTEX_CHI2<20";
    
    
    TCut totCut = cutProtonPt &&
                  cutPiPt &&
                  cutID &&
                  cutPiID&&
                  cutProtonTrack &&
                  cutPiTrack &&
                  cutIpChi2 &&
                  cutJpsiVx &&
                  cutFDChi2 &&    
                  trigger0Cut &&
                  trigger1Cut &&
                  trigger2Cut;
//                  addCut;
    
    
  TTree* DecayTree = DecayTreeCh->CopyTree(totCut);
        
  ULong64_t NEn=DecayTree->GetEntries();
  Double_t Branch_Value, PE1, PX1, PY1,PZ1, PE2, PX2, PY2,PZ2, PE, PX, PY,PZ, PPMass, PPiMass,PpPipMass;
  Double_t  pe1, px1, py1,pz1, pe2, px2, py2,pz2,pe, px, py,pz, ppe,ppx,ppy,ppz,PiPiMass, pee,pxx,pyy,pzz;
  DecayTree->SetBranchAddress("Jpsi_m_scaled",&Branch_Value);
  DecayTree->SetBranchAddress("ProtonP_PE",&PE1);
  DecayTree->SetBranchAddress("ProtonP_PX",&PX1);
  DecayTree->SetBranchAddress("ProtonP_PY",&PY1);
  DecayTree->SetBranchAddress("ProtonP_PZ",&PZ1);
    
  DecayTree->SetBranchAddress("ProtonM_PE",&PE2);
  DecayTree->SetBranchAddress("ProtonM_PX",&PX2);
  DecayTree->SetBranchAddress("ProtonM_PY",&PY2);
  DecayTree->SetBranchAddress("ProtonM_PZ",&PZ2);

    DecayTree->SetBranchAddress("PiP_PE",&pe1);
    DecayTree->SetBranchAddress("PiP_PX",&px1);
    DecayTree->SetBranchAddress("PiP_PY",&py1);
    DecayTree->SetBranchAddress("PiP_PZ",&pz1);
    
    DecayTree->SetBranchAddress("PiM_PE",&pe2);
    DecayTree->SetBranchAddress("PiM_PX",&px2);
    DecayTree->SetBranchAddress("PiM_PY",&py2);
    DecayTree->SetBranchAddress("PiM_PZ",&pz2);
    
  TH1D * hist = new TH1D("hist","hist" , 140, 2800, 4200);
  TH1D * histPP = new TH1D("histPP","histPP" , nch, range0, range1);
  TH1D * histPiPi = new TH1D("histPiPi","histPiPi" , 120, 200, 1400);
  TH1D * histPPi = new TH1D("histPPi","histPPi" , 200, 1000, 3000);
  TH1D * histPpPip = new TH1D("histPpPip","histPpPip" , 200, 1000, 3000);
  for (ULong64_t i=0; i<NEn; i++) 
  {

      DecayTree->GetEntry(i);

      PE=PE1+PE2;
      PX=PX1+PX2;
      PY=PY1+PY2;
      PZ=PZ1+PZ2;
      PPMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      
      
      
      pe=pe1+pe2;
      px=px1+px2;
      py=py1+py2;
      pz=pz1+pz2;
      PiPiMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
      histPiPi->Fill(PiPiMass);
      
//      if(PPMass>3075 & PPMass<3125)
//      {
        hist->Fill(Branch_Value);
        histPP->Fill(PPMass);
//      }
      ppe=pe1+PE2;
      ppx=px1+PX2;
      ppy=py1+PY2;
      ppz=pz1+PZ2;
      PPiMass = TMath::Sqrt(ppe*ppe-ppx*ppx-ppz*ppz-ppy*ppy);
      histPPi->Fill(PPiMass);
      
      
      pee = pe1+PE1;
      pxx = px1+PX1;
      pyy = py1+PY1;
      pzz = pz1+PZ1;
      PpPipMass = TMath::Sqrt(pee*pee-pxx*pxx-pzz*pzz-pyy*pyy);
      histPpPip->Fill(PpPipMass);

  }
  


    TCanvas *canv2 = new TCanvas("canv2","PP",5,85,800,600);
    histPP->SetXTitle("PP");
    histPP->DrawCopy("E");
    canv2->SaveAs("pp.pdf");
    
    TCanvas *canv3 = new TCanvas("canv3","PiPi",5,85,800,600);
    histPiPi->SetXTitle("PiPi");
    histPiPi->DrawCopy("E");
    canv3->SaveAs("pipi.pdf");
    
    TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
    hist->SetXTitle("B_MM");
    hist->DrawCopy("E");
    canv1->SaveAs("pppipi.pdf");
    
    TCanvas *canv4 = new TCanvas("canv4","ppiM",5,85,800,600);
    histPPi->SetXTitle("PPi");
    histPPi->DrawCopy("E");
    canv4->SaveAs("ppi.pdf");
    
    TCanvas *canv5 = new TCanvas("canv5","pplpipl",5,85,800,600);
    histPpPip->SetXTitle("PplPipl");
    histPpPip->DrawCopy("E");
    canv5->SaveAs("pplpipl.pdf");

}
