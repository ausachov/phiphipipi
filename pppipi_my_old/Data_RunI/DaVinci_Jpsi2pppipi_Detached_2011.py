def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]


    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Jpsi')

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    # TISTOS for Jpsi
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    from Configurables import TupleToolDecayTreeFitter
    tuple.Jpsi.ToolList +=  ["TupleToolDecayTreeFitter/PVFit"]        # fit with both PV and mass constraint
    tuple.Jpsi.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.Jpsi.PVFit.Verbose = True
    tuple.Jpsi.PVFit.constrainToOriginVertex = True
    tuple.Jpsi.PVFit.daughtersToConstrain = []




    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled" : "DTF_FUN ( M , False )",
        "m_pv" : "DTF_FUN ( M , True )"
        #"c2dtf_1" : "DTF_CHI2NDOF( False )" ,
        #"c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }
    
    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)







    from Configurables import TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Etac')
    
    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True
    
    
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Etac=LoKi__Hybrid__TupleTool("LoKi_Etac")
    LoKi_Etac.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled" : "DTF_FUN ( M , False )",
        "m_pv" : "DTF_FUN ( M , True )"

    }
    
    tuple.addTool(TupleToolDecay, name="Etac")
    tuple.Etac.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Etac"]
    tuple.Etac.addTool(LoKi_Etac)







    from Configurables import TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Ro')
    
    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True
    
    
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Ro=LoKi__Hybrid__TupleTool("LoKi_Ro")
    LoKi_Ro.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled" : "DTF_FUN ( M , False )",
        "m_pv" : "DTF_FUN ( M , True )"
    
    }
    
    tuple.addTool(TupleToolDecay, name="Ro")
    tuple.Ro.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Ro"]
    tuple.Ro.addTool(LoKi_Ro)








    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
#        ,"m_scaled" : "DTF_FUN ( M , False )"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    

# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    # L0 Muon
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",

    # Hlt1
    "Hlt1DiProtonLowMultDecision",
    "Hlt1DiProtonDecision",

    # Hlt1 track
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",

    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",

    # Hlt2
    "Hlt2DiProtonDecision",
    "Hlt2DiProtonLowMultDecision",

    # Hlt2 Topo
                 
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo3BodySimpleDecision",
    "Hlt2Topo4BodyBBDTDecision",
    "Hlt2Topo4BodySimpleDecision"

    ]


Jpsi2pppipiLocation = "Phys/Ccbar2PPPiPiLine/Particles"
Jpsi2pppipiBranches = {
    "ProtonP"  :  "J/psi(1S) ->(eta_c(1S)->^p+ p~-) (rho(770)0->pi+ pi-)"
    ,"ProtonM"  :  "J/psi(1S) ->(eta_c(1S)->p+^p~-) (rho(770)0->pi+ pi-)"
    ,"PiP"      :  "J/psi(1S) ->(eta_c(1S)->p+ p~-) (rho(770)0->^pi+ pi-)"
    ,"PiM"      :  "J/psi(1S) ->(eta_c(1S)->p+ p~-) (rho(770)0->pi+^pi-)"
    ,"Etac"     :  "J/psi(1S) ->^(eta_c(1S)->p+ p~-) (rho(770)0->pi+ pi-)"
    ,"Ro"       :  "J/psi(1S) ->(eta_c(1S)->p+ p~-) ^(rho(770)0->pi+ pi-)"
    ,"Jpsi"     :  "^(J/psi(1S) ->(eta_c(1S)->p+ p~-) (rho(770)0->pi+ pi-))"
}

Jpsi2pppipiTuple = DecayTreeTuple("Jpsi2pppipiTuple")
Jpsi2pppipiTuple.Decay ="^(J/psi(1S) ->^(eta_c(1S)->^p+ ^p~-) ^(rho(770)0->^pi+ ^pi-))"
Jpsi2pppipiTuple.Inputs = [ Jpsi2pppipiLocation ]
fillTuple( Jpsi2pppipiTuple, Jpsi2pppipiBranches, myTriggerList )



#from PhysConf.MicroDST import uDstConf
#uDstConf ("/Event/Bhadron")


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2PPPiPiFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2PPPiPiLineDecision')"""
    )


from Configurables import TrackScaleState
StateScale = TrackScaleState("StateScale", RootInTES = '/Event/Bhadron')
year = "2011"



from Configurables import DaVinci
DaVinci().EventPreFilters = Ccbar2PPPiPiFilters.filters('Ccbar2PPPiPiFilters')
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2011"
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            StateScale,
                            Jpsi2pppipiTuple ]        # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Bhadron"

# Get Luminosity
DaVinci().Lumi = True


DaVinci().DDDBtag   = "dddb-20150928"
DaVinci().CondDBtag = "cond-20150409-1"

# database
#from Configurables import CondDB
#CondDB( LatestGlobalTagByDataType = "2011" )

## database


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00051179_00027144_1.bhadron.mdst'
#                       ], clear=True)