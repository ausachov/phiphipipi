{
TChain *chain = new TChain("DecayTree");
chain->Add("AllPPpipi_CleanLKs.root");
    
    
    
//&& Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS
    
TCut cutProtonTrack = "ProtonP_TRACK_CHI2NDOF<3  && ProtonM_TRACK_CHI2NDOF<3"; // 5
TCut cutID          = "ProtonP_ProbNNp>0.6 && ProtonM_ProbNNp>0.6"; // 15
TCut cutProtonPt    = "ProtonP_PT>1500 && ProtonM_PT>1500"; // 1500
TCut etacCut        = "Etac_PT>2500 && Etac_ENDVERTEX_CHI2<4";
    

    
TCut cutPiTrack     = "PiP_TRACK_CHI2NDOF<3  &&  PiM_TRACK_CHI2NDOF<3"; // 5
TCut cutPiID        = "PiP_ProbNNpi>0.6  &&  PiM_ProbNNpi>0.6"; // 15
TCut cutPiPt        = "PiP_PT>1000 && PiM_PT>1000"; // 1500
TCut roCut          = "Ro_PT>0 && Ro_ENDVERTEX_CHI2<4";



TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<20"; // 9
TCut cutFDChi2 = "Jpsi_FDCHI2_OWNPV>25";
    
TCut cutIpChi2 = "(ProtonP_IPCHI2_OWNPV > 9)&&(ProtonM_IPCHI2_OWNPV > 9)&&(PiP_IPCHI2_OWNPV > 9)&&(PiM_IPCHI2_OWNPV > 9)"; //

    
TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
TCut trigger1Cut = "(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS || Jpsi_Hlt1DiProtonDecision_TOS)";
TCut trigger2Cut = "(Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)";
    
TCut PhysCut = "Etac_M>2850 && Etac_M<3250 && Jpsi_M>3500 && Jpsi_M<3670";
    
TCut totCut =
    cutProtonPt &&
    cutPiPt &&
    cutID &&
    cutPiID&&
    etacCut&&
    //roCut&&
    
    cutProtonTrack &&
    cutPiTrack &&
    
    cutIpChi2 &&
    cutJpsiVx &&
    cutFDChi2 &&
    
//    trigger0Cut &&
//    trigger1Cut &&
//    trigger2Cut &&
    PhysCut;
    
TCanvas a("a","a");
    a.Divide(1,2);
a.cd(1);
    chain->Draw("Etac_M",totCut.GetTitle(),"E");
a.cd(2);
    chain->Draw("Jpsi_M",totCut.GetTitle(),"E");
}