DaVinciVersion = 'v39r1p1'
myJobName = 'Jpsi2pppipi_MagUp_2012'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
#myApplication.user_release_area = '/exp/LHCb/usachov/cmtuser'
#myApplication.user_release_area = '/afs/cern.ch/user/a/ausachov/cmtuser'
myApplication.platform='x86_64-slc6-gcc49-opt'
myApplication.optsfile = ['DaVinci_Jpsi2pppipi_Detached_2012.py']

data  = BKQuery('/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p1a/90000000/BHADRON.MDST', dqflag=['OK']).getDataset()

#validData = LHCbDataset(files= [file for file in data.files if file.getReplicas() ])
validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                         LocalFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

