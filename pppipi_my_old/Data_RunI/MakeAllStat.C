void MakeAllStat()
{
    TChain *chain = new TChain("DecayTree");
    chain->Add("Reduced_MagDown2012.root");
    chain->Add("Reduced_MagUp2012.root");
    chain->Add("Reduced_MagDown2011.root");
    chain->Add("Reduced_MagUp2011.root");
    
    
    TCut cutProtonPt = "ProtonP_PT>300 && ProtonM_PT>300"; // 1500
    TCut cutPiPt = "ProtonP_PT>250 && ProtonM_PT>250"; // 1500
    TCut cutID = "ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4"; // 15
    TCut cutPiID = "PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2"; // 15
    
    TCut cutProtonTrack = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3"; // 5
    TCut cutPiTrack = "PiP_TRACK_CHI2NDOF<3 && PiM_TRACK_CHI2NDOF<3"; // 5
    
    TCut cutIpChi2 = "(ProtonP_IPCHI2_OWNPV > 9)&&(ProtonM_IPCHI2_OWNPV > 9)&&(PiP_IPCHI2_OWNPV > 9)&&(PiM_IPCHI2_OWNPV > 9)"; //
    
    TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<20"; // 9
    
    TCut cutFDChi2 = "Jpsi_FDCHI2_OWNPV>100";
    
    TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
    TCut trigger1Cut = "Jpsi_Hlt1TrackAllL0Decision_TOS || Jpsi_Hlt1DiProtonDecision_TOS";
    TCut trigger2Cut = "Jpsi_Hlt2Topo2BodyBBDTDecision_TOS || Jpsi_Hlt2Topo3BodyBBDTDecision_TOS || Jpsi_Hlt2Topo4BodyBBDTDecision_TOS || Jpsi_Hlt2DiProtonDecision_TOS";
    
    
    TCut totCut =
    cutProtonPt &&
    cutPiPt &&
    cutID &&
    cutPiID&&
    
    cutProtonTrack &&
    cutPiTrack &&
    
    cutIpChi2 &&
    cutJpsiVx &&
    cutFDChi2 &&
    
    trigger0Cut &&
    trigger1Cut &&
    trigger2Cut;
    
    
    TFile *newfile = new TFile("AllPPpipi_TightIDCut.root","recreate");
    TTree *newtree = chain->CopyTree(totCut);
    
    
    
    const Int_t NEn=newtree->GetEntries();
    Double_t Branch_Value, PE1, PX1, PY1,PZ1, PE2, PX2, PY2,PZ2, PE, PX, PY,PZ, PPMass;
    Double_t  pe1, px1, py1,pz1, pe2, px2, py2,pz2,pe, px, py,pz, PiPiMass, PPiMass, PpPipMass, PiPMass, PipPpMass;
    DecayTree->SetBranchAddress("Jpsi_m_scaled",&Branch_Value);
    DecayTree->SetBranchAddress("ProtonP_PE",&PE1);
    DecayTree->SetBranchAddress("ProtonP_PX",&PX1);
    DecayTree->SetBranchAddress("ProtonP_PY",&PY1);
    DecayTree->SetBranchAddress("ProtonP_PZ",&PZ1);
    
    DecayTree->SetBranchAddress("ProtonM_PE",&PE2);
    DecayTree->SetBranchAddress("ProtonM_PX",&PX2);
    DecayTree->SetBranchAddress("ProtonM_PY",&PY2);
    DecayTree->SetBranchAddress("ProtonM_PZ",&PZ2);
    
    DecayTree->SetBranchAddress("PiP_PE",&pe1);
    DecayTree->SetBranchAddress("PiP_PX",&px1);
    DecayTree->SetBranchAddress("PiP_PY",&py1);
    DecayTree->SetBranchAddress("PiP_PZ",&pz1);
    
    DecayTree->SetBranchAddress("PiM_PE",&pe2);
    DecayTree->SetBranchAddress("PiM_PX",&px2);
    DecayTree->SetBranchAddress("PiM_PY",&py2);
    DecayTree->SetBranchAddress("PiM_PZ",&pz2);
    
    
    
    
    TBranch *PP_Mass_Branch = newtree->Branch("PP_Mass", &PPMass, "PP_Mass/D");
    TBranch *PPi_Mass_Branch = newtree->Branch("PPi_Mass", &PPiMass, "PPi_Mass/D");
    TBranch *PiP_Mass_Branch = newtree->Branch("PiP_Mass", &PiPMass, "PiP_Mass/D");
    TBranch *PpPip_Mass_Branch = newtree->Branch("PpPip_Mass", &PpPipMass, "PpPip_Mass/D");
    TBranch *PipPp_Mass_Branch = newtree->Branch("PipPp_Mass", &PipPpMass, "PipPp_Mass/D");
    TBranch *PiPi_Mass_Branch = newtree->Branch("PiPi_Mass", &PiPiMass, "PiPi_Mass/D");
    
    Double_t ppe,ppx,ppy,ppz;
    Double_t pee,pxx,pyy,pzz;
    Double_t ppe2,ppx2,ppy2,ppz2;
    Double_t pee2,pxx2,pyy2,pzz2;
    
    
    for (Int_t i=0; i<NEn; i++)
    {
        
        newtree->GetEntry(i);
        
        PE=PE1+PE2;
        PX=PX1+PX2;
        PY=PY1+PY2;
        PZ=PZ1+PZ2;
        PPMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
        
        
        
        pe=pe1+pe2;
        px=px1+px2;
        py=py1+py2;
        pz=pz1+pz2;
        PiPiMass = TMath::Sqrt(pe*pe-px*px-pz*pz-py*py);
        
        ppe = PE1+pe2;
        ppx = PX1+px2;
        ppy = PY1+py2;
        ppz = PZ1+pz2;
        PPiMass = TMath::Sqrt(ppe*ppe-ppx*ppx-ppy*ppy-ppz*ppz);
        
        ppe2 = PE2+pe1;
        ppx2 = PX2+px1;
        ppy2 = PY2+py1;
        ppz2 = PZ2+pz1;
        PiPMass = TMath::Sqrt(ppe2*ppe2-ppx2*ppx2-ppy2*ppy2-ppz2*ppz2);
        
        
        pee = PE1+pe1;
        pxx = PX1+px1;
        pyy = PY1+py1;
        pzz = PZ1+pz1;
        PpPipMass = TMath::Sqrt(pee*pee-pxx*pxx-pyy*pyy-pzz*pzz);
        
        pee2 = PE2+pe2;
        pxx2 = PX2+px2;
        pyy2 = PY2+py2;
        pzz2 = PZ2+pz2;
        PipPpMass = TMath::Sqrt(pee2*pee2-pxx2*pxx2-pyy2*pyy2-pzz2*pzz2);

        

        
        PP_Mass_Branch->Fill();
        PiPi_Mass_Branch->Fill();
        PPi_Mass_Branch->Fill();
        PpPip_Mass_Branch->Fill();
        PiP_Mass_Branch->Fill();
        PipPp_Mass_Branch->Fill();
    }
    
    
    
    newtree->Print();
    newfile->Write();
    
    delete chain;
    delete newfile;
}
