void MakeAllStat()
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Reduced_MagDown2011.root");
  chain->Add("Reduced_MagUp2011.root");
  chain->Add("Reduced_MagDown2012.root");
  chain->Add("Reduced_MagUp2012.root");
  
  TFile *newfile = new TFile("AllPPpipi_Dirty.root","recreate");
    
  TTree *newtree = chain->CopyTree("");
  
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
}
