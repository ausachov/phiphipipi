// $Id: $
using namespace RooFit;


void MakeCut()
{
    
  TProof::Open("");
    
  TChain *chain = new TChain("DecayTree");

  chain->Add("AllPPpipi_TightIDCut.root");
 
    
//  TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570";
//  TFile *newfile = new TFile("AllPPpipi_CleanLKs.root","recreate");
    
//  TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PiPi_Mass>700 && PiPi_Mass<950";
  TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass<570";
  TFile *newfile = new TFile("AllPPpipi_Ks.root","recreate");
    
    
//  TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PiPi_Mass>700 && PiPi_Mass<950 && PP_Mass>2940 && PP_Mass<3025";
//  TFile *newfile = new TFile("AllPPpipi_CleanLKs_ro_etac.root","recreate");
    
//    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PP_Mass>2940 && PP_Mass<3025";
//    TFile *newfile = new TFile("AllPPpipi_CleanLKs_etac.root","recreate");
    
//    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PP_Mass>3050 && PP_Mass<3150";
//    TFile *newfile = new TFile("AllPPpipi_CleanLKs_jpsi.root","recreate");

    
  TTree *newtree = chain->CopyTree(CleanCut);

    
    
  newtree->Print();
  newfile->Write();
      
  delete chain;
  delete newfile;
}

