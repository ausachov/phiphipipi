from ROOT import TChain, TCut

chain = TChain("Jpsi2pppipiTuple/DecayTree")




    gROOT->ProcessLine(".L Ccbar2PPpipi_MagDown_2011.C");
        gROOT->ProcessLine("Ccbar2PPpipi_MagDown_2011(chain)");


cutProtonPt=TCut("ProtonP_PT>300 && ProtonM_PT>300")
cutPiPt = TCut ("ProtonP_PT>250 && ProtonM_PT>250")
cutID = TCut("ProtonP_ProbNNp>0.1 && ProtonM_ProbNNp>0.1")
cutPiID = TCut("PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2")
cutProtonTrack = TCut("ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3")
cutPiTrack = TCut("PiP_TRACK_CHI2NDOF<3 && PiM_TRACK_CHI2NDOF<3")
cutIpChi2 = TCut("(ProtonP_IPCHI2_OWNPV > 9)&&(ProtonM_IPCHI2_OWNPV > 9)&&(PiP_IPCHI2_OWNPV > 9)&&(PiM_IPCHI2_OWNPV > 9)")
cutJpsiVx = TCut("Jpsi_ENDVERTEX_CHI2<45")
cutFDChi2 = TCut("Jpsi_FDCHI2_OWNPV>0")
trigger0Cut = TCut("(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)")
trigger1Cut = TCut("Jpsi_Hlt1TrackAllL0Decision_TOS || Jpsi_Hlt1DiProtonDecision_TOS")
trigger2Cut = TCut ("Jpsi_Hlt2Topo2BodyBBDTDecision_TOS || Jpsi_Hlt2Topo3BodyBBDTDecision_TOS || Jpsi_Hlt2Topo4BodyBBDTDecision_TOS || Jpsi_Hlt2Topo2BodySimpleDecision_TOS || Jpsi_Hlt2Topo3BodySimpleDecision_TOS || Jpsi_Hlt2Topo4BodySimpleDecision_TOS || Jpsi_Hlt2DiProtonDecision_TOS")
    
    
    totCut = TCut(
    cutProtonPt &&
    cutPiPt &&
    cutID &&
    cutPiID&&
    
    cutProtonTrack &&
    cutPiTrack &&
    
    cutIpChi2 &&
    cutJpsiVx &&
    cutFDChi2 &&
    
    trigger0Cut &&
    trigger1Cut &&
    trigger2Cut)
        
        
    newfile = TFile("Reduced_MagDown2011.root","recreate");
    newtree = chain.CopyTree(totCut);

//   newtree->Print();
    newfile.Write();
        

