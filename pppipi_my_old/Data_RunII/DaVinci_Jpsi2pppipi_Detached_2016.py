def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]



    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Jpsi')

    # TISTOS for Jpsi
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    from Configurables import TupleToolDecayTreeFitter
    tuple.Jpsi.ToolList +=  ["TupleToolDecayTreeFitter/PVFit"]        # fit with both PV and mass constraint
    tuple.Jpsi.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.Jpsi.PVFit.Verbose = True
    tuple.Jpsi.PVFit.constrainToOriginVertex = True
    tuple.Jpsi.PVFit.daughtersToConstrain = []

    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )",
#        "Etac_m_scaled"        : "DTF_FUN( M12 , False )",
#        "Ro_m_scaled"          : "DTF_FUN( M34 , False )",
#        "Lambda1_m_scaled"     : "DTF_FUN( M14 , False )",
#        "Lambda1_m_scaled"     : "DTF_FUN( M23 , False )",
        "m_pv" : "DTF_FUN ( M , True )"
        #"c2dtf_1" : "DTF_CHI2NDOF( False )" ,
        #"c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)


    tuple.addTool(TupleToolDecay, name = 'Etac')
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Etac=LoKi__Hybrid__TupleTool("LoKi_Etac")
    LoKi_Etac.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )",
        #        "Etac_m_scaled"        : "DTF_FUN( M12 , False )",
        #        "Ro_m_scaled"          : "DTF_FUN( M34 , False )",
        #        "Lambda1_m_scaled"     : "DTF_FUN( M14 , False )",
        #        "Lambda1_m_scaled"     : "DTF_FUN( M23 , False )",
        "m_pv" : "DTF_FUN ( M , True )"
    #"c2dtf_1" : "DTF_CHI2NDOF( False )" ,
    #"c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Etac")
    tuple.Etac.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Etac"]
    tuple.Etac.addTool(LoKi_Etac)



    tuple.addTool(TupleToolDecay, name = 'Ro')
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Ro=LoKi__Hybrid__TupleTool("LoKi_Ro")
    LoKi_Ro.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )",
        #        "Ro_m_scaled"        : "DTF_FUN( M12 , False )",
        #        "Ro_m_scaled"          : "DTF_FUN( M34 , False )",
        #        "Lambda1_m_scaled"     : "DTF_FUN( M14 , False )",
        #        "Lambda1_m_scaled"     : "DTF_FUN( M23 , False )",
        "m_pv" : "DTF_FUN ( M , True )"
    #"c2dtf_1" : "DTF_CHI2NDOF( False )" ,
    #"c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Ro")
    tuple.Ro.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Ro"]
    tuple.Ro.addTool(LoKi_Ro)



    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
#        ,"m_scaled" : "DTF_FUN ( M , False )"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    

# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    # L0 Muon
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",

    # Hlt1 track
    "Hlt1B2PhiPhi_LTUNBDecision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
                
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1B2HH_LTUNB_KPiDecision",
    "Hlt1B2HH_LTUNB_KKDecision",
    "Hlt1B2HH_LTUNB_PiPiDecision",
    "Hlt1IncPhiDecision",
    "Hlt1DiProtonDecision",
    "Hlt1DiProtonLowMultDecision",
    "Hlt1LowMultVeloCut_HadronsDecision",
    "Hlt1LowMultPassThroughDecision",
            
                 
    # Hlt2 Topo
    "Hlt2PhiIncPhiDecision",
    "Hlt2PhiPromptPhi2EETurboDecision",
    "Hlt2PhiBs2PhiPhiDecision",
    "Hlt2B2HH_B2HHDecision",
    "Hlt2CcDiHadronDiProtonDecision",
    "Hlt2CcDiHadronDiProtonLowMultDecision",
    "Hlt2CcDiHadronDiPhiDecision",
    "Hlt2PhiIncPhiDecision",
    "Hlt2PhiBs2PhiPhiDecision",
                
    "Hlt2Topo2BodyDecision",
    "Hlt2Topo3BodyDecision",
    "Hlt2Topo4BodyDecision"
    ]



from PhysSelPython.Wrappers import (
                                    Selection,
                                    SelectionSequence,
                                    DataOnDemand,
                                    AutomaticData,
                                    SimpleSelection
                                    )
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from Configurables import CombineParticles, DaVinci, FilterInTrees, PrintDecayTree



PionsFromStr = FilterInTrees("PionsFromStr",Code="('pi+' == ABSID)")
Pions = Selection("Pions",Algorithm=PionsFromStr,
                RequiredSelections=[AutomaticData(Location='Phys/Ccbar2PPPiPiLine/Particles')])

ProtonsFromStr = FilterInTrees("ProtonsFromStr",Code="('p+' == ABSID)")
Protons = Selection("Protons",Algorithm=ProtonsFromStr,
                  RequiredSelections=[AutomaticData(Location='Phys/Ccbar2PPPiPiLine/Particles')])

#'HLTCuts'       : "(HLT_PASS_RE('Hlt2Topo.*Decision'))",
#    'ProtonCuts'    : "(PROBNNp  > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 9.)",
#        'PionCuts'      : "(PROBNNpi > 0.2) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 9.)",
#        'EtacComAMCuts' : "(AM<4.3*GeV)",
#        'EtacComN4Cuts' : """
#            (AM > 2.7 *GeV)
#            & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
#            & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
#            """,
#        'EtacMomN4Cuts' : "(VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 4.2*GeV)) & (BPVDLS>5)",
#        'Prescale'      : 1.


etac = CombineParticles('Combine_etac',
                      DecayDescriptor='[eta_c(1S) -> p+ p~-]',
                      DaughtersCuts={},
                      CombinationCut="(AM > 2.5*GeV)",
                      MotherCut="ALL"
                      )

etac_sel = Selection('Sel_etac',
                   Algorithm=etac,
                   RequiredSelections=[Protons])




ro = CombineParticles('Combine_ro',
                        DecayDescriptor='[rho(770)0 -> pi+ pi-]',
                        DaughtersCuts={},
                        CombinationCut="(AM > 0.1*GeV)",
                        MotherCut="ALL"
                        )

ro_sel = Selection('Sel_ro',
                     Algorithm=ro,
                     RequiredSelections=[Pions])



jpsi_sel = SimpleSelection(
                            'Sel_jpsi',
                            ConfigurableGenerators.CombineParticles,
                            [etac_sel, ro_sel],
                            DecayDescriptor='[J/psi(1S) -> eta_c(1S) rho(770)0]',
                            DaughtersCuts={},
                            CombinationCut="(AM > 2.7*GeV) & (AM<4.3*GeV)",
                            MotherCut=
                            """
                            (VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 4.2*GeV)) & (BPVDLS>5)
                            """
                            )

jpsi_seq = SelectionSequence('jpsi_Seq', TopSelection=jpsi_sel)





Jpsi2pppipiLocation = "Phys/Ccbar2PPPiPiLine/Particles"
Jpsi2pppipiTuple = DecayTreeTuple("Jpsi2pppipiTuple")
Jpsi2pppipiTuple.Decay = "^(J/psi(1S) -> ^(eta_c(1S)->^p+ ^p~-) ^(rho(770)0->^pi+ ^pi-))"
Jpsi2pppipiTuple.Inputs =[ jpsi_seq.outputLocation()]

Jpsi2pppipiBranches = {
    "ProtonP"   :  "J/psi(1S) -> (eta_c(1S)->^p+ p~-)  (rho(770)0->pi+ pi-)"
    ,"ProtonM"  :  "J/psi(1S) -> (eta_c(1S)->p+^p~-)   (rho(770)0->pi+ pi-)"
    ,"PiP"      :  "J/psi(1S) -> (eta_c(1S)->p+ p~-)   (rho(770)0->^pi+ pi-)"
    ,"PiM"      :  "J/psi(1S) -> (eta_c(1S)->p+ p~-)   (rho(770)0->pi+^pi-)"
    ,"Etac"     :  "J/psi(1S) -> ^(eta_c(1S)->p+ p~-)  (rho(770)0->pi+ pi-)"
    ,"Ro"       :  "J/psi(1S) -> (eta_c(1S)->p+ p~-)  ^(rho(770)0->pi+ pi-)"
    ,"Jpsi"     :  "^(J/psi(1S) -> (eta_c(1S)->p+ p~-) (rho(770)0->pi+ pi-))"
}

fillTuple( Jpsi2pppipiTuple, Jpsi2pppipiBranches, myTriggerList )




from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2PPPiPiFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2PPPiPiLineDecision')"""
    )


from Configurables import TrackScaleState
StateScale = TrackScaleState("StateScale", RootInTES = "/Event/Bhadron")




from Configurables import DaVinci
DaVinci().EventPreFilters = Ccbar2PPPiPiFilters.filters('Ccbar2PPPiPiFilters')
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2016"
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            StateScale,
                            jpsi_seq.sequence(),
                            Jpsi2pppipiTuple
                            ]
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Bhadron"

# Get Luminosity
DaVinci().Lumi = True


DaVinci().DDDBtag   = "dddb-20150724"
DaVinci().CondDBtag = "cond-20160522"

# database
#from Configurables import CondDB
#CondDB( LatestGlobalTagByDataType = "2012" )

## database


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00052200_00000211_1.bhadron.mdst'
#                       ], clear=True)