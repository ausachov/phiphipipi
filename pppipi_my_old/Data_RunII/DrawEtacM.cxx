{
TChain *chain = new TChain("DecayTree");
chain->Add("AllPPpipi_RunII_PID02.root");
    
    
    
//&& Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS
    
TCut cutProtonTrack = "ProtonP_TRACK_CHI2NDOF<3  && ProtonM_TRACK_CHI2NDOF<3"; // 5
TCut cutID          = "ProtonP_ProbNNp>0.6 && ProtonM_ProbNNp>0.6"; // 15
TCut cutProtonPt    = "ProtonP_PT>900 && ProtonM_PT>900"; // 1500
TCut etacCut        = "Etac_PT>1000 && Etac_ENDVERTEX_CHI2<4";
    

    
TCut cutPiTrack     = "PiP_TRACK_CHI2NDOF<3  &&  PiM_TRACK_CHI2NDOF<3"; // 5
TCut cutPiID        = "PiP_ProbNNpi>0.6  &&  PiM_ProbNNpi>0.6"; // 15
TCut cutPiPt        = "PiP_PT>900 && PiM_PT>900"; // 1500
TCut roCut          = "Ro_PT>1000 && Ro_ENDVERTEX_CHI2<4";



TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<20"; // 9
TCut cutFDChi2 = "Jpsi_FDCHI2_OWNPV>25";
    
TCut cutIpChi2 = "(ProtonP_IPCHI2_OWNPV > 16)&&(ProtonM_IPCHI2_OWNPV > 16)&&(PiP_IPCHI2_OWNPV > 16)&&(PiM_IPCHI2_OWNPV > 16)"; //

    
TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
TCut trigger1Cut = "(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS || Jpsi_Hlt1DiProtonDecision_TOS)";
TCut trigger2Cut = "(Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)";
    
TCut PhysCut = "Etac_M>2850 && Etac_M<3080 && Jpsi_M>3350 && Ro_M>700 && Ro_M<1000 && nCandidate==0";
    
TCut totCut =
    cutProtonPt &&
    cutPiPt &&
    cutID &&
    cutPiID&&
    etacCut&&
    roCut&&
    
    cutProtonTrack &&
    cutPiTrack &&
    
    cutIpChi2 &&
    cutJpsiVx &&
    cutFDChi2 &&
    
//    trigger0Cut &&
//    trigger1Cut &&
//    trigger2Cut &&
    PhysCut;
    
TCanvas a("a","a",900,900);
    a.Divide(1,3);
a.cd(1);
    chain->Draw("Etac_M",totCut.GetTitle(),"E");
a.cd(2);
    chain->Draw("Ro_M",totCut.GetTitle(),"E");
a.cd(3);
    chain->Draw("Jpsi_M",totCut.GetTitle(),"E");
}