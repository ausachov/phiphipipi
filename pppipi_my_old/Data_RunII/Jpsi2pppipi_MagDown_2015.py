DaVinciVersion = 'v41r2'
myJobName = 'Jpsi2pppipi_MagDown_2015'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
myApplication.platform='x86_64-slc6-gcc49-opt'
myApplication.optsfile = ['DaVinci_Jpsi2pppipi_Detached_2015.py']

data  = BKQuery('/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/BHADRON.MDST', dqflag=['OK']).getDataset()

#validData = LHCbDataset(files= [file for file in data.files if file.getReplicas() ])
validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 20, maxFiles = -1, ignoremissing = True, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                         LocalFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

