{
  TChain *chain = new TChain("Jpsi2pppipiTuple/DecayTree");
  gROOT->ProcessLine(".L Ccbar2PPPiPi_MagUp_2016.C");
  gROOT->ProcessLine("Ccbar2PPPiPi_MagUp_2016(chain)");
  
    TCut cutProtonPt = "ProtonP_PT>300 && ProtonM_PT>300"; // 1500
    TCut cutPiPt = "PiP_PT>250 && PiM_PT>250"; // 1500
    TCut cutID = "ProtonP_ProbNNp>0.1 && ProtonM_ProbNNp>0.1"; // 15
    TCut cutPiID = "PiP_ProbNNpi>0.2 && PiM_ProbNNpi>0.2"; // 15
    
    TCut cutProtonTrack = "ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5"; // 5
    TCut cutPiTrack = "PiP_TRACK_CHI2NDOF<5 && PiM_TRACK_CHI2NDOF<5"; // 5
    
    
    
    TCut cutJpsiVx = "Jpsi_ENDVERTEX_CHI2<45"; // 9
    
    TCut cutIpChi2 = "(ProtonP_IPCHI2_OWNPV > 9)&&(ProtonM_IPCHI2_OWNPV > 9)&&(PiP_IPCHI2_OWNPV > 9)&&(PiM_IPCHI2_OWNPV > 9)"; //
    TCut cutFDChi2 = "Jpsi_FDCHI2_OWNPV>25";
    
    
    TCut trigger0Cut = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)";
    TCut trigger1Cut = "(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS || Jpsi_Hlt1DiProtonDecision_TOS)";
    TCut trigger2Cut = "(Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)";
    

    
    
    TCut totCut =
    cutProtonPt &&
    cutPiPt &&
    cutID &&
    cutPiID&&
    
    cutProtonTrack &&
    cutPiTrack &&
    
    cutIpChi2 &&
    cutJpsiVx &&
    cutFDChi2 &&
    
    trigger0Cut &&
    trigger1Cut &&
    trigger2Cut;
    
    
  TFile *newfile = new TFile("Reduced_MagUp2016.root","recreate");
  TTree *newtree = chain->CopyTree(totCut);
  
//   newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}
