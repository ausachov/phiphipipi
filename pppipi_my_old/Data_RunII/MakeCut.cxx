// $Id: $
using namespace RooFit;


void MakeCut()
{
    
    TProof::Open("");
    
    TChain *chain = new TChain("DecayTree");
    
    chain->Add("AllPPpipi_RunII_TightID.root");
    TCut CleanCut = "Etac_M>2700 && PiP_Mass>1150 && PPi_Mass>1150 && ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4 && PiP_ProbNNpi>0.4 && PiM_ProbNNpi>0.4 && nCandidate%8==0";
    TFile *newfile = new TFile("AllPPpipi_CleanL_PID04.root","recreate");
    
//    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PiPi_Mass>700 && PiPi_Mass<950";
//    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass<570";
//    TFile *newfile = new TFile("AllPPpipi_Ks.root","recreate");
    
    
    //  TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PiPi_Mass>700 && PiPi_Mass<950 && PP_Mass>2940 && PP_Mass<3025";
    //  TFile *newfile = new TFile("AllPPpipi_CleanLKs_ro_etac.root","recreate");
    
    //    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PP_Mass>2940 && PP_Mass<3025";
    //    TFile *newfile = new TFile("AllPPpipi_CleanLKs_etac.root","recreate");
    
    //    TCut CleanCut = "PiP_Mass>1200 && PPi_Mass>1200 && PiPi_Mass>570 && PP_Mass>3050 && PP_Mass<3150";
    //    TFile *newfile = new TFile("AllPPpipi_CleanLKs_jpsi.root","recreate");
    
    
    TTree *newtree = chain->CopyTree(CleanCut);
    
    
    
    newtree->Print();
    newfile->Write();
    
    delete chain;
    delete newfile;
}

