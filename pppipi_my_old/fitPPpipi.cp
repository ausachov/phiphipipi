using namespace RooFit;
using namespace RooStats;


void fitPPpipi()
{
    gROOT.Reset();
    gROOT.SetStyle("Plain");
    gROOT.ProcessLine(".L RooRelBreitWigner.cxx+");
    gStyle.SetOptStat(000);
    TProof::Open("");
    
    
    
    minMassJpsi = 2850;
    maxMassJpsi = 4150;
    binWidthJpsi = 10.;
    Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
    
    
    minMassPP = 2850;
    maxMassPP = 3250;
    binWidthPP = 10.;
    Int_t binNPP = int((maxMassPP-minMassPP)/binWidthPP);
    
    
    minMassPiPi = 600;
    maxMassPiPi = 1200;
    binWidthPiPi = 10.;
    Int_t binNPiPi = int((maxMassPiPi-minMassPiPi)/binWidthPiPi);
    
    
    
    
    
    
    
    etacMass =	2983.6;
    jpsi_mass = 3096.97;
    chi0Mass =	3414.75;
    chi1Mass =	3510.66;
    hcMass   = 	3525.38;
    chi2Mass =	3556.2;
    etac2Mass =	3639.4;
    Psi2S_mass = 3686.1;
    Psi3770Mass = 3773.15;
    x3872Mass =  3871.69;
    x3915Mass =  3918.4;
    x3927Mass =  3927.2;

    

    
    etacGamma =	32.2;
    chi0Gamma =	10.5;
    chi1Gamma =	0.84;
    hcGamma   = 0.7;
    chi2Gamma =	1.93; 
    etac2Gamma =11.3;
    
    
    
    
    RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
    Jpsi_m_scaled.setBins(binNJpsi);
    
    
    
    TChain* chain = new TChain("DecayTree");
    chain.Add("Data/AllPPpipi_CleanLKs_ro_etac.root");
    
    
    
    dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled),"","");
    
    ASig = RooRealVar("ASig","ASig",0.1,0.5);
     PPPiPiMass = RooRealVar("PPPiPiMass","PPPiPiMass",2*938.27+2*139.57);
    
    
    varEtacMass=RooRealVar("M(#eta_{c})", "varEtacMass", etacMass,etacMass-20,etacMass+20);
    varJpsiMass=RooRealVar("M(J/#psi)", "varJpsiMass",jpsi_mass,jpsi_mass-2,jpsi_mass+2);
    varChi0Mass=RooRealVar("M(#chi_{c0})", "varChi0Mass", chi0Mass);
    varChi1Mass=RooRealVar("M(#chi_{c1})", "varChi1Mass", chi1Mass);
    varHcMass=RooRealVar("M(hc)", "varHcMass", hcMass);
    varChi2Mass=RooRealVar("M(#chi_{c2})", "varChi2Mass", chi2Mass);
    varEtac2Mass=RooRealVar("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass);
    varPsi2SMass=RooRealVar("MPsi2S", "varPsi2SMass",Psi2S_mass);
    varPsi3770Mass=RooRealVar("M(psi3770)", "varPsi3770Mass", Psi3770Mass);
    
    
    RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, PPPiPiMass));
    RooFormulaVar varJpsiSigmaN("varJpsiSigmaN", "varJpsiSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varJpsiMass, PPPiPiMass));
    RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, PPPiPiMass));
    RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, PPPiPiMass));
    RooFormulaVar varHcSigmaN("varHcSigmaN", "varHcSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varHcMass, PPPiPiMass));
    RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, PPPiPiMass));
    RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, PPPiPiMass));
    RooFormulaVar varPsi2SigmaN("varPsi2SigmaN", "varPsi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varPsi2SMass, PPPiPiMass));
    RooFormulaVar varPsi3770SigmaN("varPsi3770SigmaN", "varPsi3770SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varPsi3770Mass, PPPiPiMass));
    
    

    
    RooArgList listCompPPPiPi, listNormPPPiPi;
    RooArgSet showParamsPPPiPi;
    RooRealVar radius("radius", "radius", 1.5);
    RooRealVar massa("massa", "massa", 938.27, "MeV");
    RooRealVar massb("massb", "massb", 938.27, "MeV");
    RooRealVar var2PMass("var2PMass", "var2PMass", 2*938.27);
    RooRealVar FitMin("FitMin","FitMin",minMassPP);
    
    RooRealVar varJpsiNumber("NJpsi", "varJpsiNumber", 1e4, 0, 1e7);
    RooGaussian pdfJpsi("pdfJpsi","pdfJpsi",Jpsi_m_scaled,varJpsiMass,varJpsiSigmaN);
    
    RooRealVar varEtac2JpsiRatio("N(Etac)/N(Jpsi)","varEtac2JpsiRatio",0,1);
    //RooRealVar varEtacNumber("NEta", "varEtacNumber", 1e3,0,1e7);
    RooFormulaVar varEtacNumber("NEta", "varEtacNumber", "@0*@1", RooArgList(varEtac2JpsiRatio, varJpsiNumber));
    RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32.00);
    RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", Jpsi_m_scaled, varEtacMass, varEtacGamma,
                                                       RooConst(0.), radius, massa, massb);
    RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",Jpsi_m_scaled,RooConst(0.),varEtacSigmaN);
    RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussN) ;
    
    

    
    



    
    RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
    RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
    RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);
    RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma);
    RooRealVar varPsi3770Gamma("varPsi3770Gamma", "varPsi3770Gamma", 27.2);
    RooRealVar varHcGamma("varHcGamma", "varHcGamma", hcGamma);
    

    


    RooRealVar varChi0Number("NChi0", "varChi0Number", 1000,0,1e5);
    RooRealVar varChi1Number("NChi1", "varChi1Number", 1000,0,1e5);
    RooRealVar varHcNumber("NHc", "varHcNumber", 1000,0,1e5);
    RooRealVar varChi2Number("NChi2", "varChi2Number",1000,0,1e5);
    RooRealVar varEtac2Number("NEta2", "varEtac2Number", 400,0,1e5);
    RooRealVar varPsi3770Number("NPsi3770", "varPsi3770Number", 1e3,0,1e7);


    
    

    
    RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", Jpsi_m_scaled, varChi0Mass, varChi0Gamma,
                                                       RooConst(0), radius, massa, massb);
    RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",Jpsi_m_scaled,RooConst(0.),varChi0SigmaN);
    RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussN) ;

    
    RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", Jpsi_m_scaled, varChi1Mass, varChi1Gamma,
                                                       RooConst(1), radius, massa, massb);
    RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",Jpsi_m_scaled,RooConst(0.),varChi1SigmaN);
    RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussN) ;
    
    
    
    RooRelBreitWigner pdfHcBWrel = RooRelBreitWigner("pdfHcBWrel", "pdfHcBWrel", Jpsi_m_scaled, varHcMass, varHcGamma,
                                                       RooConst(1), radius, massa, massb);
    RooGaussian pdfHcGaussN("pdfHcGaussN","pdfHcGaussN",Jpsi_m_scaled,RooConst(0.),varHcSigmaN);
    RooFFTConvPdf pdfHcN("pdfHcN","pdfHcN",Jpsi_m_scaled,pdfHcBWrel,pdfHcGaussN) ;
    
    

    
    RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", Jpsi_m_scaled, varChi2Mass, varChi2Gamma,
                                                       RooConst(2), radius, massa, massb);
    RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",Jpsi_m_scaled,RooConst(0.),varChi2SigmaN);
    RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussN) ;

    
    
    RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", Jpsi_m_scaled, varEtac2Mass, varEtac2Gamma,
                                                        RooConst(0), radius, massa, massb);
    RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",Jpsi_m_scaled,RooConst(0.),varEtac2SigmaN);
    RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussN) ;
	
    
    
    RooRealVar varPsi2SNumber("NPsi2S", "varPsi2SNumber", 1e4, 0, 1e7);
    RooGaussian pdfPsi2S("pdfPsi2S","pdfPsi2S",Jpsi_m_scaled,varPsi2SMass,varPsi2SigmaN);
    
    
    
    RooRelBreitWigner pdfPsi3770BWrel = RooRelBreitWigner("pdfPsi3770BWrel", "pdfPsi3770BWrel", Jpsi_m_scaled, varPsi3770Mass,varPsi3770Gamma, RooConst(0.), radius, massa, massb);
    RooGaussian pdfPsi3770GaussN("pdfPsi3770GaussN","pdfPsi3770GaussN",Jpsi_m_scaled,RooConst(0.),varPsi3770SigmaN);
    RooFFTConvPdf pdfPsi3770N("pdfPsi3770N","pdfPsi3770N",Jpsi_m_scaled,pdfPsi3770BWrel,pdfPsi3770GaussN) ;
    
    
    
    
    
    RooRealVar varBgrNumber("N_{bgr}", "number of background events", 1e4, 0, 1e9);
    RooRealVar varB0("varB0", "varB0", 0, -1, 1);
    RooRealVar varB1("varB1", "varB1", 0, -1, 1);
    RooRealVar varB2("varB2", "varB2", 0, -1, 1);
    RooRealVar varB3("varB3", "varB3", 0, -1, 1);
    //RooRealVar varB4("varB4", "varB4", 0, -1, 1);
    
    
    
    
    RooChebychev pdfBgr("pdfBgr", "pdfBgr", Jpsi_m_scaled, RooArgList(varB0,varB1,varB2,varB3));
    
    
    
    listCompPPPiPi.add(pdfJpsi);
    listCompPPPiPi.add(pdfEtacN);
    listCompPPPiPi.add(pdfChi0N);
    listCompPPPiPi.add(pdfChi1N);
    listCompPPPiPi.add(pdfHcN);
    listCompPPPiPi.add(pdfChi2N);
    listCompPPPiPi.add(pdfEtac2N);
    listCompPPPiPi.add(pdfPsi2S);
    //listCompPPPiPi.add(pdfPsi3770N);
    listCompPPPiPi.add(pdfBgr);
    
    listNormPPPiPi.add(varJpsiNumber);
    listNormPPPiPi.add(varEtacNumber);
    listNormPPPiPi.add(varChi0Number);
    listNormPPPiPi.add(varChi1Number);
    listNormPPPiPi.add(varHcNumber);
    listNormPPPiPi.add(varChi2Number);
    listNormPPPiPi.add(varEtac2Number);
    //listNormPPPiPi.add(varPsi2SNumber);
    //listNormPPPiPi.add(varPsi3770Number);
    listNormPPPiPi.add(varBgrNumber);
    
     showParamsPPPiPi.add(varJpsiNumber);
     showParamsPPPiPi.add(varEtac2JpsiRatio);
     showParamsPPPiPi.add(varChi0Number);
     showParamsPPPiPi.add(varChi1Number);
     showParamsPPPiPi.add(varHcNumber);
     showParamsPPPiPi.add(varChi2Number);
     showParamsPPPiPi.add(varEtac2Number);
     //showParamsPPPiPi.add(varPsi2SNumber);
     //showParamsPPPiPi.add(varPsi3770Number);
     showParamsPPPiPi.add(varBgrNumber);
    
    
    
    
    RooAddPdf pdfModelPPPiPi("pdfModelPPPiPi", "pdfModelPPPiPi",  listCompPPPiPi, listNormPPPiPi);
    
    
    TH1F* histPPPiPi = new TH1F("histPPPiPi", "histPPPiPi", binNJpsi, minMassJpsi, maxMassJpsi);
    chain.Project("histPPPiPi", "Jpsi_m_scaled", "");
    RooDataHist* dset = new RooDataHist("dset", "dset", Jpsi_m_scaled, histPPPiPi);
    
    
    
    RooFitResult* res = pdfModelPPPiPi.fitTo(*dset, Save(true), Extended(true), PrintLevel(0));
    res = pdfModelPPPiPi.fitTo(*dsetFull, Save(true), Extended(true), PrintLevel(0));
    //res = pdfModelPPPiPi.fitTo(*dset, Save(true), Minos(true), Extended(true), PrintLevel(0));
    
    
    RooPlot* framePPPiPi = Jpsi_m_scaled.frame(Title("PPPiPi mass"));
    dsetFull.plotOn(framePPPiPi, Binning(binNJpsi, minMassJpsi, maxMassJpsi));
    
    pdfModelPPPiPi.plotOn(framePPPiPi);
    pdfModelPPPiPi.paramOn(framePPPiPi, Layout(0.75, 0.95, 0.95), Parameters(showParamsPPPiPi), ShowConstants(true));
    
    RooHist* hresidPPPiPi = framePPPiPi.residHist();
    
    
    TCanvas* canvP = new TCanvas("canvP", "canvP", 1000, 500);
    
    RooPlot* framePP2 = Jpsi_m_scaled.frame();
    framePP2.addPlotable(hresidPPPiPi,"P");
    TPad *pad2 = new TPad("pad2", "pad2",0.01,0.05,0.99,0.3);
    pad2.Draw();
    pad2.cd();
    framePP2.Draw();
    
    canvP.cd();
    TPad *pad = new TPad("pad", "pad",0.01,0.3,0.99,0.99);
    pad.Draw();
    pad.cd();
    framePPPiPi.Draw();
    

}