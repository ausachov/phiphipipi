using namespace RooFit;
void fitPiPi()
{
    bool Test=false;
    
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    gStyle->SetOptStat(000);
    TProof::Open("");
    
    
    
    Float_t minMassJpsi = 2900;
    Float_t maxMassJpsi = 4200;
    Float_t binWidthJpsi = 10.;
    Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
    
    
    Float_t minMassPP = 2850;
    Float_t maxMassPP = 3300;
    Float_t binWidthPP = 10.;
    Int_t binNPP = int((maxMassPP-minMassPP)/binWidthPP);
    
    
    Float_t minMassPiPi = 600;
    Float_t maxMassPiPi = 1100;
    Float_t binWidthPiPi = 10.;
    Int_t binNPiPi = int((maxMassPiPi-minMassPiPi)/binWidthPiPi);
    
    
    
    
    
    
    
    
    Float_t Ks_mass = 497.614;
    
    Float_t RoMass =775.49;
    Float_t RoMassError =	0.7;
    Float_t RoGamma = 149.1;
    Float_t RoGammaError =	0.8;
    Float_t f0_mass = 980;
    Float_t f0_gamma = 70;
    
    
    
    
    
    
    
    //    // make Pull histo
    //    TH1F* h_pull = new TH1F("h_pull","h_pull",100,-5.,5.);
    //    for(int i =0; i < h_pull_vs_m4l->GetNbinsX()+1;i++){
    //        double center_bin = h_m4l_inclusive->GetBinCenter(i);
    //        double y_bin = h_m4l_inclusive->GetBinContent(i);
    //        double err_y_bin = h_m4l_inclusive->GetBinError(i);
    //        double func_center_bin = func->Eval(center_bin);
    //        double err_func = func_center_bin*0.0;
    //        // calculate
    //        double pull;
    //        if( y_bin != 0. ){
    //            pull = (func_center_bin - y_bin) / TMath::Power(TMath::Power(err_y_bin,2) + TMath::Power(err_func,2),0.5);
    //            h_pull_vs_m4l->SetBinContent(i,pull);
    //            h_pull_vs_m4l->SetBinError(i,0);
    //            h_pull->Fill(pull);
    //        }
    //    }
    
    
    //    TH1F* h_pull_vs_m4l = (TH1F*)h_m4l_inclusive->Clone("h_pull_vs_m4l");
    //    h_pull_vs_m4l->Scale(0);
    //    TCanvas *C = new TCanvas();
    //    C->cd();
    //    TPad *pad_hist = new TPad("pad_hist","pad_hist",0.05,0.35,0.95,0.95);
    //    TPad *pad_pull = new TPad("pad_pull","pad_pull",0.05,0.05,0.95,0.30);
    //    pad_hist->Draw();
    //    pad_pull->Draw();
    //
    //    pad_hist->cd();
    //    h_m4l_inclusive->Draw();
    //
    //    pad_pull->cd();
    //    h_pull_vs_m4l->GetYaxis()->SetRangeUser(-5.,5.);
    //    h_pull_vs_m4l->GetYaxis()->SetLabelSize(0.07);
    //    h_pull_vs_m4l->GetXaxis()->SetLabelSize(0.07);
    //    pad_pull->SetGridy();
    //    h_pull_vs_m4l->SetMarkerStyle(20);
    //    h_pull_vs_m4l->SetMarkerSize(0.6);
    //    h_pull_vs_m4l->Draw("P0");
    //    [07/11/16 19:46:46] Denys Denysiuk: /
    //    [07/11/16 19:46:46] Denys Denysiuk: /
    //    [07/11/16 19:46:47] Denys Denysiuk: h_m4l_inclusive
    //
    
    
    
    
    
    
    RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
    RooRealVar Jpsi_ENDVERTEX_CHI2("Jpsi_ENDVERTEX_CHI2","Jpsi_ENDVERTEX_CHI2",0,20.);
    
    RooRealVar PP_Mass("PP_Mass", "PP_Mass", minMassPP, maxMassPP, "MeV");
    PP_Mass.setBins(binNPP);
    
    RooRealVar PiPi_Mass("PiPi_Mass", "PiPi_Mass", minMassPiPi, maxMassPiPi, "MeV");
    PiPi_Mass.setBins(binNPiPi);
    
    
    
    //	TH1F* histKsDiPhi = new TH1F("histKsDiPhi", "histKsDiPhi", binNKs, minMassKs, maxMassKs);
    //	TH1F* histKsDiK = new TH1F("histKsDiK", "histKsDiK", binNKs, minMassKs, maxMassKs);
    //	TH1F* histKsPhiK = new TH1F("histKsPhiK", "histKsPhiK", binNKs, minMassKs, maxMassKs);
    
    
    
    if(Test==true)
    {
        TH1F* histPiPi;
        TFile* fileH = new TFile("histPiPi.root");
        fileH->GetObject("histPiPi", histPiPi);
    }
    else
    {
        TChain* chain = new TChain("DecayTree");
        chain->Add("Data/AllPPpipi_CleanLKs.root");
        RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(PiPi_Mass),"","");
        TH1F* histPiPi = new TH1F("histPiPi", "histPiPi", binNPiPi, minMassPiPi, maxMassPiPi);
        chain->Project("histPiPi", "PiPi_Mass", "");
        histPiPi->SaveAs("histPiPi.root");
    }
    
    
    
    
    
    RooArgList listCompPiPi, listNormPP;
    RooArgSet showParamsPP;
    RooRealVar radius("radius", "radius", 1.5);
    RooRealVar massa("massa", "massa", 139.57, "MeV");
    RooRealVar massb("massb", "massb", 139.57, "MeV");
    RooRealVar var2PiMass("var2PiMass", "var2PiMass", 2*139.57);
    RooRealVar FitMin("FitMin","FitMin",minMassPiPi);
    RooRealVar FitCentre("FitCentre","FitCentre",0.5*(minMassPiPi+maxMassPiPi));
    
    
    
    RooRealVar varKsNumber("NKs", "varKsNumber", 1e3, 1e2, 1e5);
    RooRealVar varKsMass("M(J/#psi)", "varKsMass",Ks_mass,Ks_mass-50,Ks_mass+50);
    RooRealVar varKsSigmaN("varKsSigmaN", "varKsSigmaN", 150, 100,600);
    RooBreitWigner pdfKs("pdfKs","pdfKs",PiPi_Mass,varKsMass,varKsSigmaN);
    
    RooRealVar varRoNumber("NRo", "varRoNumber", 1e5,0,1e7);
    RooRealVar varRoMass("M(ro)", "varRoMass", RoMass,RoMass-50,RoMass+30);
    RooRealVar varRoGamma("varRoGamma", "varRoGamma", RoGamma);//,RoGamma-5,RoGamma+5);
    RooRelBreitWigner pdfRoN = RooRelBreitWigner("pdfRoBWrel", "pdfRoBWrel", PiPi_Mass, varRoMass, varRoGamma,
                                                 RooConst(0), radius, massa, massb);
    
    
    
    RooRealVar varf0Number("Nf0", "varf0Number", 1e3, 1e1, 1e5);
    RooRealVar varf0Mass("varf0Mass", "varf0Mass",f0_mass,f0_mass-50,f0_mass+50);
    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", f0_gamma, 40,100);
    RooBreitWigner pdff0("pdff0","pdff0",PiPi_Mass,varf0Mass,varf0Gamma);
    //    RooGaussian pdfRoGaussN("pdfRoGaussN","pdfRoGaussN",PiPi_Mass,RooConst(0.),varKsSigmaN);
    //    RooFFTConvPdf pdfRoN("pdfRoN","pdfRoN",PiPi_Mass,pdfRoBWrel,pdfRoGaussN) ;
    
    
    //    RooRealVar varXNumber("NX", "varXNumber", 1e3,0,1e7);
    //    RooRealVar varXMass("M(X)", "varXMass", 2950.,2970.);
    //    RooRealVar varXGamma("varXGamma", "varXGamma", 0,900);
    //    RooRelBreitWigner pdfXBWrel = RooRelBreitWigner("pdfXBWrel", "pdfXBWrel", PiPi_Mass, varXMass, varXGamma,
    //                                                       RooConst(0), radius, massa, massb);
    //    RooGaussian pdfXGaussN("pdfXGaussN","pdfXGaussN",PiPi_Mass,RooConst(0.),varKsSigmaN);
    //    RooFFTConvPdf pdfXN("pdfXN","pdfXN",PiPi_Mass,pdfXBWrel,pdfRoGaussN) ;
    
    
    RooRealVar varBgrNumber("N_{bgr}", "number of background events", 5e5, 0, 1e7);
    RooRealVar varB0("varB0", "varB0", 0, -1, 1);
    RooRealVar varB1("varB1", "varB1", 0, -1, 1);
    RooRealVar varB2("varB2", "varB2", 0, -1, 1);
    RooRealVar varB3("varB3", "varB3", 0, -1, 1);
    RooRealVar varB4("varB4", "varB4", 0, -1, 1);
    
    RooRealVar varBExp("varBExp", "varBExp", 0.0001,0,0.01);
    
    
    
    RooGenericPdf pdfBgr("pdfBgr","pdfBgr","(@0>@1)*TMath::Sqrt(@0-@1)*TMath::Exp(-@0*@2)*(1+(@0-@3)/100*@4+(@0-@3)*(@0-@3)/10000*@5+(@0-@3)*(@0-@3)*(@0-@3)/1000000*@6+(@0-@3)*(@0-@3)*(@0-@3)*(@0-@3)/100000000*@7)",RooArgList(PiPi_Mass,var2PiMass,varBExp,FitCentre, varB1,varB2,varB3,varB4));
    //RooChebychev pdfBgr("pdfBgr", "pdfBgr", PiPi_Mass, RooArgList(varB0,varB1,varB2,varB3));
    
    
    
    //    listCompPiPi.add(pdfKs);
    //    listNormPP.add(varKsNumber);
    listCompPiPi.add(pdfRoN);
    listNormPP.add(varRoNumber);
    listCompPiPi.add(pdff0);
    listNormPP.add(varf0Number);
    //    listCompPiPi.add(pdfXN);
    //    listNormPP.add(varXNumber);
    listCompPiPi.add(pdfBgr);
    listNormPP.add(varBgrNumber);
    RooAddPdf pdfModelPiPi("pdfModelPiPi", "pdfModelPiPi",  listCompPiPi, listNormPP);
    
    
    
    
    
    
    
    RooDataHist* dset = new RooDataHist("dset", "dset", PiPi_Mass, histPiPi);
    

    
    RooFitResult* res = pdfModelPiPi.fitTo(*dset, Save(true), Extended(true), PrintLevel(0));
    
    
    //varRoNumber.setVal(4e4);
    //varRoNumber.setConstant(kTRUE);
    //varKsNumber.setVal(2e3);
    //varKsNumber.setConstant(kTRUE);
    
    //res = pdfModelPiPi.fitTo(*dset, Save(true), Minos(true), Extended(true), PrintLevel(0));
    if(Test==true)
        res = pdfModelPiPi.fitTo(*dset, Save(true), Minos(true), Extended(true), PrintLevel(0));
    else
    {
        res = pdfModelPiPi.fitTo(*dsetFull, Save(true), Extended(true), PrintLevel(0));
        //res = pdfModelPiPi.fitTo(*dsetFull, Save(true), Extended(true), PrintLevel(0));
    }
    //res = pdfModelPiPi.fitTo(*dset, Save(true), Minos(true), Extended(true), PrintLevel(0));
    
    
    RooPlot* framePiPi = PiPi_Mass.frame(Title("PiPi mass"));
    dset->plotOn(framePiPi, Binning(binNPiPi, minMassPiPi, maxMassPiPi));
    pdfModelPiPi.plotOn(framePiPi);
    
    RooHist* hresidPiPi = framePiPi->residHist();
    
    
    TCanvas* canvP = new TCanvas("canvP", "canvP", 1000, 500);
    
    RooPlot* framePiPi2 = PiPi_Mass.frame();
    framePiPi2->addPlotable(hresidPiPi,"P");
    TPad *pad2 = new TPad("pad2", "pad2",0.01,0.05,0.99,0.3);
    pad2->Draw();
    pad2->cd();
    framePiPi2->Draw();
    
    canvP->cd();
    TPad *pad = new TPad("pad", "pad",0.01,0.3,0.99,0.99);
    pad->Draw();
    pad->cd();
    framePiPi->Draw();
    
}