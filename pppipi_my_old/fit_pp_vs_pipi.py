from ROOT import *


gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
TProof.Open("")



minMassPPpipi = 3400.
maxMassPPpipi = 4200.
binWidthPPpipi = 10.
binNPPpipi = int((maxMassPPpipi-minMassPPpipi)/binWidthPPpipi)

minMassPP = 2850.
maxMassPP = 3250.
binWidthPP = 5.
binNPP = int((maxMassPP-minMassPP)/binWidthPP)

minMasspipi = 620;
maxMasspipi = 1100;
binWidthpipi = 20.;
binNpipi = int((maxMasspipi-minMasspipi)/binWidthpipi)


protonMass = 938.
radius = 0.5


CharmoniaPDG ={
    "Etac":{"M":2983.4,"Merr":0.5,"G":31.8,"Gerr":0.8},
    "Jpsi":{"M":3096.9,"Merr":0.006,"G":0.0929,"Gerr":0.0028},
    "Chi0":{"M":3414.75,"Merr":0.31,"G":10.5,"Gerr":0.6},
    "Chi1":{"M":3510.66,"Merr":0.07,"G":0.84,"Gerr":0.04},
    "Chi2":{"M":3556.20,"Merr":0.09,"G":1.93,"Gerr":0.11},
    "hc":{"M":3525.38,"Merr":0.11,"G":0.7,"Gerr":0.4},
    "Etac2":{"M":3639.2,"Merr":1.2,"G":11.3,"Gerr":3.2},
    "Psi2":{"M":3686.097,"Merr":0.025,"G":0.296,"Gerr":0.008},
    "Psi3770":{"M":3773.13,"Merr":0.35,"G":27.2,"Gerr":1.0},
    "Psi3823":{"M":3822.2,"Merr":1.2,"G":8,"Gerr":8},
    "X3872":{"M":3871.69,"Merr":0.17,"G":0.6,"Gerr":0.6},
    "X3900":{"M":3886.6,"Merr":2.4,"G":28.1,"Gerr":2.6},
    "X3915":{"M":3918.4,"Merr":1.9,"G":20,"Gerr":5},
    "Chi22P":{"M":3927.2,"Merr":2.6,"G":24,"Gerr":6},
    "X4020":{"M":4024.1,"Merr":1.9,"G":13,"Gerr":5},
    "Psi4040":{"M":4039.,"Merr":1.,"G":80,"Gerr":10},
    "Psi4140":{"M":4146.9,"Merr":3.1,"G":15,"Gerr":6},
    "Psi4160":{"M":4191.,"Merr":5.,"G":70.,"Gerr":10.}
}

CharmoniaOpts ={
    "Etac":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Jpsi":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi0":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi1":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "hc":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Etac2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi3770":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi3823":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3872":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3900":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3915":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi22P":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X4020":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4040":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4140":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4160":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000}
}

def __init__():
    t=0

    



def inputData():
    Jpsi_m_scaled =  RooRealVar("Jpsi_m_scaled", "Jpsi_m_scaled", minMassPPpipi, maxMassPPpipi, "MeV")
    Etac_M =  RooRealVar("Etac_M", "Etac_M", minMassPP, maxMassPP, "MeV")
    Ro_M = RooRealVar("Ro_M", "Ro_M", minMasspipi, maxMasspipi, "MeV")

#    inFile = TFile("Data_RunII/AllPPpipi_CleanL_PID04.root")
#    tree = inFile.Get("DecayTree")

    chain = TChain("DecayTree")
    chain.Add("Data_RunII/AllPPpipi_RunII_PID02.root")
    tree = chain.CopyTree("Etac_M>2850 && Etac_M<3250 && Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS")

    dataArgSet = RooArgSet(Jpsi_m_scaled, Etac_M)

    dsetFull = RooDataSet("dsetFull", "dsetFull", tree,dataArgSet)

    w = RooWorkspace("w");

    getattr(w,'import')(dataArgSet)
    getattr(w,'import')(dsetFull)

    w.SaveAs("workspace_data.root")





def inputModel():
   
   
    f = TFile("workspace_data.root")
    w = f.Get("w")
   
   
    jpsi_m = CharmoniaPDG["Jpsi"]["M"]
    jpsi_dm = CharmoniaOpts["Jpsi"]["Mwind"]
    #w.factory("Gaussian::pdfJpsiPP(Etac_M,JpsiMass[{0},{1}],JpsiSigmaPP[0,15])".format(jpsi_m-jpsi_dm,jpsi_m+jpsi_dm))
    w.factory("Gaussian::pdfJpsiPP(Etac_M,JpsiMass[{0},{1}],JpsiSigmaPP[0,15])".format(jpsi_m-jpsi_dm,jpsi_m+jpsi_dm))
    
    
    etac_m = CharmoniaPDG["Etac"]["M"]
    etac_dm = CharmoniaOpts["Etac"]["Mwind"]
#    w.factory("RooRelBreitWigner::EtacRBW(Etac_M,EtacMass[{0},{1}],EtacGamma[0,80],0,{2},{3},{3})".format(etac_m-etac_dm,etac_m+etac_dm,radius,protonMass))
    w.factory("RooRelBreitWigner::EtacRBW(Etac_M,EtacMass[{0}],EtacGamma[31.8],0,{1},{2},{2})".format(etac_m,radius,protonMass))
    w.factory("FCONV::pdfEtacPP(Etac_M,EtacRBW,Gaussian::EtacG(Etac_M,0.,JpsiSigmaPP))")
#    w.factory("Chebychev::pdfBgrPP(Etac_M,{PPBg1[-1,1],PPBg2[-1,1]})")

    w.factory("Exponential::pdfBgrPP(Etac_M,PPBg1[-0.1,0])")

    
    w.factory("SUM::pdfModelPP(NEtacPP[0,1e6]*pdfEtacPP,NJpsiPP[0,1e6]*pdfJpsiPP,NBgrPP[0,1e6]*pdfBgrPP)")
    
    
#    w.factory("Gaussian::pdfKs(Ro_M,KsMass[450,500],KsSigma[0,15])")
##    w.factory("Voigtian::pdfRo(Ro_M,RoMass[700,820],KsSigma, RoGamma[150])")
#    w.factory("Voigtian::pdfRo(Ro_M,RoMass[750],KsSigma, RoGamma[150])")
#    w.factory("Chebychev::BgrPiPi(Ro_M,{A1[-10,10],A2[-10,10],A3[-10,10],A4[0]})")
##    w.factory("SUM::pdfModelPiPi(NKs[0,10000]*pdfKs,NRo[0,10000]*pdfRo,NBgPiPi[0,1000000]*BgrPiPi)")
#    w.factory("SUM::pdfModelPiPi(NRo[100,10000000]*pdfRo,NBgPiPi[0,50000]*BgrPiPi)")

    w.SaveAs("workspace.root")

def work():
    
    f = TFile("workspace.root")
     
    w = f.Get("w")
    pdf = w.pdf("pdfModelPP")
#    pdf = w.pdf("pdfModelPiPi")
    NJpsi = w.var("NJpsiPP")
    NEtac = w.var("NEtacPP")
    NBgrPP = w.var("NBgrPP")
    dsetFull = w.data("dsetFull")
    Etac_M = w.var("Etac_M")
    
    
#    Ro_M = w.var("Ro_M")
#    NRo = w.var("NRo")
#    NRo.setVal(0.)
#    NRo.setConstant(True)

 
#    NKs = w.var("NKs")
#    NKs.setVal(0.)
#    NKs.setConstant(True)




    JpsiSigma = w.var("JpsiSigmaPP")
    JpsiMass = w.var("JpsiMass")

    pdf.fitTo(dsetFull,RooFit.Save(True),RooFit.Extended(True))
#    JpsiMass.setConstant(True)
#    NKs.setConstant(False)
    pdf.fitTo(dsetFull,RooFit.Save(True),RooFit.Extended(True))
    
    
    frame = Etac_M.frame(RooFit.Title("PP"))
    dsetFull.plotOn(frame,RooFit.Binning(binNPP, minMassPP, maxMassPP))
    pdf.plotOn(frame)
    
    canvas = TCanvas("canvPP","canvPP",800,600)
    frame.Draw()
    canvas.SaveAs("PPFit_Full.pdf")
    
#    frame = Ro_M.frame(RooFit.Title("PiPi"))
#    dsetFull.plotOn(frame,RooFit.Binning(binNpipi, minMasspipi, maxMasspipi))
#    pdf.plotOn(frame)
#    canvas = TCanvas("canvPiPi","canvPiPi",800,600)
#    frame.Draw()
#    canvas.SaveAs("JpsiFitFull.pdf")





    
    JpsiSigma.setConstant(True)
    JpsiMass.setConstant(True)
   
    EtacPiPiHist = TH1F("EtacPiPiHist","EtacPiPiHist",binNPPpipi,minMassPPpipi,maxMassPPpipi)
    JpsiPiPiHist = TH1F("JpsiPiPiHist","JpsiPiPiHist",binNPPpipi,minMassPPpipi,maxMassPPpipi)
    BgPiPiHist   = TH1F("BgPiPiHist","BgPiPiHist"    ,binNPPpipi,minMassPPpipi,maxMassPPpipi)


    for ii in range(binNPPpipi):
       binMin = minMassPPpipi + ii*binWidthPPpipi
       binMax = minMassPPpipi + (ii+1)*binWidthPPpipi
       label = "Jpsi_m_scaled>"+str(binMin)+"&&Jpsi_m_scaled<"+str(binMax)
       dsetBin = RooDataSet(dsetFull.reduce(RooFit.Cut(label), RooFit.Name("dsetBin"), RooFit.Title("dsetBin")))
       print NJpsi.getVal()
       pdf.fitTo(dsetBin,RooFit.Save(True),RooFit.Extended(True))
       pdf.fitTo(dsetBin,RooFit.Save(True),RooFit.Extended(True),RooFit.Minos(True))
       print NJpsi.getVal()
       EtacPiPiHist.SetBinContent(ii+1,NEtac.getVal())
       EtacPiPiHist.SetBinError(ii+1,NEtac.getErrorHi())
       JpsiPiPiHist.SetBinContent(ii+1,NJpsi.getVal())
       JpsiPiPiHist.SetBinError(ii+1,NJpsi.getErrorHi())
       BgPiPiHist.SetBinContent(ii+1,NBgrPP.getVal())
       BgPiPiHist.SetBinError(ii+1,NBgrPP.getErrorHi())


       frameBin = Etac_M.frame(RooFit.Title("PPbin"))
       dsetBin.plotOn(frameBin,RooFit.Binning(binNPP, minMassPP, maxMassPP))
       pdf.plotOn(frameBin)
    
       canvas = TCanvas("canvPPbin","canvPPbin",800,600)
       frameBin.Draw()
       canvas.SaveAs("binsFit/"+str(ii)+".pdf")



    canvPPpipiClean = TCanvas("canvPPpipi","canvPPpipi",800,900)
    canvPPpipiClean.Divide(1,3)
    canvPPpipiClean.cd(1)
    EtacPiPiHist.Draw()
    canvPPpipiClean.cd(2)
    JpsiPiPiHist.Draw()
    canvPPpipiClean.cd(3)
    BgPiPiHist.Draw()
    canvPPpipiClean.SaveAs("PPpipiClean.pdf")

if __name__ == '__main__':
#    inputData()
    inputModel()
    work()



