from ROOT import *

#gROOT.Reset()
gROOT.ProcessLine(".L RooRelBreitWigner.cxx+")
TProof.Open("")

minMassPPpipi = 3550.
maxMassPPpipi = 4180.
binWidthPPpipi = 5.
binNPPpipi = int((maxMassPPpipi-minMassPPpipi)/binWidthPPpipi)

#minMassPP = 3070.
#maxMassPP = 3130.
#minMassPP = 2930.
#maxMassPP = 3035.

minMassPP = 3414.
maxMassPP = 3560.

binWidthPP = 5.
binNPP = int((maxMassPP-minMassPP)/binWidthPP)

minMasspipi = 600;
maxMasspipi = 1200;
binWidthpipi = 10.;
binNpipi = int((maxMasspipi-minMasspipi)/binWidthpipi)


protonMass = 938.
radius = 0.5
    
Jpsi_m_scaled =  RooRealVar("Jpsi_m_scaled", "Jpsi_m_scaled", minMassPPpipi, maxMassPPpipi, "MeV")
PP_Mass =  RooRealVar("PP_Mass", "PP_Mass", minMassPP, maxMassPP, "MeV")
PiPi_Mass = RooRealVar("PiPi_Mass", "PiPi_Mass", minMasspipi, maxMasspipi, "MeV")



#inFile = TFile("Data/AllPPpipi_CleanLKs_ro.root")
#inFile = TFile("Data/AllPPpipi_CleanLKs.root")
#inFile = TFile("Data/AllPPpipi_CleanL_PIDp05PIDpi05.root")
#inFile = TFile("Data/AllPPpipi_CleanL_PIDp05PIDpi05_DiProton.root")
inFile = TFile("Data/AllPPpipi_TightIDCut.root")
#inFile = TFile("Data/AllPPpipi_Ks.root")
tree = inFile.Get("DecayTree")
dataArgSet = RooArgSet(Jpsi_m_scaled, PP_Mass)


dsetFull = RooDataSet("dsetFull", "dsetFull", tree,dataArgSet)



w = RooWorkspace("w");

getattr(w,'import')(dataArgSet)


CharmoniaPDG ={
    "Etac":{"M":2983.4,"Merr":0.5,"G":31.8,"Gerr":0.8},
    "Jpsi":{"M":3096.9,"Merr":0.006,"G":0.0929,"Gerr":0.0028},
    "Chi0":{"M":3414.75,"Merr":0.31,"G":10.5,"Gerr":0.6},
    "Chi1":{"M":3510.66,"Merr":0.07,"G":0.84,"Gerr":0.04},
    "Chi2":{"M":3556.20,"Merr":0.09,"G":1.93,"Gerr":0.11},
    "hc":{"M":3525.38,"Merr":0.11,"G":0.7,"Gerr":0.4},
    "Etac2":{"M":3639.2,"Merr":1.2,"G":11.3,"Gerr":3.2},
    "Psi2":{"M":3686.097,"Merr":0.025,"G":0.296,"Gerr":0.008},
    "Psi3770":{"M":3773.13,"Merr":0.35,"G":27.2,"Gerr":1.0},
    "Psi3823":{"M":3822.2,"Merr":1.2,"G":8,"Gerr":8},
    "X3872":{"M":3871.69,"Merr":0.17,"G":0.6,"Gerr":0.6},
    "X3900":{"M":3886.6,"Merr":2.4,"G":28.1,"Gerr":2.6},
    "X3915":{"M":3918.4,"Merr":1.9,"G":20,"Gerr":5},
    "Chi22P":{"M":3927.2,"Merr":2.6,"G":24,"Gerr":6},
    "X4020":{"M":4024.1,"Merr":1.9,"G":13,"Gerr":5},
    "Psi4040":{"M":4039.,"Merr":1.,"G":80,"Gerr":10},
    "Psi4140":{"M":4146.9,"Merr":3.1,"G":15,"Gerr":6},
    "Psi4160":{"M":4191.,"Merr":5.,"G":70.,"Gerr":10.},
}

CharmoniaOpts ={
    "Etac":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Jpsi":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi0":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi1":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "hc":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Etac2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi2":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi3770":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi3823":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3872":{"ON":True,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3900":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X3915":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Chi22P":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "X4020":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4040":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4140":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
    "Psi4160":{"ON":False,"Mwind":5.,"Gwind":0.,"Nmax":10000},
}


w.factory("Asig[0,1]")


sumPDFJpsi = ""
sumPDFEtac = ""
sumPDFBg = ""
for state in CharmoniaPDG:
    if CharmoniaOpts[state]["ON"]:
        Mass = CharmoniaPDG[state]["M"]
        MWindow = CharmoniaOpts[state]["Mwind"]
        M_name = state+"Mass"
        Sigma_name = state+"Sigma"
        if(MWindow>1.):
            w.factory(M_name+"[{0},{1}]".format(Mass-MWindow,Mass+MWindow))
        else:
            w.factory(M_name+"[{0}]".format(Mass))
        w.factory("expr::"+Sigma_name+"('Asig*TMath::Sqrt({0}-2*{1})',Asig)".format(Mass,protonMass))
        if CharmoniaPDG[state]["G"]>5:
            G_name = state+"Gamma"
            Gamma = CharmoniaPDG[state]["G"]
            GammaWindow = CharmoniaOpts[state]["Gwind"]
            if(GammaWindow>1.):
                w.factory(G_name+"[{0},{1}]".format(Gamma-GammaWindow,Gamma+GammaWindow))
            else:
                w.factory(G_name+"[{0}]".format(Gamma))
                w.factory("RooRelBreitWigner::"+state+"RBW(Jpsi_m_scaled,"+M_name+","+G_name+",0,{0},{1},{1})".format(radius,protonMass))
            w.factory("FCONV::pdf"+state+"(Jpsi_m_scaled,"+state+"RBW,RooGaussian::"+state+"Gau(Jpsi_m_scaled,"+M_name+","+Sigma_name+"))")
            
        else:
            w.factory("RooGaussian::pdf"+state+"(Jpsi_m_scaled,"+M_name+","+Sigma_name+")")
        pdfName = "pdf"+state
        sumPDFJpsi+=state+"N_Jpsi[0,"+str(CharmoniaOpts[state]["Nmax"])+"]*"+pdfName+","
        sumPDFEtac+=state+"N_Etac[0,"+str(CharmoniaOpts[state]["Nmax"])+"]*"+pdfName+","
        sumPDFBg+=state+"N_Bg[0,"+str(CharmoniaOpts[state]["Nmax"])+"]*"+pdfName+","

w.factory("Chebychev::pdfBgr_Jpsi(Jpsi_m_scaled,{Bgr1_Jpsi[-1,1],Bgr2_Jpsi[-1,1],Bgr3_Jpsi[-1,1]})")
w.factory("Chebychev::pdfBgr_Etac(Jpsi_m_scaled,{Bgr1_Etac[-1,1],Bgr2_Etac[-1,1],Bgr3_Etac[-1,1]})")
w.factory("Chebychev::pdfBgr_Bg(Jpsi_m_scaled,{Bgr1_Bg[-1,1],Bgr2_Bg[-1,1],Bgr3_Bg[-1,1]})")


sumPDFJpsi+="NBgr_Jpsi[0,100000]*pdfBgr_Jpsi"
sumPDFEtac+="NBgr_Etac[0,100000]*pdfBgr_Etac"
sumPDFBg+="NBgr_Bg[0,100000]*pdfBgr_Bg"

w.factory("SUM::SumPDFpppipi_Jpsi("+sumPDFJpsi+")")
w.factory("SUM::SumPDFpppipi_Etac("+sumPDFEtac+")")
w.factory("SUM::SumPDFpppipi_Bg("+sumPDFBg+")")






jpsi_m = CharmoniaPDG["Jpsi"]["M"]
jpsi_dm = CharmoniaOpts["Jpsi"]["Mwind"]
w.factory("Gaussian::pdfJpsiPP(PP_Mass,JpsiMass[{0},{1}],JpsiSigmaPP[0,15])".format(jpsi_m-jpsi_dm,jpsi_m+jpsi_dm))



etac_m = CharmoniaPDG["Etac"]["M"]
etac_dm = CharmoniaOpts["Etac"]["Mwind"]
w.factory("RooRelBreitWigner::EtacRBW(PP_Mass,EtacMass[{0},{1}],EtacGamma[0,80],0,{2},{3},{3})".format(etac_m-etac_dm,etac_m+etac_dm,radius,protonMass))
w.factory("FCONV::pdfEtacPP(PP_Mass,EtacRBW,Gaussian::EtacG(PP_Mass,EtacMass,JpsiSigmaPP))")




w.factory("Chebychev::pdfBgrPP(PP_Mass,{PPBg1[-1,1],PPBg2[-1,1]})")






w.factory("PROD::pdfJpsi(SumPDFpppipi_Jpsi,pdfJpsiPP)")
w.factory("PROD::pdfEtac(SumPDFpppipi_Etac,pdfEtacPP)")
w.factory("PROD::pdfBgr(SumPDFpppipi_Bg,pdfBgrPP)")



#w.factory("SUM::pdfModel(1*pdfJpsi,1*pdfEtac,1*pdfBgr)")
w.factory("SUM::pdfModel(1*pdfJpsi,1*pdfBgr)")

#pdf = w.pdf("SumPDFpppipi_Jpsi")
pdf = w.pdf("pdfModel")

#pdf.fitTo(dsetFull,RooFit.Save(True))



framePPpipi = Jpsi_m_scaled.frame(RooFit.Title("PPpipi"))
dsetFull.plotOn(framePPpipi,RooFit.Binning(binNPPpipi, minMassPPpipi, maxMassPPpipi))
#pdf.plotOn(framePPpipi)


framePP = PP_Mass.frame(RooFit.Title("PP"))
dsetFull.plotOn(framePP,RooFit.Binning(binNPP, minMassPP, maxMassPP))
#pdf.plotOn(framePP)



canvas = TCanvas("canv","canv",800,600)
canvas.Divide(1,2)
canvas.cd(1)
framePP.Draw()
canvas.cd(2)
framePPpipi.Draw()

canvas.SaveAs("Jpsi.pdf")


