/*
 * splot2D.cpp
 *
 *  Created on: Jun 6, 2013
 *      Author: maksym
 */
using namespace RooFit;
using namespace RooStats;




void splot2D(){
	
	gROOT->Reset();
	gROOT->SetStyle("Plain");
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
	gStyle->SetOptStat(000);
	TProof::Open("");

    

    Float_t minMassJpsi = 2900;
    Float_t maxMassJpsi = 4200;
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);

	
    Float_t minMassPP = 2850;
    Float_t maxMassPP = 3250;
    Float_t binWidthPP = 10.;
    Int_t binNPP = int((maxMassPP-minMassPP)/binWidthPP);
    
	
    Float_t minMassPiPi = 600;
    Float_t maxMassPiPi = 1200;
    Float_t binWidthPiPi = 10.;
    Int_t binNPiPi = int((maxMassPiPi-minMassPiPi)/binWidthPiPi);
    

    
    
    
    
    
    Float_t etacMass =	2983.6;
    Float_t jpsi_mass =3096.97;
    Float_t etacMassError =	0.7;
    Float_t etacGamma =	32.2;
    Float_t etacGammaError =	0.9;
    Float_t etacMassBase = 2982.93;
    Float_t etacGammaBase = 31.80;
    
  
    

    
    
//    // make Pull histo
//    TH1F* h_pull = new TH1F("h_pull","h_pull",100,-5.,5.);
//    for(int i =0; i < h_pull_vs_m4l->GetNbinsX()+1;i++){
//        double center_bin = h_m4l_inclusive->GetBinCenter(i);
//        double y_bin = h_m4l_inclusive->GetBinContent(i);
//        double err_y_bin = h_m4l_inclusive->GetBinError(i);
//        double func_center_bin = func->Eval(center_bin);
//        double err_func = func_center_bin*0.0;
//        // calculate
//        double pull;
//        if( y_bin != 0. ){
//            pull = (func_center_bin - y_bin) / TMath::Power(TMath::Power(err_y_bin,2) + TMath::Power(err_func,2),0.5);
//            h_pull_vs_m4l->SetBinContent(i,pull);
//            h_pull_vs_m4l->SetBinError(i,0);
//            h_pull->Fill(pull);
//        }
//    }
    
    
//    TH1F* h_pull_vs_m4l = (TH1F*)h_m4l_inclusive->Clone("h_pull_vs_m4l");
//    h_pull_vs_m4l->Scale(0);
//    TCanvas *C = new TCanvas();
//    C->cd();
//    TPad *pad_hist = new TPad("pad_hist","pad_hist",0.05,0.35,0.95,0.95);
//    TPad *pad_pull = new TPad("pad_pull","pad_pull",0.05,0.05,0.95,0.30);
//    pad_hist->Draw();
//    pad_pull->Draw();
//    
//    pad_hist->cd();
//    h_m4l_inclusive->Draw();
//    
//    pad_pull->cd();
//    h_pull_vs_m4l->GetYaxis()->SetRangeUser(-5.,5.);
//    h_pull_vs_m4l->GetYaxis()->SetLabelSize(0.07);
//    h_pull_vs_m4l->GetXaxis()->SetLabelSize(0.07);
//    pad_pull->SetGridy();
//    h_pull_vs_m4l->SetMarkerStyle(20);
//    h_pull_vs_m4l->SetMarkerSize(0.6);
//    h_pull_vs_m4l->Draw("P0");
//    [07/11/16 19:46:46] Denys Denysiuk: /
//    [07/11/16 19:46:46] Denys Denysiuk: /
//    [07/11/16 19:46:47] Denys Denysiuk: h_m4l_inclusive
//


    
    
    
    
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
    
    RooRealVar PP_Mass("PP_Mass", "PP_Mass", minMassPP, maxMassPP, "MeV");
    PP_Mass.setBins(binNPP);
    
    RooRealVar PiPi_Mass("PiPi_Mass", "PiPi_Mass", minMassPiPi, maxMassPiPi, "MeV");
    PiPi_Mass.setBins(binNPiPi);



//	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
//	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
//	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TChain* chain = new TChain("DecayTree");
	chain->Add("Data/AllPPpipi_CleanLKs.root");
	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, PP_Mass, PiPi_Mass),"","");





    RooArgList listCompPP, listNormPP;
    RooArgSet showParamsPP;
    RooRealVar radius("radius", "radius", 1.5);
    RooRealVar massa("massa", "massa", 938.27, "MeV");
    RooRealVar massb("massb", "massb", 938.27, "MeV");
    RooRealVar var2PMass("var2PMass", "var2PMass", 2*938.27);
    RooRealVar FitMin("FitMin","FitMin",minMassPP);


    RooRealVar varJpsiNumber("NJpsi", "varJpsiNumber", 1e4, 0, 1e7);
    RooRealVar varJpsiMass("M(J/#psi)", "varJpsiMass",jpsi_mass,jpsi_mass-2,jpsi_mass+2);
    RooRealVar varJpsiSigmaN("varJpsiSigmaN", "varJpsiSigmaN", 0,20);
    RooGaussian pdfJpsi("pdfJpsi","pdfJpsi",PP_Mass,varJpsiMass,varJpsiSigmaN);

    RooRealVar varEtacNumber("NEta", "varEtacNumber", 1e3,0,1e7);
    RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", etacMass,etacMass-20,etacMass+20);
    RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32.00);
    RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", PP_Mass, varEtacMass, varEtacGamma,
                                                       RooConst(0), radius, massa, massb);
    RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",PP_Mass,RooConst(0.),varJpsiSigmaN);
    RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",PP_Mass,pdfEtacBWrel,pdfEtacGaussN) ;
    
    
    RooRealVar varXNumber("NX", "varXNumber", 1e3,0,1e7);
    RooRealVar varXMass("M(X)", "varXMass", 2950.,2970.);
    RooRealVar varXGamma("varXGamma", "varXGamma", 0,900);
    RooRelBreitWigner pdfXBWrel = RooRelBreitWigner("pdfXBWrel", "pdfXBWrel", PP_Mass, varXMass, varXGamma,
                                                       RooConst(0), radius, massa, massb);
    RooGaussian pdfXGaussN("pdfXGaussN","pdfXGaussN",PP_Mass,RooConst(0.),varJpsiSigmaN);
    RooFFTConvPdf pdfXN("pdfXN","pdfXN",PP_Mass,pdfXBWrel,pdfEtacGaussN) ;


    RooRealVar varBgrNumber("N_{bgr}", "number of background events", 1e4, 0, 1e9);
    RooRealVar varB0("varB0", "varB0", 0, -1, 1);
    RooRealVar varB1("varB1", "varB1", 0, -1, 1);
    RooRealVar varB2("varB2", "varB2", 0, -1, 1);
    RooRealVar varB3("varB3", "varB3", 0, -1, 1);
    RooRealVar varB4("varB4", "varB4", 0, -1, 1);

    
    
    
    RooChebychev pdfBgr("pdfBgr", "pdfBgr", PP_Mass, RooArgList(varB0,varB1,varB2,varB3));

    
    
    listCompPP.add(pdfJpsi);
    listNormPP.add(varJpsiNumber);
    listCompPP.add(pdfEtacN);
    listNormPP.add(varEtacNumber);
//    listCompPP.add(pdfXN);
//    listNormPP.add(varXNumber);
    listCompPP.add(pdfBgr);
    listNormPP.add(varBgrNumber);
	RooAddPdf pdfModelPP("pdfModelPP", "pdfModelPP",  listCompPP, listNormPP);
    
    
    TH1F* histPP = new TH1F("histPP", "histPP", binNPP, minMassPP, maxMassPP);
    chain->Project("histPP", "PP_Mass", "");
    RooDataHist* dset = new RooDataHist("dset", "dset", PP_Mass, histPP);
    
    
    
    RooFitResult* res = pdfModelPP.fitTo(*dset, Save(true), Extended(true), PrintLevel(0));
    res = pdfModelPP.fitTo(*dset, Save(true), Extended(true), PrintLevel(0));
    //res = pdfModelPP.fitTo(*dset, Save(true), Minos(true), Extended(true), PrintLevel(0));
    
    
    RooPlot* framePP = PP_Mass.frame(Title("PP mass"));
    dset->plotOn(framePP, Binning(binNPP, minMassPP, maxMassPP));
    pdfModelPP.plotOn(framePP);
    
    RooHist* hresidPP = framePP->residHist();
   
    
    TCanvas* canvP = new TCanvas("canvP", "canvP", 1000, 500);
    
    RooPlot* framePP2 = PP_Mass.frame();
    framePP2->addPlotable(hresidPP,"P");
    TPad *pad2 = new TPad("pad2", "pad2",0.01,0.05,0.99,0.3);
    pad2->Draw();
    pad2->cd();
    framePP2->Draw();
    
    canvP->cd();
    TPad *pad = new TPad("pad", "pad",0.01,0.3,0.99,0.99);
    pad->Draw();
    pad->cd();
    framePP->Draw();

    
    

   
    
//    varEtacMass.setConstant(kTRUE);
//    varEtacGamma.setConstant(kTRUE);
//    varJpsiMass.setConstant(kTRUE);
//    varJpsiSigmaN.setConstant(kTRUE);
//    varB0.setConstant(kTRUE);
//    varB1.setConstant(kTRUE);
//    varB2.setConstant(kTRUE);
//    varB2.setConstant(kTRUE);
//    varB3.setConstant(kTRUE);
//    varB4.setConstant(kTRUE);
//    
//     SPlot* sDataPP = new SPlot("sDataPP", "sDataPP", *dsetFull, &pdfModelPP, RooArgList(varEtacNumber, varJpsiNumber, varBgrNumber));
//
//    
//    
//    RooDataSet * dsetNew = new RooDataSet(dsetFull->GetName(),
//    			dsetFull->GetTitle(),dsetFull,*dsetFull->get(),0,"NEta_sw");
//
//    
//    
//    
//    TCanvas* canvPPSplot = new TCanvas("canvPPSplot", "canvPPSplot", 1000, 400);
//    RooPlot* frameSplotPP = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled Splot"));
//    dsetNew->plotOn(frameSplotPP,Binning(binNJpsi, minMassJpsi, maxMassJpsi),DataError(RooAbsData::SumW2));
//    frameSplotPP->Draw();


    


    
    
    
//    RooRealVar var2PiMass("var2PiMass", "var2PiMass", 2*139.57);
//    RooRealVar FitMinPiPi("FitMinPiPi","FitMinPiPi",minMassPiPi);
//    
//    
//    Double_t Ro_mass = 775.26;
//    Double_t Ro_gamma = 149.1;
//    RooArgList listCompPiPi, listNormPiPi;
//    RooArgSet showParamsPiPi;
//    
//    RooRealVar varRoNumber("N(Ro)", "varRoNumber", 1e2, 0, 1e5);
//    RooRealVar varRoMass("M(Ro)", "varRoMass",Ro_mass);
//    RooRealVar varRoGamma("varRoGamma", "varRoGamma", Ro_gamma,0,2*Ro_gamma);
//    RooBreitWigner pdfRo("pdfRo","pdfRo",PiPi_Mass,varRoMass,varRoGamma);
////
////    RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984.00, 2975, 2995);
////    RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32.00);
////    RooRealVar varEtacNumber("NEta", "varEtacNumber", 1e5, 1e3, 1e7);
////    RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", PiPi_Mass, varEtacMass, varEtacGamma,
////                                                       RooConst(0), radius, massa, massb);
////    RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",PiPi_Mass,RooConst(0.),varRoSigmaN);
////    RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",PiPi_Mass,pdfEtacBWrel,pdfEtacGaussN) ;
////    
////    
//    RooRealVar varBgrNumberPi("N_{bgr} pi", "number of background events pi", 6.5e4, 1e2, 1e9);
//    RooRealVar varBExpPi("varBExpPi", "varBExpPi", -0.1, -10, 0);
//    RooRealVar varB0pi("varB0pi", "varB0pi", 0, -1000, 1000);
//    RooRealVar varB1pi("varB1pi", "varB1pi", 0, -1000, 1000);
//    RooRealVar varB2pi("varB2pi", "varB2pi", 0, -1000, 1000);
//    RooRealVar varB3pi("varB3pi", "varB3pi", 0, -1000, 1000);
//    RooRealVar varB4pi("varB4pi", "varB4pi", 0, -1000, 1000);
//    RooRealVar varB5pi("varB5pi", "varB5pi", 0, -1000, 1000);
//    RooRealVar varB6pi("varB6pi", "varB6pi", 0, -1000, 1000);
//    RooGenericPdf pdfRootPi("pdfRootPi","pdfRootPi","TMath::Sqrt(@0-@1)*TMath::Exp(-@0*@2)*(1+(@0-@1))",RooArgList(PiPi_Mass,var2PiMass, varBExpPi,varB0pi));
//    RooChebychev pdfBgrPiCheb("pdfBgrPiCheb", "pdfBgrPiCheb", PiPi_Mass, RooArgList(varB1pi,varB2pi,varB3pi,varB4pi,varB5pi));
//    RooProdPdf pdfBgrPi("pdfBgrPi", "pdfBgrPi",RooArgSet(pdfRootPi, pdfBgrPiCheb));
//
//    
//    
//
//
//    
//    listCompPiPi.add(pdfRo);
//    listNormPiPi.add(varRoNumber);
//    listCompPiPi.add(pdfBgrPi);
//    listNormPiPi.add(varBgrNumberPi);
//    RooAddPdf pdfModelPiPi("pdfModelPiPi", "pdfModelPiPi",  listCompPiPi, listNormPiPi);
//    
//    
//    TH1F* histPiPi = new TH1F("histPiPi", "histPiPi", binNPiPi, minMassPiPi, maxMassPiPi);
//    chain->Project("histPiPi", "PiPi_Mass", "Jpsi_PT>2500");
//    RooDataHist* dsetPi = new RooDataHist("dsetPi", "dsetPi", PiPi_Mass, histPiPi);
//    RooFitResult* resPi = pdfModelPiPi.fitTo(*dsetPi, Save(true), PrintLevel(0));
//    
//    
//    RooPlot* framePiPi = PiPi_Mass.frame(Title("PiPi mass"));
//    dsetPi->plotOn(framePiPi, Binning(binNPiPi, minMassPiPi, maxMassPiPi));
//    pdfModelPiPi.plotOn(framePiPi);
//    
//    TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 400);
//    framePiPi->Draw();
    
    
    
    
    
    
    
    
    
    
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1);
////	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
//	if(Type==0)
//	  RooRealVar varSigma("varSigma", "varSigma", 1.20);
//	if(Type==2)
//	  RooRealVar varSigma("varSigma", "varSigma", 1.15);
//	if(Type==1)
//	  RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
//	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
//	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
//	
//
//	RooRealVar varA1("varA1", "varA1", 0,0,1);
//	RooRealVar varA2("varA2", "varA2", 0);
//	
//
//	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
//			      RooArgList(PP_Mass, var2KMass, varPhiMass,varSigma,varPhiGamma));	
//	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
//			      RooArgList(PiPi_Mass, var2KMass, varPhiMass,varSigma,varPhiGamma));
//
//	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(PP_Mass, var2KMass,varA1));
//	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(PP_Mass, var2KMass,varA2));
//	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(PiPi_Mass, var2KMass,varA1));
//	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(PiPi_Mass, var2KMass,varA2));
//	
//	
//	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
//	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
//	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
//	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
//	
//	
//	
//	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
//	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
//	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
//	
//	
//
//	
//	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
//							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
//	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
//							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
//
//	
//	RooAddPdf pdfModel("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));



//	RooFitResult* res = pdfModel.fitTo(*dsetFull, Save(true), PrintLevel(0));
//	varPhiMass.setConstant(kTRUE);
//	varSigma.setConstant(kTRUE);
//    varA1.setConstant(kTRUE);
//	SPlot* sData1 = new SPlot("sData1", "sData1", *dsetFull, &pdfModel, RooArgList(varNDiPhi, varNPhiK, varNDiK));



//	RooDataSet * dset = new RooDataSet(dsetFull->GetName(),
//			dsetFull->GetTitle(),dsetFull,*dsetFull->get(),0,"varNDiPhi_sw") ;
//

//
//	RooPlot* frame2 = PiPi_Mass.frame(Title("#phi_{2} mass"));
//	dsetFull->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
//	pdfModel2.plotOn(frame2);
//	pdfModel.plotOn(frame2, Components(pdfSB), LineStyle(kDashed));
//
//	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 400);
//	canvA->Divide(3, 1);
//	canvA->cd(1);
//	TH2D* histViz = pdfModel.createHistogram("PP_Mass, PiPi_Mass");
//	histViz->Draw("surf");
//	canvA->cd(2);
//	framePP->Draw();
//	canvA->cd(3);
//	frame2->Draw();
//	canvA->SaveAs("diKMass.png");
//
//	RooPlot* frame3 = Jpsi_m_scaled.frame(Title("#phi#phi mass"));
//	dset->plotOn(frame3,Binning(binNJpsi, minMassJpsi, maxMassJpsi));
////	dsetFull->plotOn(frame3);
//	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 400);
//	frame3->Draw();
//    
//    
//    RooPlot* frame4 = PP_Mass.frame(Title("#phi#phi mass"));
//    dset->plotOn(frame4,DataError(RooAbsData::SumW2));
//    //	dsetFull->plotOn(frame3);
//    TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 400);
//    frame4->Draw();
//
//
//    TFile file("Splot.root","recreate");
//
//
//	TTree* tree = dset->tree();
//	tree->SetName("DecayTree");
//	tree->SetTitle("DecayTree");
//	tree->Write();
//	file.Close();
	
	
}



